import React, { createContext, useEffect, useState } from "react";
import { useHistory, useLocation, useParams } from "react-router-dom";
import { v4 as uuid } from "uuid/dist";

import card from "../static/images/sidebarIcons/card.svg";
import engage from "../static/images/sidebarIcons/engage.svg";
import issuance from "../static/images/sidebarIcons/issuance.svg";
import vendors from "../static/images/sidebarIcons/vendors.svg";
import funding from "../static/images/sidebarIcons/funding.svg";
import iconVault from "../static/images/logos/vaultLogo.svg";
import bankersLogo from "../static/images/sidebarIcons/bankersLogo.svg";
import { Fragment } from "react";

export const TabsContext = createContext();

function TabsContextProvider({ children }) {
  const history = useHistory();
  const [typeOfPage, setTypeOfPage] = useState("partner");
  const [tabs, setTabs] = useState([{ location: `/${typeOfPage}`, key: "0" }]);
  const [tabIndex, setTabIndex] = useState(0);
  const { pathname } = useLocation();

  useEffect(() => {
    let tempArr = tabs.slice();
    tempArr[tabIndex] = { ...tempArr[tabIndex], location: pathname };
    setTabs(tempArr);
  }, [pathname, setTabs]);

  function newTab(to = `/${typeOfPage}/`) {
    setTabIndex(tabs.length);
    setTabs((tabs) => [...tabs, { location: to, key: uuid() }]);
    history.push(to);
  }
  function removeTab(i) {
    setTabs((tabs) => tabs.filter((v, index) => index !== i));
    let indexTemp = tabIndex - 1 || 0;
    if (indexTemp < 1) indexTemp = 0;
    history.push(tabs[indexTemp]);
    setTabIndex(indexTemp);
  }
  function getNavHead(tab, i) {
    switch (true) {
      case /\/*\/new-card/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <div
              className="navLogoText"
              onClick={() => {
                setTabIndex(i);
                history.push(tab.location);
              }}
            >
              <img src={card} className="navLogo" alt="" />
              <span>Card Machine</span>
            </div>
            <div className="divider" />
          </Fragment>
        );
      case /\/*\/liquidVault/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <div
              className="navLogoText"
              onClick={() => {
                setTabIndex(i);
                history.push(tab.location);
              }}
            >
              <img src={iconVault} className="navLogo" alt="" />
              <span>Liquid Vaults</span>
            </div>
            <div className="divider" />
          </Fragment>
        );
      case /\/*\/withdrawals/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <div
              className="navLogoText"
              onClick={() => {
                setTabIndex(i);
                history.push(tab.location);
              }}
            >
              <img src={bankersLogo} className="navLogo" alt="" />
              <span>Bankers</span>
            </div>
            <div className="divider" />
          </Fragment>
        );
      case /\/*\/content/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <div
              className="navLogoText"
              onClick={() => {
                setTabIndex(i);
                history.push(tab.location);
              }}
            >
              <img src={engage} className="navLogo" alt="" />
              <span>Engagement</span>
            </div>
            <div className="divider" />
          </Fragment>
        );
      case /\/*\/funding/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <div
              className="navLogoText"
              onClick={() => {
                setTabIndex(i);
                history.push(tab.location);
              }}
            >
              <img src={funding} className="navLogo" alt="" />
              <span>Funding</span>
            </div>
            <div className="divider" />
          </Fragment>
        );
      case /\/*\/vendors/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <div
              className="navLogoText"
              onClick={() => {
                setTabIndex(i);
                history.push(tab.location);
              }}
            >
              <img src={vendors} className="navLogo" alt="" />
              <span>Vendors</span>
            </div>
            <div className="divider" />
          </Fragment>
        );
      case /\/*/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <div
              className="navLogoText"
              onClick={() => {
                setTabIndex(i);
                history.push(tab.location);
              }}
            >
              <img src={issuance} className="navLogo" alt="" />
              <span>Issuance</span>
            </div>
            <div className="divider" />
          </Fragment>
        );
      default:
        break;
    }
  }

  const navTabs = (
    <div className="navList">
      {tabs.map((tab, i) => (
        <div key={tab.key} className={`navBarWrapper ${i === tabIndex}`}>
          {getNavHead(tab, i)}
          {tabs.length > 1 && (
            <div
              className="btnClose"
              onClick={() =>
                setTimeout(() => {
                  removeTab(i);
                }, 200)
              }
            />
          )}
        </div>
      ))}
      <div className="addNav" onClick={() => newTab()} />
    </div>
  );

  return (
    <TabsContext.Provider
      value={{
        tabs,
        setTabs,
        tabIndex,
        setTabIndex,
        navTabs,
        newTab,
        setTypeOfPage,
      }}
    >
      {children}
    </TabsContext.Provider>
  );
}

export default TabsContextProvider;
