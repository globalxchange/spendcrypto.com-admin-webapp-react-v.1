import React, { createContext, useCallback, useEffect, useState } from "react";
import Axios from "axios";
import Toast from "../components/Toast/Toast";
import ModalSessionExpired from "../components/ModalSessionExpired/ModalSessionExpired";

export const MainContext = createContext();

function MainContextProvider({ children }) {
  const [email, setEmail] = useState(
    localStorage.getItem("LoginAccount") || ""
  );
  const [accessToken, setAccessToken] = useState(
    localStorage.getItem("AccessToken") || ""
  );
  const [token, setToken] = useState(localStorage.getItem("Token") || "");

  useEffect(() => {
    localStorage.setItem("LoginAccount", email);
  }, [email]);
  useEffect(() => {
    localStorage.setItem("AccessToken", accessToken);
  }, [accessToken]);
  useEffect(() => {
    localStorage.setItem("Token", token);
  }, [token]);

  const login = (paramEmail, paramAccessToken, paramToken) => {
    setEmail(paramEmail);
    setAccessToken(paramAccessToken);
    setToken(paramToken);
  };

  const [profilePic, setProfilePic] = useState("");
  const [profileName, setProfileName] = useState("");
  const [profileId, setProfileId] = useState("");

  useEffect(() => {
    if (email && token) {
      Axios.post("https://comms.globalxchange.com/coin/verifyToken", {
        email,
        token: token,
      }).then((res) => (res.data.status ? "" : login("", "", "")));

      Axios.get(
        `https://comms.globalxchange.com/user/details/get?email=${email}`
      ).then((res) => {
        const { data } = res;
        console.log("data :>> ", data);
        if (data.status) {
          setProfileName(data.user.name);
          setProfilePic(data.user.profile_img);
        }
      });
      Axios.get(
        `https://comms.globalxchange.com/user/details/get?email=${email}`
      ).then((res) => {
        const { data } = res;
        if (data.status) {
          setProfileId(data.user.spendcrypto_profile_id);
        }
      });
    }
  }, [email, token]);

  const [rates, setRates] = useState({});
  useEffect(() => {
    Axios.get("https://comms.globalxchange.com/coin/getCmcPrices").then(
      (res) => {
        const { data } = res;
        setRates({ ...data, USD: 1 });
      }
    );
  }, []);

  const [toastShow, setToastShow] = useState(false);
  const [toastMessage, setToastMessage] = useState("");
  const tostShowOn = (message) => {
    setToastShow(true);
    setToastMessage(message);
    setTimeout(() => {
      setToastShow(false);
    }, 3000);
  };

  const [appSelected, setAppSelected] = useState(
    JSON.parse(localStorage.getItem("appSelected"))
  );
  useEffect(() => {
    localStorage.setItem("appSelected", JSON.stringify(appSelected));
  }, [appSelected]);

  useEffect(() => {
    !email && setAppSelected("");
  }, [email]);

  const [coinList, setCoinList] = useState([]);

  const updateBalance = useCallback(() => {
    Axios.post("https://comms.globalxchange.com/coin/vault/service/coins/get", {
      app_code: "spendcrypto",
      profile_id: profileId,
    }).then((res) => {
      const { data } = res;
      if (data.status) {
        const { coins_data } = data;
        setCoinList(coins_data);
      }
    });
  }, [profileId]);

  const [coinListObject, setCoinListObject] = useState({});
  useEffect(() => {
    let coinObj = {};
    coinList.forEach((coin) => {
      coinObj[coin.coinSymbol] = coin;
    });
    setCoinListObject(coinObj);
  }, [coinList]);

  useEffect(() => {
    updateBalance();
  }, [profileId, updateBalance]);

  const [userApps, setUserApps] = useState([]);
  useEffect(() => {
    Axios.get(
      `https://comms.globalxchange.com/gxb/apps/registered/user?email=${email}`
    ).then(({ data }) => {
      if (data.status) {
        setUserApps(data.userApps);
      }
    });
  }, [email]);

  const [modalSessionExpOpen, setModalSessionExpOpen] = useState(false);
  const validateToken = async (paramEmail, paramToken) => {
    const res = await Axios.post(
      "https://comms.globalxchange.com/coin/verifyToken",
      {
        email: paramEmail,
        token: paramToken,
      }
    );
    if (res.data && res.data.status) {
      return true;
    } else {
      setModalSessionExpOpen(true);
      return false;
    }
  };

  const [bankerSelected, setBankerSelected] = useState(
    JSON.parse(localStorage.getItem("bankerSelected"))
  );
  useEffect(() => {
    localStorage.setItem("bankerSelected", JSON.stringify(bankerSelected));
  }, [bankerSelected]);

  const [vendorEmail, setVendorEmail] = useState("");
  const [vendorPin, setVendorPin] = useState(false);
  return (
    <MainContext.Provider
      value={{
        login,
        email,
        token,
        profilePic,
        profileName,
        profileId,
        rates,
        tostShowOn,
        appSelected,
        setAppSelected,
        bankerSelected,
        setBankerSelected,
        vendorEmail,
        setVendorEmail,
        vendorPin,
        setVendorPin,
        coinList,
        coinListObject,
        userApps,
        validateToken,
      }}
    >
      {children}
      {modalSessionExpOpen ? (
        <ModalSessionExpired
          onClose={() => {
            setModalSessionExpOpen(false);
          }}
        />
      ) : (
        ""
      )}
      <Toast show={toastShow} message={toastMessage} />
    </MainContext.Provider>
  );
}

export default MainContextProvider;
