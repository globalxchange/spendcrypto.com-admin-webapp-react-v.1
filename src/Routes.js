import React from "react";
import { Switch, Route } from "react-router-dom";
import CardsPage from "./pages/CardsPage";
import GetMobileAppPage from "./pages/GetMobileAppPage";
import HomePage from "./pages/HomePage";
import LandingPage from "./pages/LandingPage";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import SelectAppPage from "./pages/SelectAppPage";
import SelectPartnerPage from "./pages/SelectPartnerPage";
import SelectVendorAppPage from "./pages/SelectVendorAppPage";
import SwitcherPage from "./pages/SwitcherPage";

function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route exact path="/appSelect" component={SelectAppPage} />
      <Route exact path="/partners" component={SelectPartnerPage} />
      <Route exact path="/partnerWithUs" component={RegisterPage} />
      <Route exact path="/getApp" component={GetMobileAppPage} />
      <Route exact path="/cards" component={CardsPage} />
      <Route exact path="/cards/:card" component={CardsPage} />
      <Route exact path="/:typeOfPage/landing" component={LandingPage} />
      <Route exact path="/:typeOfPage/selectApp" component={SelectAppPage} />
      <Route
        exact
        path="/:typeOfPage/selectVendorApp"
        component={SelectVendorAppPage}
      />
      <Route exact path="/:typeOfPage/login" component={LoginPage} />
      <Route path="/:typeOfPage/*/:txnId" component={SwitcherPage} />
      <Route path="/:typeOfPage/*" component={SwitcherPage} />
      <Route path="/:typeOfPage*" component={SwitcherPage} />
    </Switch>
  );
}

export default Routes;
