import React from "react";
import { BrowserRouter } from "react-router-dom";
import MainContextProvider from "./context/MainContext";
import TabsContextProvider from "./context/TabsContext";
import Routes from "./Routes";
import "./static/scss/master.scss";
import "bootstrap-scss/bootstrap.scss";
import "@teamforce/broker-admin-dashboards/dist/index.css";

function AppBadVault() {
  return (
    <MainContextProvider>
      <BrowserRouter>
        <TabsContextProvider>
          <Routes />
        </TabsContextProvider>
      </BrowserRouter>
    </MainContextProvider>
  );
}

export default AppBadVault;
