import { MainLayout } from "@teamforce/broker-admin-dashboards";
import React, { useContext } from "react";
import { Redirect, useParams } from "react-router-dom";
import { MainContext } from "../context/MainContext";
import Sidebar from "./Sidebar/Sidebar";

function MainLayoutComponent({ children, active, onNew, hide }) {
  const { email, appSelected, bankerSelected } = useContext(MainContext);
  const { typeOfPage } = useParams();

  return (
    <>
      {email === "" ? <Redirect to={`/${typeOfPage}/login`} /> : ""}
      {email === "" ? (
        <Redirect to={`/${typeOfPage}/login`} />
      ) : appSelected === "" && typeOfPage === "partner" ? (
        <Redirect to={`/partners`} />
      ) : !bankerSelected?.sc_vendor_id && typeOfPage === "banker" ? (
        <Redirect to={`/partners`} />
      ) : (
        ""
      )}
      <MainLayout
        primaryColor="#484848"
        className={`${active}`}
        sidebar={<Sidebar active={active} />}
        hide={hide}
        children={children}
      />
    </>
  );
}

export default MainLayoutComponent;
