import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import spendcryptoLogo from "../../static/images/logos/spendCryptoLogo.svg";
import walletIcon from "../../static/images/sidebarIcons/issuance.svg";
import profileIcon from "../../static/images/postClipArt/profile.svg";
import { ReactComponent as SpendSupported } from "../../static/images/logos/spendSupported.svg";

function LandingNavbar({
  menuShow,
  menu,
  active,
  search,
  setSearch,
  showSearch,
  placeHolder,
  onClose,
}) {
  const history = useHistory();
  const [navOpen, setNavOpen] = useState(false);
  const [searchOpen, setSearchOpen] = useState(false);
  return (
    <nav className={`landing-nav ${!menuShow}`}>
      <div
        onClick={() => setNavOpen(!navOpen)}
        className={
          "d-flex hamburger hamburger--squeeze" + (navOpen ? " is-active" : "")
        }
      >
        <span className="hamburger-box m-auto">
          <span className="hamburger-inner"></span>
        </span>
      </div>
      {menuShow && (
        <div className="nav-menu">
          {menu ? (
            <div
              className="bt-login"
              onClick={() => {
                try {
                  menu[0].onClick();
                } catch (error) {}
              }}
            >
              <div className="insider" />
              {menu[0]?.img && <img src={menu[0]?.img} alt="" />}
              {menu[0]?.name}
            </div>
          ) : (
            <Link to="/partners" className="bt-login">
              <div className="insider" />
              <img src={profileIcon} alt="" />
              Partners
            </Link>
          )}
        </div>
      )}
      <img
        src={spendcryptoLogo}
        onClick={() => history.push("/")}
        alt=""
        className="market-logo"
      />
      {menuShow && (
        <div className="nav-menu">
          {menu ? (
            <div
              className="bt-login"
              onClick={() => {
                try {
                  menu[1].onClick();
                } catch (error) {}
              }}
            >
              <div className="insider" />
              {menu[1]?.img && <img src={menu[1]?.img} alt="" />}
              {menu[1]?.name}
            </div>
          ) : (
            <Link to="/appSelect" className="bt-login">
              <div className="insider" />
              <img src={walletIcon} alt="" />
              Wallets
            </Link>
          )}
        </div>
      )}
      {!menuShow && (
        <label className="box">
          {!searchOpen ? (
            <SpendSupported className="logo" />
          ) : (
            <input
              type="text"
              value={search}
              onChange={(e) => setSearch(e.target.value)}
              className="logoIp"
              placeholder={placeHolder}
            />
          )}

          {showSearch && (
            <svg
              width="20"
              height="20"
              viewBox="0 0 18 18"
              fill="transparent"
              xmlns="http://www.w3.org/2000/svg"
              style={{
                marginRight: "30px",
              }}
              onClick={() => setSearchOpen(!searchOpen)}
            >
              {searchOpen ? (
                <path
                  d="M10.6317 9L17.6617 1.96993C18.1127 1.51912 18.1128 0.789171 17.6618 0.338256C17.2109 -0.112694 16.481 -0.112799 16.0301 0.338221L9 7.36829L1.96993 0.338256C1.51913 -0.112658 0.78914 -0.112799 0.338226 0.338186C-0.112724 0.789101 -0.112759 1.51895 0.338226 1.9699L7.36822 9L0.33819 16.03C-0.111705 16.4799 -0.111705 17.2119 0.33819 17.6617C0.78907 18.1127 1.51899 18.1128 1.9699 17.6617L8.99993 10.6316L16.03 17.6617C16.4808 18.1126 17.2108 18.1126 17.6616 17.6617C18.1126 17.2108 18.1127 16.4809 17.6617 16.0299L10.6317 9Z"
                  fill="#E87D6D"
                />
              ) : (
                <path
                  d="M7.20006 1.8506C4.86031 4.19035 4.76729 7.90559 6.86488 10.3893L6.00904 11.2451C5.42499 10.9587 4.70787 11.0202 4.19438 11.4685L0.701473 14.504C-0.181546 15.2514 -0.240889 16.5987 0.580184 17.4198C1.40604 18.2457 2.75407 18.1744 3.49119 17.3047L6.53111 13.806C6.97967 13.2922 7.04126 12.575 6.75488 11.9909L7.61065 11.1351C10.0944 13.2327 13.8096 13.1397 16.1494 10.8C18.6169 8.33249 18.6169 4.31811 16.1494 1.85064C13.6819 -0.616837 9.66753 -0.616908 7.20006 1.8506ZM14.6578 9.30832C13.0129 10.9532 10.3365 10.9532 8.69156 9.30832C7.04671 7.66347 7.04671 4.98695 8.69156 3.3421C10.3365 1.69725 13.0129 1.69725 14.6578 3.3421C16.3026 4.98695 16.3027 7.66347 14.6578 9.30832Z"
                  fill="#E87D6D"
                />
              )}
            </svg>
          )}
          {!searchOpen && (
            <svg
              width="20"
              height="20"
              viewBox="0 0 12 12"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              style={{
                marginRight: "30px",
              }}
              onClick={() => {
                try {
                  onClose();
                } catch (error) {}
              }}
            >
              <path
                d="M7.08781 6L11.7745 1.31329C12.0751 1.01275 12.0752 0.526114 11.7745 0.225504C11.4739 -0.075129 10.9873 -0.0751993 10.6867 0.225481L6 4.91219L1.31329 0.225504C1.01275 -0.0751056 0.526094 -0.0751993 0.225484 0.225457C-0.0751495 0.526067 -0.075173 1.01263 0.225484 1.31326L4.91215 6L0.22546 10.6867C-0.0744698 10.9866 -0.0744698 11.4746 0.22546 11.7745C0.526047 12.0751 1.01266 12.0752 1.31327 11.7745L5.99995 7.08776L10.6866 11.7744C10.9872 12.0751 11.4738 12.0751 11.7744 11.7745C12.0751 11.4739 12.0751 10.9873 11.7744 10.6866L7.08781 6Z"
                fill="#E87D6D"
              />
            </svg>
          )}
        </label>
      )}
      {navOpen && (
        <div className="navDrawer">
          <img src={spendcryptoLogo} alt="" className="mobileNavLogo" />
          <Link to="/" className={`navItem ${active === "home"}`}>
            Home
          </Link>
          <Link to="/appSelect" className={`navItem ${active === "apps"}`}>
            Apps
          </Link>
          <Link to="/partners" className={`navItem ${active === "partners"}`}>
            Partners
          </Link>
        </div>
      )}
    </nav>
  );
}

export default LandingNavbar;
