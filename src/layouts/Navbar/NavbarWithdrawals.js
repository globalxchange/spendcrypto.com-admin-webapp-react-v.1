import React, { useState, useEffect, useContext } from "react";
import { TabsContext } from "../../context/TabsContext";

function NavbarWithdrawals({
  tabs,
  changeTab,
  onChangeTab,
  dropDownOpen,
  search,
  setSearch,
  button,
  viewSwitcher,
  tabClick,
  searchPlaceHolder,
}) {
  const [tabList, setTabList] = useState([]);
  const [tabSelected, setTabSelected] = useState("");
  const { navTabs } = useContext(TabsContext);

  useEffect(() => {
    setTabList(tabs);
    setTabSelected(tabs[0]);
  }, [tabs]);

  useEffect(() => {
    if (tabSelected) {
      onChangeTab(tabSelected);
    }
  }, [tabSelected]);

  useEffect(() => {
    if (changeTab) {
      setTabSelected(changeTab);
    }
  }, [changeTab]);

  return (
    <nav>
      <div className="bets-dash-navbar">
        {navTabs}
        {button}
      </div>
      <div className="nav-bottom">
        <div className="tab">
          {tabList.map((tabItm) => (
            <div
              className={"tab-itm " + (tabSelected === tabItm)}
              onClick={() => {
                setTabSelected(tabItm);
                tabClick();
              }}
            >
              <h6>{tabItm}</h6>
            </div>
          ))}
        </div>
        {viewSwitcher}
        {dropDownOpen && (
          <input
            type="text"
            className="search"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            placeholder={searchPlaceHolder || "Search"}
          />
        )}
      </div>
    </nav>
  );
}

export default NavbarWithdrawals;
