import React, { useContext } from "react";
import { useParams } from "react-router-dom";
import { TabsContext } from "../../context/TabsContext";

function NavbarVendors({
  tabs,
  tabSelected,
  setTabSelected,
  dropDownOpen,
  search,
  setSearch,
  button,
  viewSwitcher,
  tabClick,
  searchPlaceHolder,
}) {
  const { navTabs } = useContext(TabsContext);
  const { typeOfPage } = useParams();
  return (
    <nav>
      <div className="bets-dash-navbar">
        {navTabs}
        {typeOfPage !== "banker" && button}
      </div>
      <div className="nav-bottom">
        <div className="tab">
          {tabs.map((tabItm) => (
            <div
              className={"tab-itm " + (tabSelected === tabItm)}
              onClick={() => {
                try {
                  setTabSelected(tabItm);
                } catch (error) {}
                try {
                  tabClick();
                } catch (error) {}
              }}
            >
              <h6>{tabItm}</h6>
            </div>
          ))}
        </div>
        {viewSwitcher}
        {dropDownOpen && (
          <input
            type="text"
            className="search"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            placeholder={searchPlaceHolder || "Search"}
          />
        )}
      </div>
    </nav>
  );
}

export default NavbarVendors;
