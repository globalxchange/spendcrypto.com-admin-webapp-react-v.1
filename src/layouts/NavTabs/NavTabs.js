import React, { useContext, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";

import { TabsContext } from "../../context/TabsContext";
import assetsLogoFull from "../../static/images/logos/assetsLogoFull.svg";
import engage from "../../static/images/sidebarIcons/engage.svg";
import issuance from "../../static/images/sidebarIcons/issuance.svg";
import bankersLogo from "../../static/images/sidebarIcons/bankersLogo.svg";

function NavTabs() {
  const history = useHistory();
  const { pathname } = useLocation();
  const { tabs, setTabs, tabIndex, setTabIndex } = useContext(TabsContext);
  useEffect(() => {
    setTabs((tabs) => {
      let tempArr = tabs;
      tempArr[tabIndex] = pathname;
      return tempArr;
    });
  }, [pathname, setTabs, tabIndex]);
  function newTab() {
    setTabs((tabs) => [...tabs, tabs[tabIndex]]);
    setTabIndex(tabs.length);
  }
  function removeTab(i) {
    setTabs((tabs) => tabs.filter((v, index) => index !== i));
    let indexTemp = tabIndex - 1 || 0;
    if (indexTemp < 1) indexTemp = 0;
    history.push(tabs[indexTemp]);
    setTabIndex(indexTemp);
  }
  function getNavHead(tab) {
    switch (true) {
      case /\/AssetsIo/.test(tab):
        return (
          <>
            <div className="navLogoText">
              <img src={assetsLogoFull} className="navLogo" alt="" />
            </div>
            <div className="divider" />
          </>
        );
      case /\/withdrawals/.test(tab):
        return (
          <>
            <div className="navLogoText">
              <img src={bankersLogo} className="navLogo" alt="" />
              <span>Bankers</span>
            </div>
            <div className="divider" />
          </>
        );
      case /\/content/.test(tab):
        return (
          <>
            <div className="navLogoText">
              <img src={engage} className="navLogo" alt="" />
              <span>Engagement</span>
            </div>
            <div className="divider" />
          </>
        );
      case "/" === tab:
        return (
          <>
            <div className="navLogoText">
              <img src={issuance} className="navLogo" alt="" />
              <span>Issuance</span>
            </div>
            <div className="divider" />
          </>
        );
      default:
        break;
    }
  }
  console.log("tabs.length", tabs.length, tabs);
  return (
    <div className="navList">
      {tabs.map((tab, i) => (
        <div
          key={`${i}-${tab}`}
          className={`navBarWrapper ${i === tabIndex}`}
          onClick={() => {
            setTabIndex(i);
            history.push(tab);
          }}
        >
          {getNavHead(tab)}
          {tabs && tabs.length && tabs.length > 1 && (
            <div className="btnClose" onClick={() => removeTab(i)} />
          )}
        </div>
      ))}
      <div className="addNav" onClick={() => newTab()} />
    </div>
  );
}

export default NavTabs;
