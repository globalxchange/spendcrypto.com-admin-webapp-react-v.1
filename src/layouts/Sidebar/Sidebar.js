import React, { useContext } from "react";
import { useHistory, useParams } from "react-router-dom";
import { SidebarClassic } from "@teamforce/broker-admin-dashboards";

import issuance from "../../static/images/sidebarIcons/issuance.svg";
import vendors from "../../static/images/sidebarIcons/vendors.svg";
import funding from "../../static/images/sidebarIcons/funding.svg";
import card from "../../static/images/sidebarIcons/card.svg";
import float from "../../static/images/sidebarIcons/float.svg";
import { ReactComponent as IconVault } from "../../static/images/logos/vaultLogo.svg";
import { MainContext } from "../../context/MainContext";
import logo from "../../static/images/logos/spendIconLogo.svg";
import logoVendor from "../../static/images/loginBgs/vendor.svg";
import logoPartner from "../../static/images/loginBgs/partner.svg";

function Sidebar({ active }) {
  const { login, profilePic, profileName, email, bankerSelected } = useContext(
    MainContext
  );
  const { typeOfPage } = useParams();
  const history = useHistory();
  return (
    <SidebarClassic
      logo={logo}
      logoText={typeOfPage === "partner" ? logoPartner : logoVendor}
      onLogout={() => {
        login();
        history.push("/partners");
      }}
      profilePic={
        typeOfPage !== "banker" ? profilePic : bankerSelected?.profilePicURL
      }
      profileName={
        typeOfPage !== "banker"
          ? profileName
          : bankerSelected?.displayName || bankerSelected?.bankerTag
      }
      email={email}
      active={active}
      groupOneName="Operations"
      groupTwoName="Vaults"
      groupThreeName="Cards"
      groupOneList={[
        {
          name: "Issuance",
          icon: issuance,
          onClick: () => history.push(`/${typeOfPage}`),
        },
        {
          name: typeOfPage === "banker" ? "Transactions" : "Funding",
          icon: funding,
          onClick: () => history.push(`/${typeOfPage}/funding`),
        },
        {
          name: typeOfPage === "banker" ? "CRM" : "Vendors",
          icon: vendors,
          onClick: () => history.push(`/${typeOfPage}/vendors`),
        },
      ]}
      groupTwoList={[
        {
          name: "Vaults",
          svg: <IconVault />,
          onClick: () => history.push(`/${typeOfPage}/liquidVault`),
        },
        {
          name: "Float",
          icon: float,
          onClick: () => {},
        },
      ]}
      groupThreeList={[
        {
          name: "Card Machine",
          icon: card,
          onClick: () => history.push(`/${typeOfPage}/new-card`),
        },
      ]}
      // groupOneFunction,
      // groupTwoFunction,
      // groupThreeFunction
    />
  );
}

export default Sidebar;
