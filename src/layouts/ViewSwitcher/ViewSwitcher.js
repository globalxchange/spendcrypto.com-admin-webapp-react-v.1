import React from "react";
import boards from "../../static/images/clipIcons/boards.svg";
import list from "../../static/images/clipIcons/list.svg";
import search from "../../static/images/clipIcons/search.svg";
import close from "../../static/images/clipIcons/close.svg";

function ViewSwitcher({ isList, setIsList, onClick, isClose }) {
  return (
    <div className="viewSwitcher">
      <div
        className={`btSwitchView ${!isList}`}
        onClick={() => setIsList(false)}
      >
        <img src={boards} alt="" />
      </div>
      <div className={`btSwitchView ${isList}`} onClick={() => setIsList(true)}>
        <img src={list} alt="" />
      </div>
      <div
        className={`btSwitchView`}
        onClick={() => {
          try {
            onClick();
          } catch (error) {}
        }}
      >
        <img className="searchIcn" src={isClose ? close : search} alt="" />
      </div>
    </div>
  );
}

export default ViewSwitcher;
