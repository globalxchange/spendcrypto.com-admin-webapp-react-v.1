import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import Scrollbars from "react-custom-scrollbars";

// import { ReactComponent as AndroidIcon } from "../static/images/platforms/android.svg";
// import { ReactComponent as IosIcon } from "../static/images/platforms/ios.svg";
import appSelectBg from "../static/images/loginBgs/appSelectBg.jpg";
import spendPartnerIcon from "../static/images/spendIcons/spendPartners.svg";
import appOwnerIcon from "../static/images/sidebarIcons/appOwner.svg";
import bankIcon from "../static/images/sidebarIcons/issuingBank.svg";
import paymentProcessor from "../static/images/sidebarIcons/paymentProcessor.svg";
import SendInviteModal from "../components/SendInviteModal/SendInviteModal";
import LandingNavbar from "../layouts/Navbar/LandingNavbar";
import AppSelectSidebar from "../components/AppSelectSidebar/AppSelectSidebar";
import BankerSelectSidebar from "../components/BankerSelectSidebar/BankerSelectSidebar";

function SelectPartnerPage() {
  const history = useHistory();
  const [platform, setPlatform] = useState("");
  const [search, setSearch] = useState("");
  const [appListOpen, setAppListOpen] = useState(false);
  const [bankerListOpen, setBankerListOpen] = useState(false);
  const [login, setLogin] = useState(false);
  return (
    <>
      <div className="selectAppPage partner">
        <LandingNavbar
          menuShow={!login}
          menu={[
            {
              name: "Get In Touch",
              onClick: () => {},
            },
            {
              name: "Login",
              onClick: () => {
                setLogin(true);
              },
            },
          ]}
          search={search}
          setSearch={setSearch}
          showSearch={appListOpen || bankerListOpen}
          placeHolder={
            bankerListOpen ? "Search All Bankers" : "Search All Wallets"
          }
          onClose={() => {
            if (bankerListOpen || appListOpen) {
              setBankerListOpen(false);
              setAppListOpen(false);
            } else {
              setLogin(false);
            }
          }}
        />
        <div className="content">
          <div className="contentArea">
            <div className="space">
              <div className="heading">
                <span className="desktop-view">Partner With Us</span>
              </div>
              <p className="desc">
                SpendCrypto™ Is The Ultimate Two Sided Network. We Work WIth
                Leading Banks Which Then Gets Into Crypto And Then Continuing To
                Fall. Become A SpendPartner Today By Clicking On The Button
                Below
              </p>
              <div
                className="btSpend"
                onClick={() => {
                  history.push("/partnerWithUs");
                }}
              >
                <img className="patnerLogo" src={spendPartnerIcon} alt="" />
              </div>
            </div>
            {/* <div className="fixed-bottom">
              <span className="desktop-foot">
                Download The SpendCrypto™ App Today
              </span>
              <div className="appLink" onClick={() => setPlatform("android")}>
                <div className="insider" />
                <AndroidIcon />
                Android
              </div>
              <div className="appLink" onClick={() => setPlatform("ios")}>
                <div className="insider" />
                <IosIcon />
                iOS
              </div>
              <span className="mobile-foot">Get Your Card Today</span>
            </div> */}
          </div>
          <Scrollbars className="appList">
            {appListOpen ? (
              <AppSelectSidebar search={search} />
            ) : bankerListOpen ? (
              <BankerSelectSidebar search={search} />
            ) : login ? (
              <>
                <div onClick={() => setAppListOpen(true)} className="appItem">
                  <img className="smIcon" src={appOwnerIcon} alt="" />
                  <span>App Owner</span>
                </div>
                <div
                  onClick={() => setBankerListOpen(true)}
                  className="appItem"
                >
                  <img className="smIcon" src={bankIcon} alt="" />
                  <span>Issuing Banks</span>
                </div>
                <div to="/banker" className="appItem disable">
                  <img className="smIcon" src={paymentProcessor} alt="" />
                  <span>Payment Processor</span>
                </div>
              </>
            ) : (
              <img className="appSelectBg" src={appSelectBg} alt="" />
            )}
          </Scrollbars>
        </div>
      </div>
      {platform && (
        <SendInviteModal onClose={() => setPlatform("")} platform={platform} />
      )}
    </>
  );
}

export default SelectPartnerPage;
