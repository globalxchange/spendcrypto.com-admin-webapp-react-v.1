import React from "react";
import { Link, useParams } from "react-router-dom";

import spendCryptoLogo from "../static/images/logos/spendCryptoLogo.svg";

function LandingPage() {
  const { typeOfPage } = useParams();
  return (
    <div className="landingPage">
      <img src={spendCryptoLogo} alt="" className="spendLogo" />
      <Link to={`/${typeOfPage}/login`} className="btLogin">
        Login
      </Link>
      <hr className="dividerH" />
      <div className="btRegister">Register</div>
    </div>
  );
}

export default LandingPage;
