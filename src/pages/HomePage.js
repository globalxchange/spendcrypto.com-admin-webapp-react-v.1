import React, { useState } from "react";
import { ReactComponent as AndroidIcon } from "../static/images/platforms/android.svg";
import { ReactComponent as IosIcon } from "../static/images/platforms/ios.svg";
import SendInviteModal from "../components/SendInviteModal/SendInviteModal";

import s1 from "../static/images/carouselBgs/s1.jpg";
import s2 from "../static/images/carouselBgs/s2.jpg";
import s3 from "../static/images/carouselBgs/s3.jpg";
import sm1 from "../static/images/carouselBgs/sm1.jpg";
import sm2 from "../static/images/carouselBgs/sm2.jpg";
import sm3 from "../static/images/carouselBgs/sm3.jpg";
import LandingNavbar from "../layouts/Navbar/LandingNavbar";
import { Link } from "react-router-dom";

function HomePage() {
  const [index, setIndex] = useState(0);
  // useEffect(() => {
  //   try {
  //     inter = setInterval(() => {
  //       setIndex((index) => (index + 1) % 3);
  //     }, 2000);
  //   } catch (error) {}
  //   return () => {
  //     try {
  //       clearInterval(inter);
  //     } catch (error) {}
  //   };
  // }, []);
  const [platform, setPlatform] = useState("");
  return (
    <>
      <section className="slide-wrapper">
        <LandingNavbar menuShow active="home" />
        <div id="myCarousel" className="carousel slide" data-ride="carousel">
          <div className="col-lg-6">
            <div className="section-head-1 desktop-view">
              <p className="heading">
                <span className="desktop-view">It’s That Simple</span>
              </p>
            </div>
            {/* Indicators */}
            <ol className="carousel-indicators desktop-view">
              <li
                className={index === 0 && "active"}
                onClick={() => setIndex(0)}
              >
                <span>Earn It</span>
              </li>
              <li
                className={index === 1 && "active"}
                onClick={() => setIndex(1)}
              >
                <span>Convert It</span>
              </li>
              <li
                className={index === 2 && "active"}
                onClick={() => setIndex(2)}
              >
                <span>Spend It</span>
              </li>
            </ol>
            <div className="fixed-bottom">
              <span className="desktop-foot">
                Download The SpendCrypto™ App Today
              </span>
              <div className="appLink" onClick={() => setPlatform("android")}>
                <div className="insider" />
                <AndroidIcon />
                Android
              </div>
              <div className="appLink" onClick={() => setPlatform("ios")}>
                <div className="insider" />
                <IosIcon />
                iOS
              </div>
              <span className="mobile-foot">Get Your Card Today</span>
            </div>
          </div>
          <div className="col-lg-6 desktop-view">
            {/* Wrapper for slides */}
            <div className="carousel-inner">
              <div
                className={`carousel-item ${index === 0 && "active"} ${
                  index === 1 && "carousel-item-prev"
                } ${index === 2 && "carousel-item -next"}`}
              >
                <div className="fill">
                  <img src={s1} className="slider-image" alt="" />
                </div>
              </div>
              <div
                className={`carousel-item ${index === 1 && "active"} ${
                  index === 0 && "carousel-item-next"
                } ${index === 2 && "carousel-item-prev"}`}
              >
                <div className="fill">
                  <img src={s2} className="slider-image" alt="" />
                </div>
              </div>
              <div
                className={`carousel-item ${index === 2 && "active"} ${
                  index === 0 && "carousel- item-prev"
                } ${index === 1 && "carousel-item-next"}`}
              >
                <div className="fill">
                  <img src={s3} className="slider-image" alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="mobile-view">
          <div className="s1-content-mobile">
            <div className="s1-mobile-heading">
              <p className="heading">
                <span>
                  We Make It <br />
                  Simple
                </span>
              </p>
            </div>
            <ul>
              <li>
                <div>
                  <p className="mobile-head-title">Earn It</p>
                  <img src={sm1} alt="" />
                </div>
              </li>
              <li>
                <div>
                  <p className="mobile-head-title">Convert It</p>
                  <img src={sm2} alt="" />
                </div>
              </li>
              <li>
                <div>
                  <p className="mobile-head-title">Spend It</p>
                  <img src={sm3} alt="" />
                </div>
              </li>
            </ul>
          </div>
        </div>
        <Link to="/getApp" className="mobileFooter">
          Download App
        </Link>
      </section>
      {platform && (
        <SendInviteModal onClose={() => setPlatform("")} platform={platform} />
      )}
    </>
  );
}

export default HomePage;
