import React, { Fragment, useContext, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { MainContext } from "../context/MainContext";
import { TabsContext } from "../context/TabsContext";
import FundingPage from "./FundingPage";
import IssuancePage from "./IssuancePage";
import LiquidVaultPage from "./LiquidVaultPage";
import NewCardPage from "./NewCardPage";
import VendorsPage from "./VendorsPage";

function SwitcherPage() {
  const { tabIndex, tabs, setTypeOfPage } = useContext(TabsContext);
  const { email } = useContext(MainContext);
  const history = useHistory();
  const { typeOfPage } = useParams();
  useEffect(() => {
    setTypeOfPage(typeOfPage);
    if (typeOfPage === "partner" && !email) {
      history.push("/partners/selectVendorApp");
    }
  }, [email, typeOfPage, history]);
  function getNavHead(tab, i) {
    switch (true) {
      case /\/*\/new-card/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <NewCardPage hide={i !== tabIndex} />
          </Fragment>
        );
      case /\/*\/vendors/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <VendorsPage hide={i !== tabIndex} />
          </Fragment>
        );
      case /\/*\/funding/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <FundingPage hide={i !== tabIndex} />
          </Fragment>
        );
      case /\/*\/liquidVault/.test(tab.location):
        return (
          <Fragment key={tab.key}>
            <LiquidVaultPage hide={i !== tabIndex} />
          </Fragment>
        );

      default:
        return (
          <Fragment key={tab.key}>
            <IssuancePage hide={i !== tabIndex} />
          </Fragment>
        );
    }
  }
  return <>{tabs.map((tab, i) => getNavHead(tab, i))}</>;
}

export default SwitcherPage;
