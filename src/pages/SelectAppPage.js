import React, { useState, useEffect } from "react";
import Axios from "axios";
import { Link, useHistory } from "react-router-dom";
import Scrollbars from "react-custom-scrollbars";

import { ReactComponent as AndroidIcon } from "../static/images/platforms/android.svg";
import { ReactComponent as IosIcon } from "../static/images/platforms/ios.svg";
import spendcryptoLogo from "../static/images/logos/spendCryptoLogo.svg";
import SendInviteModal from "../components/SendInviteModal/SendInviteModal";
import LandingNavbar from "../layouts/Navbar/LandingNavbar";
import IndegrateSpendCryptoModal from "../components/IndegrateSpendCryptoModal/IndegrateSpendCryptoModal";
import Skeleton from "react-loading-skeleton";

function SelectAppPage() {
  const history = useHistory();
  const [appList, setAppList] = useState([]);
  const [platform, setPlatform] = useState("");
  const [search, setSearch] = useState("");
  const [indegrateModal, setIndegrateModal] = useState(false);
  const [appListObject, setAppListObject] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    Axios.get(
      "https://comms.globalxchange.com/gxb/apps/get?group_id=66me9vdki2husjy"
    ).then(({ data }) => {
      if (data.status) {
        let tempObj = {};
        data.apps.forEach((app) => {
          tempObj[app.app_code] = app;
        });
        setAppListObject(tempObj);
        Axios.get("https://comms.globalxchange.com/coin/sc/partner/get")
          .then(({ data }) => {
            if (data.status) {
              setAppList(data.partners);
            }
          })
          .finally(() => {
            setLoading(false);
          });
      }
    });
  }, []);
  return (
    <>
      <div className="selectAppPage">
        <LandingNavbar
          active="apps"
          search={search}
          setSearch={setSearch}
          showSearch
          placeHolder="Search All Wallets"
          onClose={() => history.goBack()}
        />
        <div className="content">
          <div className="contentArea">
            <div className="space">
              <div className="heading">
                <div className="desktop-view">
                  Spend From Any <br />
                  Of These Apps
                </div>
              </div>
              <p className="desc">
                SpendCrypto™ Integrates With Leading Cryptocurrency Wallets So
                That Users From All Over The Crypto Universe Can Seemlessly
                Spend Their Crypto. See If Your App Is Compatible With
                SpendCrypto
              </p>
              <div className="btnsSc">
                <div
                  className="btnMain"
                  onClick={() => setIndegrateModal(true)}
                >
                  <div className="insider" />
                  Integrate
                  <img src={spendcryptoLogo} alt="" className="logo" />
                </div>
                {/* <div className="btnSecond">
                  <div className="insider" />
                  Refer
                  <img src={spendcryptoLogo} alt="" className="logo" />
                </div> */}
              </div>
            </div>
            <div className="fixed-bottom">
              <span className="desktop-foot">
                Download The SpendCrypto™ App Today
              </span>
              <div className="appLink" onClick={() => setPlatform("android")}>
                <div className="insider" />
                <AndroidIcon />
                Android
              </div>
              <div className="appLink" onClick={() => setPlatform("ios")}>
                <div className="insider" />
                <IosIcon />
                iOS
              </div>
              <span className="mobile-foot">Get Your Card Today</span>
            </div>
          </div>
          <Scrollbars className="appList">
            {loading
              ? Array(6)
                  .fill("")
                  .map((a, i) => (
                    <div className="appItem">
                      <Skeleton className="icon" width={240} height={60} />
                    </div>
                  ))
              : appList &&
                appList
                  .filter(
                    (app) =>
                      appListObject[app.app_code]?.app_name
                        ?.toLowerCase()
                        .includes(search?.toLowerCase()) ||
                      app.app_code
                        ?.toLowerCase()
                        .includes(search?.toLowerCase())
                  )
                  .map((app) => (
                    <Link to={`/cards/${app.app_code}`} className="appItem">
                      <img
                        src={appListObject[app.app_code].colouredlogo}
                        alt={app.app_code}
                        className="icon"
                      />
                    </Link>
                  ))}
          </Scrollbars>
        </div>
        <Link to="/getApp" className="mobileFooter">
          Download App
        </Link>
      </div>
      {platform && (
        <SendInviteModal onClose={() => setPlatform("")} platform={platform} />
      )}
      {indegrateModal && (
        <IndegrateSpendCryptoModal onClose={() => setIndegrateModal(false)} />
      )}
    </>
  );
}

export default SelectAppPage;
