import React, { useState, useContext, useEffect } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import Axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { faEye, faEyeSlash } from "@fortawesome/free-regular-svg-icons";

import { MainContext } from "../context/MainContext";
import LoginProgressBar from "../components/LoginProgressBar/LoginProgressBar";
import bgVendor from "../static/images/loginBgs/bgVendor.jpg";
import bgPartner from "../static/images/loginBgs/bgPartner.jpg";
import { ReactComponent as TypeLogo } from "../static/images/loginBgs/typeLogo.svg";
import NotValidUser from "../components/NotValidUser/NotValidUser";

function LoginPage() {
  const history = useHistory();
  const { typeOfPage } = useParams();
  const [emailid, setEmailId] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [inValid, setInValid] = useState(false);
  const { login, tostShowOn, vendorEmail, vendorPin } = useContext(MainContext);
  useEffect(() => {
    if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(vendorEmail)) {
      setEmailId(vendorEmail);
    }
  }, [vendorEmail]);
  useEffect(() => {
    if (typeOfPage === "partner")
      if (!(vendorEmail || vendorPin)) {
        history.push("/partner");
      }
  }, [vendorEmail, vendorPin, typeOfPage, history]);
  const [progress, setProgress] = useState(0);
  const config = {
    onUploadProgress: function (progressEvent) {
      setProgress(
        Math.round((progressEvent.loaded * 100) / progressEvent.total)
      );
    },
  };

  const loginValidate = (e) => {
    e.preventDefault();
    Axios.get(
      `https://comms.globalxchange.com/coin/sc/users/get?email=${emailid}`
    ).then(({ data }) => {
      if (data.status) {
        if (typeOfPage === "partner") {
          if (data?.users[0]?.sc_partner) {
            loginUser();
          } else {
            setInValid(true);
          }
        } else {
          if (data?.users[0]?.sc_vendor) {
            loginUser();
          } else {
            setInValid(true);
          }
        }
      }
    });
  };

  const loginUser = () => {
    if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(emailid)) {
      setLoading(true);
      Axios.post(
        "https://gxauth.apimachine.com/gx/user/login",
        {
          email: emailid || vendorEmail,
          password,
        },
        config
      )
        .then((response) => {
          const { data } = response;
          if (data.status) {
            login(emailid, data.accessToken, data.idToken);
            // tostShowOn(data.message);
            Axios.post(
              "https://comms.globalxchange.com/gxb/apps/register/user",
              {
                email: emailid, // user email
                app_code: "spendcrypto", // app_code
              }
            );
            history.push(`/${typeOfPage}`);
          } else {
            tostShowOn(data.message);
          }
        })
        .catch((error) => {
          tostShowOn(error.message ? error.message : "Some Thing Went Wrong!");
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      tostShowOn("Enter Valid EmailId");
    }
  };
  return (
    <div className="loginPage">
      <div
        className="bgWrap"
        style={{
          backgroundImage: `url(${
            typeOfPage === "banker" ? bgVendor : bgPartner
          })`,
        }}
      />
      <div className="loginSidewrap">
        {inValid ? (
          <NotValidUser typeOfPage={typeOfPage} setInValid={setInValid} />
        ) : (
          <div className="loginWrapper">
            <div className="logoWrap">
              <TypeLogo className={`logo ${typeOfPage}`} />
            </div>
            <form className="login-form mx-5" onSubmit={loginValidate}>
              <div className="group">
                <input
                  type="text"
                  name="email"
                  className={`${Boolean(vendorEmail)}`}
                  readOnly={Boolean(vendorEmail)}
                  value={emailid}
                  onChange={(e) => setEmailId(e.target.value)}
                  required="required"
                />
                <span className="highlight" />
                <span className="bar" />
                <label>Email</label>
              </div>
              <div className="group">
                <input
                  type={showPassword ? "text" : "password"}
                  name="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required="required"
                />
                <span className="highlight" />
                <span className="bar" />
                <FontAwesomeIcon
                  className="eye"
                  onClick={() => {
                    setShowPassword(!showPassword);
                  }}
                  icon={showPassword ? faEyeSlash : faEye}
                />
                <label>Password</label>
              </div>
              <div className="group">
                <button type="submit" disabled={loading}>
                  {loading ? (
                    <FontAwesomeIcon icon={faSpinner} spin />
                  ) : (
                    "Login"
                  )}
                </button>
              </div>
            </form>
          </div>
        )}{" "}
        <Link to="/" className="goHome">
          Go Home
        </Link>
      </div>
      {loading && <LoginProgressBar progress={progress} />}
    </div>
  );
}

export default LoginPage;
