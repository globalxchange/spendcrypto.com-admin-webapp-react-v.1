import React, { useEffect, useState, useContext } from "react";
import Scrollbars from "react-custom-scrollbars";
import { useParams } from "react-router-dom";
import Axios from "axios";
import { IconsViewSwitcher, Navbar } from "@teamforce/broker-admin-dashboards";

import MainLayout from "../layouts/MainLayout";
import crown from "../static/images/coinIcon/crownCoin.svg";
import IssuanceFeed from "../components/IssuanceFeed/IssuanceFeed";
import { MainContext } from "../context/MainContext";
import { TabsContext } from "../context/TabsContext";

const tabs = ["Pending", "Completed", "Rejected", "All"];
function IssuancePage({ hide }) {
  const { appSelected, setAppSelected, bankerSelected } = useContext(
    MainContext
  );
  const { navTabs } = useContext(TabsContext);

  const { typeOfPage } = useParams();
  const [tabSelected, setTabSelected] = useState("Pending");
  const [search, setSearch] = useState("");
  const [isList, setIsList] = useState(false);
  const [searchStr, setSearchStr] = useState("");

  useEffect(() => {
    setSearchStr("");
  }, [tabSelected]);

  // App List Data
  const [appList, setAppList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [appListObject, setAppListObject] = useState({});
  const [dropDownOpen, setDropDownOpen] = useState(false);

  useEffect(() => {
    setLoading(true);
    Axios.get("https://comms.globalxchange.com/gxb/apps/get")
      .then(({ data }) => {
        if (data.status) {
          setAppList(data.apps);
          let tempObj = {};
          data.apps.forEach((app) => {
            tempObj[app.app_code] = app;
          });
          setAppListObject(tempObj);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const [filterOn, setFilterOn] = useState();

  return (
    <MainLayout hide={hide} active={"Issuance"} onNew={() => {}}>
      <Navbar
        navTabs={navTabs}
        tabs={tabs}
        tabSelected={tabSelected}
        setTabSelected={(tab) => setTabSelected(tab)}
        search
        setSearch={() => {}}
        button={
          typeOfPage === "partner" && (
            <div className={`publication `}>
              <img
                className="logoIcn"
                src={
                  typeOfPage === "partner"
                    ? appSelected.app_icon
                    : bankerSelected?.profilePicURL || crown
                }
                alt=""
              />
              <span>
                {typeOfPage === "partner"
                  ? appSelected.app_name
                  : bankerSelected?.displayName || bankerSelected?.bankerTag}
              </span>
            </div>
          )
        }
        viewSwitcher={
          <IconsViewSwitcher
            list={isList}
            board={!isList}
            onBoardClick={() => setIsList(false)}
            onListClick={() => setIsList(true)}
          />
        }
        tabClick={() => {}}
        searchPlaceHolder=""
      />
      <div className="withdrawalView">
        <IssuanceFeed
          selectedApp={appSelected}
          isList={isList}
          appListObject={appListObject}
          tabSelected={tabSelected}
          dropDownOpen={dropDownOpen}
          setAppListOpen={setDropDownOpen}
          filterOn={filterOn}
        />
        {dropDownOpen && (
          <Scrollbars
            autoHide
            className="sideBarAppSelect"
            renderView={(props) => <div {...props} className="view" />}
          >
            <label className="headSearach">
              <input
                type="text"
                placeholder="Search Apps..|"
                className="inpSearch"
                value={search}
                onChange={(e) => setSearch(e.target.value)}
              />
            </label>
            {appList
              .filter((app) =>
                app.app_name.toLowerCase().includes(search.toLowerCase())
              )
              .map((app) => (
                <div
                  className="appPublication"
                  key={app._id}
                  onClick={() => {
                    setAppSelected(app);
                    setDropDownOpen(false);
                  }}
                >
                  <img src={app.app_icon} alt="" className="pubLogo" />
                  <div className="nameNbtns">
                    <div className="name">{app.app_name}</div>
                    <div className="btns">
                      <div className="btnAction">Copy</div>
                      <div className="btnAction">Expand</div>
                    </div>
                  </div>
                </div>
              ))}
            <div
              className="showAllApp"
              onClick={() => {
                setAppSelected("");
                setDropDownOpen(false);
              }}
            >
              Show All Apps
            </div>
          </Scrollbars>
        )}
      </div>
    </MainLayout>
  );
}

export default IssuancePage;
