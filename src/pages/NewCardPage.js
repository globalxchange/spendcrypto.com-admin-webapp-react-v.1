import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import MainLayout from "../layouts/MainLayout";
import crown from "../static/images/coinIcon/crownCoin.svg";
import NewCardComponent from "../components/NewCardComponent/NewCardComponent";
import MyCards from "../components/MyCards/MyCards";
import { MainContext } from "../context/MainContext";
import VendorsFilter from "../components/VendorsFilter/VendorsFilter";
import Scrollbars from "react-custom-scrollbars";
import Axios from "axios";
import { IconsViewSwitcher, Navbar } from "@teamforce/broker-admin-dashboards";
import { TabsContext } from "../context/TabsContext";

const tabs = ["My Cards", "Rewards", "Analytics", "New"];
function NewCardPage({ hide }) {
  const { appSelected, bankerSelected, setAppSelected } = useContext(
    MainContext
  );
  const { navTabs } = useContext(TabsContext);
  const { typeOfPage } = useParams();
  const [tabSelected, setTabSelected] = useState("My Cards");
  const [search, setSearch] = useState("");
  const [isList, setIsList] = useState(true);

  const [searchStr, setSearchStr] = useState("");
  const [dropDownOpen, setDropDownOpen] = useState(false);

  // App List Data
  const [appList, setAppList] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true);
    Axios.get("https://comms.globalxchange.com/gxb/apps/get")
      .then(({ data }) => {
        if (data.status) {
          setAppList(data.apps);
          !appSelected && setAppSelected(data.apps[0]);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, [appSelected, setAppSelected]);

  function getContent() {
    switch (tabSelected) {
      case "New":
        return <NewCardComponent />;
      case "My Cards":
        return <MyCards />;

      default:
        break;
    }
  }
  const [searchOn, setSearchOn] = useState(false);
  return (
    <MainLayout hide={hide} active={"Card Machine"} onNew={() => {}}>
      <Navbar
        navTabs={navTabs}
        tabs={tabs}
        tabSelected={tabSelected}
        setTabSelected={(tab) => setTabSelected(tab)}
        search
        setSearch={() => {}}
        button={
          typeOfPage === "partner" && (
            <div className={`publication `}>
              <img
                className="logoIcn"
                src={
                  typeOfPage === "partner"
                    ? appSelected.app_icon
                    : bankerSelected?.profilePicURL || crown
                }
                alt=""
              />
              <span>
                {typeOfPage === "partner"
                  ? appSelected.app_name
                  : bankerSelected?.displayName || bankerSelected?.bankerTag}
              </span>
            </div>
          )
        }
        viewSwitcher={
          <IconsViewSwitcher
            search={searchOn}
            onBoardClick={() => setIsList(false)}
            onListClick={() => setIsList(true)}
            onSearchClick={() => setSearchOn(!searchOn)}
          />
        }
        tabClick={() => {}}
        searchPlaceHolder=""
      />
      {searchOn && (
        <VendorsFilter
          setSearchStr={setSearchStr}
          searchStr={searchStr}
          placeholder="Search All Cards"
          filterOne={appSelected.app_name || "All"}
          filterTwo={"All"}
          filterOneClick={() => {
            if (typeOfPage === "banker") {
              setDropDownOpen(!dropDownOpen);
            }
          }}
          filterTwoClick={() => {}}
        />
      )}
      <div className="cardPageWrapper">
        {getContent()}
        {dropDownOpen && (
          <Scrollbars
            autoHide
            className="sideBarAppSelect"
            renderView={(props) => <div {...props} className="view" />}
          >
            <label className="headSearach">
              <input
                type="text"
                placeholder="Search Apps..|"
                className="inpSearch"
                value={search}
                onChange={(e) => setSearch(e.target.value)}
              />
            </label>
            {appList
              .filter((app) =>
                app.app_name.toLowerCase().includes(search.toLowerCase())
              )
              .map((app) => (
                <div
                  className="appPublication"
                  key={app._id}
                  onClick={() => {
                    setAppSelected(app);
                    setDropDownOpen(false);
                  }}
                >
                  <img src={app.app_icon} alt="" className="pubLogo" />
                  <div className="nameNbtns">
                    <div className="name">{app.app_name}</div>
                    <div className="btns">
                      <div className="btnAction">Copy</div>
                      <div className="btnAction">Expand</div>
                    </div>
                  </div>
                </div>
              ))}
          </Scrollbars>
        )}
      </div>
    </MainLayout>
  );
}

export default NewCardPage;
