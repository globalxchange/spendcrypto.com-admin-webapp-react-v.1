import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import { Redirect, useHistory, useParams } from "react-router-dom";
import { MainContext } from "../context/MainContext";
import logo from "../static/images/logos/spendCryptoLogo.svg";

function SelectVendorAppPage() {
  const { setAppSelected, email } = useContext(MainContext);
  const history = useHistory();
  const { typeOfPage } = useParams();
  const [search, setSearch] = useState("");
  const [appList, setAppList] = useState([]);
  useEffect(() => {
    Axios.get("https://comms.globalxchange.com/gxb/apps/get").then(
      ({ data }) => {
        if (data.status) setAppList(data.apps);
      }
    );
  }, []);
  return (
    <>
      {email === "" ? <Redirect to={`/${typeOfPage}/landing`} /> : ""}
      <div className="selectVendorAppPage">
        <div className="sideInput">
          <div className="content">
            <img src={logo} alt="" className="logoMain" />
            <div className="label">Enter The Name Of You Brand</div>
            <input
              type="text"
              placeholder="Ex. VSA"
              className="appSearch"
              value={search}
              onChange={(e) => setSearch(e.target.value)}
            />
            <div className="btnLogin">Login</div>
          </div>
          <div className="footer">
            Don’t Have An Account. &nbsp;<span>Click Here</span>
          </div>
        </div>
        <Scrollbars
          className="sideLists"
          renderThumbHorizontal={() => <div />}
          renderThumbVertical={() => <div />}
        >
          <div className="headStick">
            <div className="title">All Our Crypto Brands</div>
            <div className="subTitle">Click On Your Brand To Enter</div>
          </div>
          {appList
            .filter(
              (app) =>
                app.app_name.toLowerCase().includes(search.toLowerCase()) ||
                app.app_code.toLowerCase().includes(search.toLowerCase())
            )
            .map((app) => (
              <div
                className="appItem"
                key={app._id}
                onClick={() => {
                  setAppSelected(app);
                  history.push(`/${typeOfPage}`);
                }}
              >
                <img src={app.app_icon} className="appIcon" alt="" />
                <div className="texts">
                  <div className="appName">{app.app_name}</div>
                  <div className="desc">{app.short_description}</div>
                </div>
              </div>
            ))}
        </Scrollbars>
      </div>
    </>
  );
}

export default SelectVendorAppPage;
