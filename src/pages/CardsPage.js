import Axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { useHistory, useParams } from "react-router-dom";

import MasterCardSvg from "../components/MasterCardSvg/MasterCardSvg";
import SendInviteModal from "../components/SendInviteModal/SendInviteModal";
import SendInviteModalPlatform from "../components/SendInviteModal/SendInviteModalPlatform";

import { ReactComponent as PoweredBy } from "../static/images/cardIcons/poweredBy.svg";
import spendCryptoLogo from "../static/images/logos/spendCryptoWhite.svg";

function CardsPage() {
  const history = useHistory();
  const { card } = useParams();
  const [platform, setPlatform] = useState("");
  const [modalOpen, setModalOpen] = useState(false);
  const [index, setIndex] = useState(0);
  const [res, setRes] = useState({});
  useEffect(() => {
    Axios.get(
      `https://comms.globalxchange.com/gxb/apps/get?app_code=${card}`
    ).then(({ data }) => {
      if (data.status) {
        setRes(data.apps[0]);
      }
    });
    return () => {};
  }, [card]);
  const isPrev = (i) => {
    if (index === 0) {
      if (i === 4 - 1) return true;
    } else {
      if (i === index - 1) return true;
    }
    return false;
  };
  const isNext = (i) => {
    if (index === 4 - 1) {
      if (i === 0) return true;
    } else {
      if (i === index + 1) return true;
    }
    return false;
  };

  const ref = useRef();
  useEffect(() => {
    ref.current.style.setProperty(
      "--card-color",
      res?.color_codes && res?.color_codes[0]
        ? `${res?.color_codes[0]}`
        : "#484848"
    );
    return () => {};
  }, [res]);

  return (
    <div className="cardsPage" ref={ref}>
      <div className="headLogoArea">
        <div className="logoWrapper">
          <img className="logo" src={res?.colouredlogo} alt="" />
          <PoweredBy className="poweredBy" />
        </div>
      </div>
      <div className="carousel-wrapper">
        <div className="carousel-horizontal">
          {Array(4)
            .fill("")
            .map((app, i) => (
              <MasterCardSvg
                logo={res?.spendcryptocardlogo1 || spendCryptoLogo}
                color={
                  res?.[`scc${i + 1}backgroundcolor`]
                    ? `#${res?.[`scc${i + 1}backgroundcolor`]}`
                    : "#050505"
                }
                gradient={!res?.[`scc${i + 1}backgroundcolor`] && i}
                onClick={() => setIndex(i)}
                className={
                  index === i
                    ? "active"
                    : isPrev(i)
                    ? "prev"
                    : isNext(i)
                    ? "next"
                    : "inactive"
                }
              />
            ))}
        </div>
      </div>
      <div className="desc">
        {res?.long_description?.slice(0, 100)} <span>More...</span>
      </div>
      <div className="btnRegister" onClick={() => setModalOpen(true)}>
        <span>Spend With</span>
        <img className="btnLogo" src={res?.colouredlogo} alt="" />
      </div>
      {modalOpen ? (
        <>
          {platform ? (
            <SendInviteModal
              onClose={() => setModalOpen(false)}
              platform={platform}
            />
          ) : (
            <SendInviteModalPlatform
              onClose={() => setModalOpen(false)}
              setPlatform={setPlatform}
              appName={res.app_name}
            />
          )}
        </>
      ) : (
        ""
      )}
      <div className="closeBtn" onClick={() => history.push("/appSelect")} />
    </div>
  );
}

export default CardsPage;
