import { IconsViewSwitcher, Navbar } from "@teamforce/broker-admin-dashboards";
import React, { useContext, useState } from "react";
import { useParams } from "react-router-dom";
import VaultControlls from "../components/VaultsPage/VaultControlls";
import VaultFab from "../components/VaultsPage/VaultFab";
import VaultPageHead from "../components/VaultsPage/VaultPageHead";
import VaultTransactionTable from "../components/VaultsPage/VaultTransactionTable";
import { MainContext } from "../context/MainContext";
import { TabsContext } from "../context/TabsContext";
import VaultContextProvider from "../context/VaultContext";

import MainLayout from "../layouts/MainLayout";
import NavbarVendors from "../layouts/Navbar/NavbarVendors";
import crown from "../static/images/coinIcon/crownCoin.svg";

const tabs = ["Crypto", "Fiat"];
function LiquidVaultPage({ hide }) {
  const { appSelected, bankerSelected } = useContext(MainContext);
  const { navTabs } = useContext(TabsContext);
  const { typeOfPage } = useParams();
  const [tabSelected, setTabSelected] = useState("Crypto");
  const [search, setSearch] = useState("");

  return (
    <VaultContextProvider>
      <MainLayout hide={hide} active={"Vaults"} onNew={() => {}}>
        <Navbar
          navTabs={navTabs}
          tabs={tabs}
          tabSelected={tabSelected}
          setTabSelected={(tab) => setTabSelected(tab)}
          search
          setSearch={() => {}}
          button={
            typeOfPage === "partner" && (
              <div className={`publication `}>
                <img
                  className="logoIcn"
                  src={
                    typeOfPage === "partner"
                      ? appSelected.app_icon
                      : bankerSelected?.profilePicURL || crown
                  }
                  alt=""
                />
                <span>
                  {typeOfPage === "partner"
                    ? appSelected.app_name
                    : bankerSelected?.displayName || bankerSelected?.bankerTag}
                </span>
              </div>
            )
          }
          viewSwitcher={<IconsViewSwitcher />}
          tabClick={() => {}}
          searchPlaceHolder=""
        />
        <div className="liquidVaultContent">
          <VaultPageHead tabSelected={tabSelected} />
          <VaultControlls />
          <VaultTransactionTable />
          <VaultFab />
        </div>
      </MainLayout>
    </VaultContextProvider>
  );
}

export default LiquidVaultPage;
