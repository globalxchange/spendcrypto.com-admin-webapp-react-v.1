import React, { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Axios from "axios";

import RegisterSelectUserLevel from "../components/RegisterSteps/RegisterSelectUserLevel";
import RegisterSelectUserType from "../components/RegisterSteps/RegisterSelectUserType";
import registerBg from "../static/images/loginBgs/registerBg.jpg";
import bgPartner from "../static/images/loginBgs/bgPartner.jpg";
import RegisterAllUserTypes from "../components/RegisterSteps/RegisterAllUserTypes";
import RegisterGXUser from "../components/RegisterSteps/RegisterGXUser";
import RegisterIsAgenncy from "../components/RegisterSteps/RegisterIsAgenncy";
import RegisterSorryOperator from "../components/RegisterSteps/RegisterSorryOperator";
import RegisterSelectApp from "../components/RegisterSteps/RegisterSelectApp";
import RegisterLXApp from "../components/RegisterSteps/RegisterLXApp";
import partnerBg from "../static/images/loginBgs/partnerBg.jpg";
import vendorBg from "../static/images/loginBgs/vendorBg.jpg";
import { MainContext } from "../context/MainContext";
import LoginProgressBar from "../components/LoginProgressBar/LoginProgressBar";
import RegisterSuccess from "../components/RegisterSteps/RegisterSuccess";

function RegisterPage() {
  const history = useHistory();
  const [userType, setUserType] = useState("");
  const [userLevel, setUserLevel] = useState("");
  const [isAgency, setIsAgency] = useState("");
  const [appCode, setAppCode] = useState("");
  const [lxData, setLxData] = useState("");
  const { login, tostShowOn, email, token, profileName } = useContext(
    MainContext
  );
  useEffect(() => {
    login();
  }, []);
  const [loadingMessage, setLoadingMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const [progress, setProgress] = useState(0);
  const [succesGoPage, setSuccesGoPage] = useState("");
  const config = {
    onUploadProgress: function (progressEvent) {
      setProgress(
        Math.round((progressEvent.loaded * 100) / progressEvent.total)
      );
    },
  };
  const loginValidate = (email, password, app) => {
    if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      setLoadingMessage(`Logging Into ${app || "GX"} Account`);
      setLoading(true);
      Axios.post(
        "https://gxauth.apimachine.com/gx/user/login",
        {
          email,
          password,
        },
        config
      )
        .then((response) => {
          const { data } = response;
          if (data.status) {
            login(email, data.accessToken, data.idToken);
            Axios.post(
              "https://comms.globalxchange.com/gxb/apps/register/user",
              {
                email: email, // user email
                app_code: "spendcrypto", // app_code
                fromAppCreation: true,
              }
            );
            tostShowOn(data.message);
          }
        })
        .catch((error) => {
          tostShowOn(error.message ? error.message : "Some Thing Went Wrong!");
        })
        .finally(() => {
          setLoadingMessage("");
          setLoading(false);
        });
    } else {
      tostShowOn("Enter Valid EmailId");
    }
  };

  useEffect(() => {
    if (email && token && userType && userLevel) {
      const getDetail = async () => {
        if (userType === "vendor") {
          setLoading(true);
          if ((userLevel === "nota" || userLevel === "gx") && lxData.dpLink) {
            await becomeLxUser();
            await becomeBanker();
            const { sc_profile_id, banker_profile_id } = await getProfileData();
            becomeVendor(sc_profile_id, banker_profile_id);
          } else if (userLevel === "banker") {
            const { sc_profile_id, banker_profile_id } = await getProfileData();
            becomeVendor(sc_profile_id, banker_profile_id);
          }
        }
        setLoading(false);
      };
      getDetail();
    }
  }, [email, token, userType, userLevel, lxData]);

  useEffect(() => {
    if (email && token && userType && appCode) {
      const getDetail = async () => {
        if (userType === "partner" && appCode) {
          setLoading(true);
          const { sc_profile_id } = await getProfileData();
          becomePartner(sc_profile_id);
        }
        setLoading(false);
      };
      getDetail();
    }
  }, [email, token, userType, appCode]);

  const becomeLxUser = async () => {
    setLoadingMessage("Creating LX Profile");
    const { data } = await Axios.post(
      "https://teller2.apimachine.com/lxUser/register",
      {
        profilePicURL: lxData.dpLink,
        firstName: lxData.fName,
        lastName: lxData.lName,
        lxTag: profileName || `${lxData.fName}${lxData.lName}`,
      },
      {
        headers: {
          email,
          token,
        },
        ...config,
      }
    );
    setLoadingMessage("");
  };

  const becomeBanker = async () => {
    setLoadingMessage("Creating Banker Profile");
    const { data } = await Axios.post(
      "https://teller2.apimachine.com/lxUser/register/banker/noDetails",
      {},
      {
        headers: {
          email,
          token,
        },
        ...config,
      }
    );
    setLoadingMessage("");
  };

  const getProfileData = async () => {
    setLoadingMessage("Getting User Data");
    const { data } = await Axios.get(
      `https://comms.globalxchange.com/user/details/get?email=${email}`,
      config
    );
    setLoadingMessage("");
    if (data.status)
      return {
        sc_profile_id: data.user.spendcrypto_profile_id,
        banker_profile_id: data.user.bankerapp_profile_id,
      };
    else return {};
  };

  const becomeVendor = async (sc_profile_id, banker_profile_id) => {
    setLoadingMessage("Genarating Vendor Profile");
    await Axios.post(
      "https://comms.globalxchange.com/coin/sc/vendor/register",
      {
        email,
        sc_profile_id,
        banker_profile_id,
      },
      config
    )
      .then(({ data }) => {
        if (data.status) {
          tostShowOn("Regitration Success As Vendor");
          setSuccesGoPage("/bank");
        } else {
          tostShowOn(data.message);
        }
      })
      .finally(() => {
        setLoadingMessage("");
      });
  };

  const becomePartner = async (sc_profile_id) => {
    setLoadingMessage("Genarating Partner Profile");
    Axios.post(
      "https://comms.globalxchange.com/coin/sc/partner/register",
      {
        email,
        partner_appCode: appCode,
        sc_profile_id,
      },
      config
    )
      .then(({ data }) => {
        if (data.status) {
          tostShowOn("Regitration Success As Partner");
          setSuccesGoPage("/partner");
        } else {
          tostShowOn(data.message);
        }
      })
      .finally(() => {
        setLoadingMessage("");
      });
  };

  function getStep() {
    switch (true) {
      case Boolean(succesGoPage):
        return (
          <RegisterSuccess userType={userType} succesGoPage={succesGoPage} />
        );
      case isAgency === "yes":
        return (
          <RegisterSelectApp
            loginValidate={loginValidate}
            setAppCode={setAppCode}
            onClose={() => {
              setIsAgency("");
            }}
          />
        );
      case isAgency === "no":
        return (
          <RegisterSorryOperator
            onClose={() => {
              setIsAgency("");
            }}
          />
        );
      case userType === "partner":
        return (
          <RegisterIsAgenncy
            setIsAgency={setIsAgency}
            onClose={() => {
              setUserType("");
            }}
          />
        );
      case userLevel === "banker":
        return (
          <RegisterGXUser
            loginValidate={loginValidate}
            app="Banker"
            onClose={() => {
              setUserLevel("");
            }}
          />
        );
      case userLevel === "gx" && Boolean(email):
        return <RegisterLXApp emailId={email} setLxData={setLxData} />;
      case userLevel === "gx":
        return (
          <RegisterGXUser
            loginValidate={loginValidate}
            app="GX"
            onClose={() => {
              setUserLevel("");
            }}
          />
        );
      case userLevel === "nota":
        return (
          <RegisterAllUserTypes
            loginValidate={loginValidate}
            setLxData={setLxData}
            config={config}
            setLoading={setLoading}
            setLoadingMessage={setLoadingMessage}
            onClose={() => setUserLevel("")}
          />
        );
      case userType === "vendor":
        return (
          <RegisterSelectUserLevel
            onClose={() => {
              setUserType("");
            }}
            setUserLevel={setUserLevel}
          />
        );
      default:
        return <RegisterSelectUserType setUserType={setUserType} />;
    }
  }
  const getBg = () => {
    if (Boolean(succesGoPage)) {
      if (userType === "partner") {
        return partnerBg;
      } else {
        return vendorBg;
      }
    } else {
      if (userLevel === "nota") {
        return bgPartner;
      } else {
        return registerBg;
      }
    }
  };
  return (
    <div className={`registerPage ${Boolean(succesGoPage)}`}>
      <img src={getBg()} alt="" className="bgWrap" />
      <div className="contentWrap">
        {getStep()}
        {loading && (
          <LoginProgressBar message={loadingMessage} progress={progress} />
        )}
      </div>
    </div>
  );
}

export default RegisterPage;
