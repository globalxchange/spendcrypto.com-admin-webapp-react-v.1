import React, { useEffect, useState, useContext } from "react";
import Scrollbars from "react-custom-scrollbars";
import Axios from "axios";
import { useParams } from "react-router-dom";

import MainLayout from "../layouts/MainLayout";
import NavbarVendors from "../layouts/Navbar/NavbarVendors";
import ViewSwitcher from "../layouts/ViewSwitcher/ViewSwitcher";
import crown from "../static/images/coinIcon/crownCoin.svg";
import IssuanceFunding from "../components/IssuanceFunding/IssuanceFunding";
import { MainContext } from "../context/MainContext";

const tabs = ["Pending", "Completed", "Rejected", "All"];
function FundingPage({ hide }) {
  const { appSelected, setAppSelected, bankerSelected } = useContext(
    MainContext
  );
  const { typeOfPage } = useParams();
  const [tabSelected, setTabSelected] = useState("Pending");
  const [search, setSearch] = useState("");
  const [isList, setIsList] = useState(false);
  const [searchStr, setSearchStr] = useState("");

  useEffect(() => {
    setSearchStr("");
  }, [tabSelected]);

  // App List Data
  const [appList, setAppList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [appListObject, setAppListObject] = useState({});
  const [dropDownOpen, setDropDownOpen] = useState(false);

  useEffect(() => {
    setLoading(true);
    Axios.get("https://comms.globalxchange.com/gxb/apps/get")
      .then(({ data }) => {
        if (data.status) {
          setAppList(data.apps);
          let tempObj = {};
          data.apps.forEach((app) => {
            tempObj[app.app_code] = app;
          });
          setAppListObject(tempObj);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const [filterOn, setFilterOn] = useState(false);

  return (
    <MainLayout
      hide={hide}
      active={typeOfPage === "banker" ? "Transactions" : "Funding"}
      onNew={() => {}}
    >
      <NavbarVendors
        tabs={tabs}
        tabSelected={tabSelected}
        setTabSelected={setTabSelected}
        dropDownOpen={false}
        search={search}
        setSearch={setSearch}
        button={
          <div className={`publication `}>
            <img
              className="logoIcn"
              src={
                typeOfPage === "partner"
                  ? appSelected.app_icon
                  : bankerSelected?.profilePicURL || crown
              }
              alt=""
            />
            <span>
              {typeOfPage === "partner"
                ? appSelected.app_name
                : bankerSelected?.displayName || bankerSelected?.bankerTag}
            </span>
          </div>
        }
        viewSwitcher={
          <ViewSwitcher
            isList={isList}
            setIsList={setIsList}
            onClick={() => setFilterOn(!filterOn)}
            isClose={filterOn}
          />
        }
        tabClick={() => {}}
        searchPlaceHolder
      />
      <div className="withdrawalView">
        <IssuanceFunding
          selectedApp={appSelected}
          isList={isList}
          appListObject={appListObject}
          tabSelected={tabSelected}
          setAppListOpen={setDropDownOpen}
          dropDownOpen={dropDownOpen}
          filterOn={filterOn}
        />
        {dropDownOpen && (
          <Scrollbars
            autoHide
            className="sideBarAppSelect"
            renderView={(props) => <div {...props} className="view" />}
          >
            <label className="headSearach">
              <input
                type="text"
                placeholder="Search Apps..|"
                className="inpSearch"
                value={search}
                onChange={(e) => setSearch(e.target.value)}
              />
            </label>
            {appList
              .filter((app) =>
                app.app_name.toLowerCase().includes(search.toLowerCase())
              )
              .map((app) => (
                <div
                  className="appPublication"
                  key={app._id}
                  onClick={() => {
                    setAppSelected(app);
                    setDropDownOpen(false);
                  }}
                >
                  <img src={app.app_icon} alt="" className="pubLogo" />
                  <div className="nameNbtns">
                    <div className="name">{app.app_name}</div>
                    <div className="btns">
                      <div className="btnAction">Copy</div>
                      <div className="btnAction">Expand</div>
                    </div>
                  </div>
                </div>
              ))}
            <div
              className="showAllApp"
              onClick={() => {
                setAppSelected("");
                setDropDownOpen(false);
              }}
            >
              Show All Apps
            </div>
          </Scrollbars>
        )}
      </div>
    </MainLayout>
  );
}

export default FundingPage;
