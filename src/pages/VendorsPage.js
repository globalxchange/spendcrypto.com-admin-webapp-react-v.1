import React, { useEffect, useState, useContext } from "react";
import Axios from "axios";

import MainLayout from "../layouts/MainLayout";
import crown from "../static/images/coinIcon/crownCoin.svg";
import VendorsFilter from "../components/VendorsFilter/VendorsFilter";
import VendorsApps from "../components/VendorsApps/VendorsApps";
import VendorUsers from "../components/VendorUsers/VendorUsers";
import FilterSidebar from "../components/FilterSidebar/FilterSidebar";
import { MainContext } from "../context/MainContext";
import { useParams } from "react-router-dom";
import { IconsViewSwitcher, Navbar } from "@teamforce/broker-admin-dashboards";
import { TabsContext } from "../context/TabsContext";

const tabs = ["Users"];
function VendorsPage({ hide }) {
  const { appSelected, setAppSelected, bankerSelected } = useContext(
    MainContext
  );
  const { navTabs } = useContext(TabsContext);
  const { typeOfPage } = useParams();
  const [tabSelected, setTabSelected] = useState("Users");
  const [search, setSearch] = useState("");
  const [isList, setIsList] = useState(true);
  const [searchStr, setSearchStr] = useState("");
  const [tab, setTab] = useState("Filter 1");
  const [dropDownOpen, setDropDownOpen] = useState(false);

  useEffect(() => {
    setSearchStr("");
  }, [tabSelected]);

  // App List Data
  const [appList, setAppList] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true);
    Axios.get("https://comms.globalxchange.com/gxb/apps/get")
      .then(({ data }) => {
        if (data.status) setAppList(data.apps);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const renderContent = (tab) => {
    switch (tab) {
      case "Apps":
        return (
          <VendorsApps
            searchStr={searchStr}
            appList={appList}
            loading={loading}
            setAppSelected={setAppSelected}
            setTabSelected={setTabSelected}
          />
        );
      case "Users":
        return (
          <VendorUsers
            searchStr={searchStr}
            appSelected={appSelected}
            filter={filter}
          />
        );
      default:
        break;
    }
  };
  const [filter, setFilter] = useState("All");
  const [placeHolder, setPlaceHolder] = useState("");
  useEffect(() => {
    switch (tabSelected) {
      case "Apps":
        setPlaceHolder("Search All GX Apps....");
        break;
      case "Users":
        setPlaceHolder(
          filter === "All"
            ? `Seach All ${
                appSelected.app_name ? appSelected?.app_name : ""
              } Users`
            : `Seach All ${
                appSelected.app_name ? appSelected?.app_name : ""
              } Users Who Have Atleast One Spending Vault`
        );
        break;
      default:
        break;
    }
  }, [tabSelected, appSelected]);

  const [filterOn, setFilterOn] = useState(false);

  return (
    <MainLayout
      hide={hide}
      active={typeOfPage === "banker" ? "CRM" : "Vendors"}
      onNew={() => {}}
    >
      <Navbar
        navTabs={navTabs}
        tabs={tabs}
        tabSelected={tabSelected}
        setTabSelected={(tab) => setTabSelected(tab)}
        search
        setSearch={() => {}}
        button={
          typeOfPage === "partner" && (
            <div className={`publication `}>
              <img
                className="logoIcn"
                src={
                  typeOfPage === "partner"
                    ? appSelected.app_icon
                    : bankerSelected?.profilePicURL || crown
                }
                alt=""
              />
              <span>
                {typeOfPage === "partner"
                  ? appSelected.app_name
                  : bankerSelected?.displayName || bankerSelected?.bankerTag}
              </span>
            </div>
          )
        }
        viewSwitcher={
          <IconsViewSwitcher
            list={isList}
            board={!isList}
            search={filterOn}
            onBoardClick={() => setIsList(false)}
            onListClick={() => setIsList(true)}
            onSearchClick={() => setFilterOn(!filterOn)}
          />
        }
        tabClick={() => {}}
        searchPlaceHolder=""
      />
      {filterOn && (
        <VendorsFilter
          setSearchStr={setSearchStr}
          searchStr={searchStr}
          placeholder={placeHolder}
          filterOne={(tabSelected === "Users" && appSelected.app_name) || "All"}
          filterTwo={filter}
          filterOneClick={() => {
            if (typeOfPage === "banker") {
              setDropDownOpen(!dropDownOpen);
              setTab("App");
            }
          }}
          filterTwoClick={() => {
            setDropDownOpen(!dropDownOpen);
            setTab("Filter 1");
          }}
        />
      )}
      <div className="withdrawalView">
        <div className="withdrawalViewContent">
          {renderContent(tabSelected)}
        </div>
        {dropDownOpen && (
          <FilterSidebar
            setAppSelected={setAppSelected}
            setDropDownOpen={setDropDownOpen}
            setFilter={setFilter}
            appList={appList}
            search={search}
            tab={tab}
          />
        )}
      </div>
    </MainLayout>
  );
}

export default VendorsPage;
