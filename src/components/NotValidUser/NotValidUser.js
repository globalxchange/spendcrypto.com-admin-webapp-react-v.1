import React from "react";
import { useHistory } from "react-router-dom";
import { ReactComponent as TypeLogo } from "../../static/images/loginBgs/typeLogo.svg";

function NotValidUser({ typeOfPage, setInValid }) {
  const history = useHistory();
  return (
    <div className="notValidUser">
      <div className="logoWrap">
        <TypeLogo className={`logo ${typeOfPage}`} />
      </div>
      <p className="detail">
        Sorry But You Are Not A{" "}
        {typeOfPage === "partner" ? "SpendPartner" : "SpendVendor"}. Please Try
        Login In With A Registered{" "}
        {typeOfPage === "partner" ? "SpendPartner" : "SpendVendor"} Account Or
        Register To Become A{" "}
        {typeOfPage === "partner" ? "SpendPartner" : "SpendVendor"}
      </p>
      <div className="btns">
        <div className="btReg" onClick={() => history.push("/partnerWithUs")}>
          Register
        </div>
        <div className="btTry" onClick={() => setInValid(false)}>
          Try Again
        </div>
      </div>
    </div>
  );
}

export default NotValidUser;
