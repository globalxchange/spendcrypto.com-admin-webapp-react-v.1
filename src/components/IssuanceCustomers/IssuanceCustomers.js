import Axios from "axios";
import React, { useEffect, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";
import CustomerDepositModal from "../CustomerDepositModal/CustomerDepositModal";
import CustomerFilter from "../CustomerFilter/CustomerFilter";
import UserDetailSide from "./UserDetailSide";
import UserItem from "./UserItem";

function IssuanceCustomers() {
  const [emailSearch, setEmailSearch] = useState("");
  const [filterType, setFilterType] = useState("Email");
  const [userList, setUserList] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true);
    Axios.get(
      "https://comms.globalxchange.com/coin/vault/service/spending/vault/get"
    )
      .then(({ data }) => {
        if (data.status) {
          setUserList(data.users);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const [dataDebitModal, setDataDebitModal] = useState(false);
  const [userDataLoad, setUserDataLoad] = useState("");

  return (
    <div className="issuanceCustomers">
      <CustomerFilter
        emailSearch={emailSearch}
        setEmailSearch={setEmailSearch}
        filterType={filterType}
        setFilterType={() => {}} //{setFilterType}
      />
      <div className="userListDetailWrap">
        <Scrollbars
          className={`userListWrap ${Boolean(userDataLoad)}`}
          autoHide
          renderView={(props) => <div {...props} className="userList" />}
        >
          {loading
            ? Array.from(Array(10).keys()).map((key) => (
                <div className="userItem" key={key}>
                  <Skeleton circle width={50} height={50} />
                  <div className="nameEmail">
                    <span className="name">
                      <Skeleton />
                    </span>
                    <span className="email">
                      <Skeleton />
                    </span>
                  </div>
                  <div className="time">
                    <Skeleton />
                  </div>
                  <div className="balance">
                    <Skeleton />
                  </div>
                  <div className="btnActions">
                    <Skeleton className="btnAction" />
                    <Skeleton className="btnAction" />
                    <Skeleton className="btnAction" />
                  </div>
                </div>
              ))
            : userList
                .filter((user) =>
                  user._id.toLowerCase().includes(emailSearch.toLowerCase())
                )
                .map((user) => {
                  return (
                    <UserItem
                      key={user._id}
                      user={user}
                      onDebit={() => setDataDebitModal(user)}
                      onRefund={() => {}}
                      userDataLoad={userDataLoad}
                      setUserDataLoad={setUserDataLoad}
                    />
                  );
                })}
        </Scrollbars>
        {userDataLoad && <UserDetailSide userDataLoad={userDataLoad} />}
      </div>
      {dataDebitModal && (
        <CustomerDepositModal
          data={dataDebitModal}
          onClose={() => setDataDebitModal(false)}
          onSuccess={() => {
            setDataDebitModal(false);
          }}
        />
      )}
    </div>
  );
}

export default IssuanceCustomers;
