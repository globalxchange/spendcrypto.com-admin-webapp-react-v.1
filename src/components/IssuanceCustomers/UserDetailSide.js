import React, { useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import guest from "../../static/images/guest.jpg";
import logo from "../../static/images/logos/spendIconLogo.svg";

function UserDetailSide({ userDataLoad }) {
  const [tab, setTab] = useState("Vaults");
  const [statsSwitch, setStatsSwitch] = useState(false);
  return (
    <div className="userDetailSide">
      <div className="head">
        <img src={userDataLoad.profile_img || guest} alt="" className="dp" />
        <div className="texts">
          <div className="name">
            {userDataLoad.name || userDataLoad.username}
          </div>
          <div className="email">{userDataLoad.email}</div>
        </div>
        <div className="box">
          <img className="logo" src={logo} alt="" />
          <span>SpendCrypto</span>
        </div>
      </div>
      <div className="tabs">
        <div
          className={`tab ${tab === "Vaults"}`}
          onClick={() => setTab("Vaults")}
        >
          Vaults
        </div>
        <div
          className={`tab ${tab === "Spending"}`}
          onClick={() => setTab("Spending")}
        >
          Spending
        </div>
        <div
          className={`tab ${tab === "Cards"}`}
          onClick={() => setTab("Cards")}
        >
          Cards
        </div>
        <div
          className={`tab ${tab === "Profile"}`}
          onClick={() => setTab("Profile")}
        >
          Profile
        </div>
        <div
          className={`tab ${tab === "Lineage"}`}
          onClick={() => setTab("Lineage")}
        >
          Lineage
        </div>
      </div>
      <Scrollbars
        className="txnWrapper"
        renderView={(props) => <div {...props} className="txnList" />}
        renderThumbHorizontal={() => <div />}
        renderThumbVertical={() => <div />}
      >
        <div className="header">
          <div className="asset">Asset</div>
          <div className="balance">Balance</div>
          <div className="usd">USD</div>
        </div>
        <div className="txnItm">
          <div className="asset">
            <img
              src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
              alt=""
            />
            <span>Bitcoin</span>
          </div>
          <div className="balance">0.02561</div>
          <div className="usd">$26.26</div>
        </div>
        <div className="txnItm">
          <div className="asset">
            <img
              src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
              alt=""
            />
            <span>Bitcoin</span>
          </div>
          <div className="balance">0.02561</div>
          <div className="usd">$26.26</div>
        </div>
        <div className="txnItm">
          <div className="asset">
            <img
              src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
              alt=""
            />
            <span>Bitcoin</span>
          </div>
          <div className="balance">0.02561</div>
          <div className="usd">$26.26</div>
        </div>
        <div className="txnItm">
          <div className="asset">
            <img
              src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
              alt=""
            />
            <span>Bitcoin</span>
          </div>
          <div className="balance">0.02561</div>
          <div className="usd">$26.26</div>
        </div>
        <div className="txnItm">
          <div className="asset">
            <img
              src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
              alt=""
            />
            <span>Bitcoin</span>
          </div>
          <div className="balance">0.02561</div>
          <div className="usd">$26.26</div>
        </div>
        <div className="txnItm">
          <div className="asset">
            <img
              src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
              alt=""
            />
            <span>Bitcoin</span>
          </div>
          <div className="balance">0.02561</div>
          <div className="usd">$26.26</div>
        </div>
        <div className="txnItm">
          <div className="asset">
            <img
              src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
              alt=""
            />
            <span>Bitcoin</span>
          </div>
          <div className="balance">0.02561</div>
          <div className="usd">$26.26</div>
        </div>
        <div className="txnItm">
          <div className="asset">
            <img
              src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
              alt=""
            />
            <span>Bitcoin</span>
          </div>
          <div className="balance">0.02561</div>
          <div className="usd">$26.26</div>
        </div>
      </Scrollbars>
      <div className="footer">
        <div className="btnBox">
          Stats
          <div
            className={`box ${statsSwitch}`}
            onClick={() => setStatsSwitch(!statsSwitch)}
          >
            <div className="dot" />
          </div>
          HIghlights
        </div>
        <div className="netWorth">
          <span>NetWorth</span>
          <span>$12,526.25</span>
        </div>
      </div>
    </div>
  );
}

export default UserDetailSide;
