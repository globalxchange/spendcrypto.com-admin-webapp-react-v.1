import { faCaretDown, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Axios from "axios";
import React, { useEffect, useState } from "react";
import guest from "../../static/images/guest.jpg";

function UserItem({
  user,
  onClick,
  onDebit,
  onRefund,
  userDataLoad,
  setUserDataLoad,
}) {
  const [userData, setUserData] = useState({});
  const [moreDropOpen, setMoreDropOpen] = useState(false);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true);
    user &&
      Axios.get(
        `https://comms.globalxchange.com/user/details/get?email=${user._id}`
      )
        .then(({ data }) => {
          if (data.status) {
            setUserData(data.user);
          }
        })
        .finally(() => setLoading(false));
  }, [user]);
  return (
    <div
      onClick={() => {
        if (userData.email) {
          if (userData === userDataLoad) {
            setUserDataLoad(false);
          } else {
            setUserDataLoad(userData);
          }
        }
        try {
          onClick();
        } catch (error) {}
      }}
      className="userItem"
    >
      <img
        src={(userData && userData.profile_img) || guest}
        alt=""
        className="profileImg"
      />
      <div className="nameEmail">
        <span className="name">{userData && userData.name}</span>
        <span className="email">
          {(userData && userData.email) || user._id}
        </span>
      </div>
      <div className="time"></div>
      <div className="balance"></div>
      <div className="btnActions">
        <div
          className="btnAction"
          onClick={() => {
            try {
              onDebit(userData);
            } catch (error) {}
          }}
        >
          Debit
        </div>
        <div
          className="btnAction"
          onClick={() => {
            try {
              !loading && onRefund(userData);
            } catch (error) {}
          }}
        >
          Refund
        </div>
        <div
          className="btnAction more"
          onMouseEnter={() => setMoreDropOpen(true)}
          onMouseLeave={() => {
            setTimeout(() => {
              setMoreDropOpen(false);
            }, 100);
          }}
          onClick={() => setMoreDropOpen(!moreDropOpen)}
        >
          More
          {moreDropOpen && false && (
            <div className="dropList">
              <div className="dropItm">Lineage</div>
              <div className="dropItm">Transactions</div>
              <div className="dropItm">Balances</div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default UserItem;
