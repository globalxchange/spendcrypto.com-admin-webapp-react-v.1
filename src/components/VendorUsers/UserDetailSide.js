import Axios from "axios";
import React, { useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";
import { useEffect } from "react/cjs/react.development";
import guest from "../../static/images/guest.jpg";
import logo from "../../static/images/logos/spendIconLogo.svg";
import { FormatCurrency } from "../../utils/FunctionTools";

function UserDetailSide({ userDataLoad }) {
  const [tab, setTab] = useState("Vaults");
  const [statsSwitch, setStatsSwitch] = useState(false);
  const [appSelected, setAppSelected] = useState("");
  const [userApps, setUserApps] = useState([]);
  const [coinsData, setCoinsData] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    Axios.get(
      `https://comms.globalxchange.com/gxb/apps/registered/user?email=${userDataLoad?.email}`
    ).then(({ data }) => {
      setUserApps(data.userApps);

      // app_code: "accountingtool"
      // app_icon: "https://chatsgx.s3-us-east-2.amazonaws.com/shorupan@gmail.com/1600252796981.png"
      // app_name: "Accounting Tool"
      // profile_id:
    });
  }, [userDataLoad]);
  useEffect(() => {
    if (appSelected?.profile_id) {
      setLoading(true);
      setCoinsData([]);
      Axios.post(
        "https://comms.globalxchange.com/coin/vault/service/coins/get",
        {
          app_code: appSelected?.app_code,
          profile_id: appSelected?.profile_id,
        }
      )
        .then(({ data }) => {
          if (data.status) setCoinsData(data.coins_data);
        })
        .finally(() => setLoading(false));
    }
  }, [appSelected]);
  function renderContent(tab) {
    switch (tab) {
      case "App":
        return (
          <Scrollbars
            className="txnWrapper"
            renderView={(props) => <div {...props} className="txnList" />}
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
          >
            <div className="header">
              <div className="asset jcs">App</div>
              <div className="balance jcs">App Code</div>
              <div className="usd jcs">Access</div>
            </div>
            {loading
              ? [1, 2, 3, 4, 5].map((v, i) => (
                  <div className="txnItm" key={i}>
                    <div className="asset">
                      <Skeleton circle width={30} height={30} />
                      <Skeleton width={300} />
                    </div>
                    <Skeleton className="balance jcs" width={300} />
                    <Skeleton className="usd jcs" width={300} />
                  </div>
                ))
              : userApps.map((app) => (
                  <div className="txnItm" key={app.app_code}>
                    <div className="asset">
                      <img src={app.app_icon} alt="" />
                      <span>{app.app_name}</span>
                    </div>
                    <div className="balance jcs">{app.app_code}</div>
                    <div className="usd jcs">
                      <div
                        className="btnAcces"
                        onClick={() => {
                          setAppSelected(app);
                          setTab("Vaults");
                        }}
                      >
                        Access
                      </div>
                    </div>
                  </div>
                ))}
          </Scrollbars>
        );
      case "Vaults":
        return (
          <Scrollbars
            className="txnWrapper"
            renderView={(props) => <div {...props} className="txnList" />}
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
          >
            <div className="header">
              <div className="asset">Asset</div>
              <div className="balance">Balance</div>
              <div className="usd">USD</div>
            </div>
            {coinsData.map((coin) => (
              <div className="txnItm">
                <div className="asset">
                  <img src={coin.coinImage} alt="" />
                  <span>{coin.coinName}</span>
                </div>
                <div className="balance">
                  {FormatCurrency(coin.coinValue, coin.coinSymbol)}
                </div>
                <div className="usd">${FormatCurrency(coin.coinValueUSD)}</div>
              </div>
            ))}
          </Scrollbars>
        );
      default:
        return (
          <Scrollbars
            className="txnWrapper"
            renderView={(props) => <div {...props} className="txnList" />}
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
          >
            <div className="header">
              <div className="asset">Asset</div>
              <div className="balance">Balance</div>
              <div className="usd">USD</div>
            </div>
            <div className="txnItm">
              <div className="asset">
                <img
                  src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
                  alt=""
                />
                <span>Bitcoin</span>
              </div>
              <div className="balance">0.02561</div>
              <div className="usd">$26.26</div>
            </div>
            <div className="txnItm">
              <div className="asset">
                <img
                  src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
                  alt=""
                />
                <span>Bitcoin</span>
              </div>
              <div className="balance">0.02561</div>
              <div className="usd">$26.26</div>
            </div>
            <div className="txnItm">
              <div className="asset">
                <img
                  src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
                  alt=""
                />
                <span>Bitcoin</span>
              </div>
              <div className="balance">0.02561</div>
              <div className="usd">$26.26</div>
            </div>
            <div className="txnItm">
              <div className="asset">
                <img
                  src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
                  alt=""
                />
                <span>Bitcoin</span>
              </div>
              <div className="balance">0.02561</div>
              <div className="usd">$26.26</div>
            </div>
            <div className="txnItm">
              <div className="asset">
                <img
                  src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
                  alt=""
                />
                <span>Bitcoin</span>
              </div>
              <div className="balance">0.02561</div>
              <div className="usd">$26.26</div>
            </div>
            <div className="txnItm">
              <div className="asset">
                <img
                  src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
                  alt=""
                />
                <span>Bitcoin</span>
              </div>
              <div className="balance">0.02561</div>
              <div className="usd">$26.26</div>
            </div>
            <div className="txnItm">
              <div className="asset">
                <img
                  src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
                  alt=""
                />
                <span>Bitcoin</span>
              </div>
              <div className="balance">0.02561</div>
              <div className="usd">$26.26</div>
            </div>
            <div className="txnItm">
              <div className="asset">
                <img
                  src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/bitcoin.png"
                  alt=""
                />
                <span>Bitcoin</span>
              </div>
              <div className="balance">0.02561</div>
              <div className="usd">$26.26</div>
            </div>
          </Scrollbars>
        );
    }
  }
  return (
    <div className="userDetailSide">
      <div className="head">
        <img src={userDataLoad.profile_img || guest} alt="" className="dp" />
        <div className="texts">
          <div className="name">
            {userDataLoad.name || userDataLoad.username}
          </div>
          <div className="email">{userDataLoad.email}</div>
        </div>
        <div className="box" onClick={() => setTab("App")}>
          <img className="logo" src={appSelected?.app_icon || logo} alt="" />
          <span>{appSelected?.app_name || "SpendCrypto"}</span>
        </div>
      </div>
      <div className="tabs">
        <div
          className={`tab ${tab === "Vaults"}`}
          onClick={() => setTab("Vaults")}
        >
          Vaults
        </div>
        <div
          className={`tab ${tab === "Spending"}`}
          onClick={() => setTab("Spending")}
        >
          Spending
        </div>
        <div
          className={`tab ${tab === "Cards"}`}
          onClick={() => setTab("Cards")}
        >
          Cards
        </div>
        <div
          className={`tab ${tab === "Profile"}`}
          onClick={() => setTab("Profile")}
        >
          Profile
        </div>
        <div
          className={`tab ${tab === "Lineage"}`}
          onClick={() => setTab("Lineage")}
        >
          Lineage
        </div>
      </div>
      {renderContent(tab)}
      <div className="footer">
        <div className="btnBox">
          Stats
          <div
            className={`box ${statsSwitch}`}
            onClick={() => setStatsSwitch(!statsSwitch)}
          >
            <div className="dot" />
          </div>
          HIghlights
        </div>
        <div className="netWorth">
          <span>NetWorth</span>
          <span>$12,526.25</span>
        </div>
      </div>
    </div>
  );
}

export default UserDetailSide;
