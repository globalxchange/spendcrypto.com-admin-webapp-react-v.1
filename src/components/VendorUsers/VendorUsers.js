import Axios from "axios";
import React, { useEffect, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";
import UserDetailSide from "./UserDetailSide";
import UserItem from "./UserItem";

function VendorUsers({ searchStr, appSelected, filter }) {
  const [userListAll, setUserListAll] = useState([]);
  const [userList, setUserList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [count, setCount] = useState(8);

  useEffect(() => {
    setLoading(true);

    if (filter === "All") {
      if (appSelected?.app_code) {
        Axios.get(
          `https://comms.globalxchange.com/gxb/apps/users/get?app_code=${appSelected.app_code}`
        )
          .then(({ data }) => {
            if (data.status) {
              setUserListAll(data.users);
            }
          })
          .finally(() => {
            setLoading(false);
          });
      } else {
        Axios.get(`https://comms.globalxchange.com/getnewusers`)
          .then(({ data }) => {
            if (data.status) {
              setUserListAll(data.users);
            }
          })
          .finally(() => {
            setLoading(false);
          });
      }
    } else {
      Axios.get(
        `https://comms.globalxchange.com/coin/vault/service/spending/vault/get?${
          appSelected?.app_code ? "app_code=" + appSelected.app_code : ""
        }`
      )
        .then(({ data }) => {
          if (data.status) {
            setUserList(data.users);
          }
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [appSelected, filter]);

  const [userDataLoad, setUserDataLoad] = useState("");

  return (
    <div className="issuanceCustomers">
      <div className="userListDetailWrap">
        <Scrollbars
          className={`userListWrap ${Boolean(userDataLoad)}`}
          autoHide
          renderView={(props) => <div {...props} className="userList" />}
          onScrollFrame={(data) => {
            data.top === 1 && setCount((count) => count + 10);
          }}
        >
          {loading
            ? Array.from(Array(10).keys()).map((key) => (
                <div className="userItem" key={key}>
                  <Skeleton circle width={50} height={50} />
                  <div className="nameEmail">
                    <span className="name">
                      <Skeleton />
                    </span>
                    <span className="email">
                      <Skeleton />
                    </span>
                  </div>
                  <div className="time">
                    <Skeleton />
                  </div>
                  <div className="balance">
                    <Skeleton />
                  </div>
                  <div className="btnActions">
                    <Skeleton className="btnAction" />
                    <Skeleton className="btnAction" />
                    <Skeleton className="btnAction" />
                  </div>
                </div>
              ))
            : filter === "All"
            ? userListAll
                .filter((user) =>
                  user.email.toLowerCase().includes(searchStr.toLowerCase())
                )
                .slice(0, count)
                .map((user) => {
                  return (
                    <UserItem
                      key={user._id}
                      user={{ ...user, _id: user.email }}
                      onActionOne={() => {}}
                      onActionTwo={() => {}}
                      userDataLoad={userDataLoad}
                      setUserDataLoad={setUserDataLoad}
                    />
                  );
                })
            : userList
                .filter((user) =>
                  user._id.toLowerCase().includes(searchStr.toLowerCase())
                )
                .slice(0, count)
                .map((user) => {
                  return (
                    <UserItem
                      key={user._id}
                      user={user}
                      onActionOne={() => {}}
                      onActionTwo={() => {}}
                      userDataLoad={userDataLoad}
                      setUserDataLoad={setUserDataLoad}
                    />
                  );
                })}
        </Scrollbars>
        {userDataLoad && <UserDetailSide userDataLoad={userDataLoad} />}
      </div>
    </div>
  );
}

export default VendorUsers;
