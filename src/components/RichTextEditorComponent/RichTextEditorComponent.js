import React, { useState } from "react";
import RichTextEditor from "react-rte";
import {
  getTextAlignClassName,
  getTextAlignStyles,
} from "react-rte/lib/lib/blockStyleFunctions";

function RichTextEditorComponent() {
  const [textValue, setTextValue] = useState(RichTextEditor.createEmptyValue());

  console.log(textValue.toString("html", { blockStyleFn: getTextAlignStyles }));
  return (
    <RichTextEditor
      className="richTextEditor"
      blockStyleFn={getTextAlignClassName}
      value={textValue}
      onChange={(value) => {
        setTextValue(value);
      }}
    />
  );
}

export default RichTextEditorComponent;
