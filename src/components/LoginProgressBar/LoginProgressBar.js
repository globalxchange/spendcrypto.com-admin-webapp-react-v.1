import React from "react";
import Lottie from "react-lottie";
import * as animationData from "../../static/animations/moneyCard.json";

function LoginProgressBar({ progress = 0, message }) {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData.default,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <div className="loginPogressWrapper">
      <div className="landingLottie">
        <div className="m-auto">
          <Lottie options={defaultOptions} width={300} height={300} />
        </div>
      </div>
      {/* <div className="progress">
        <div className="progress-bar-login" style={{ width: `${progress}%` }} />
      </div> */}
      <div className="messge">{message}</div>
    </div>
  );
}

export default LoginProgressBar;
