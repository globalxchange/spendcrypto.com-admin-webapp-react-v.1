import React, { useState, useEffect } from "react";
import Scrollbars from "react-custom-scrollbars";
import Axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisH } from "@fortawesome/free-solid-svg-icons";
import Skeleton from "react-loading-skeleton";
import CopyDetailModal from "../CopyDetailModal/CopyDetailModal";
import FundingCard from "./FundingCard";
import RejectFundingModal from "../RejectFundingModal/RejectFundingModal";
import CreditFundingModal from "../CreditFundingModal/CreditFundingModal";
import VendorsFilter from "../VendorsFilter/VendorsFilter";
import { useParams } from "react-router-dom";

function IssuanceFunding({
  selectedApp,
  isList,
  filterOn,
  appListObject,
  tabSelected,
  setAppListOpen,
  dropDownOpen,
}) {
  const { typeOfPage } = useParams();
  const [loading, setLoading] = useState(false);
  const [processingList, setProcessingList] = useState([]);
  const [issuedList, setIssuedList] = useState([]);
  const [cancelledList, setCancelledList] = useState([]);
  const [toCopy, setToCopy] = useState(false);
  const [cardToIssue, setCardToIssue] = useState(false);
  const [cardToReject, setCardToReject] = useState(false);
  const [jsonShow, setJsonShow] = useState(false);
  const getCardLists = async () => {
    setLoading(true);
    const processingRes = await Axios.get(
      "https://comms.globalxchange.com/coin/vault/service/spending/balance/request/get",
      {
        params: selectedApp
          ? { status: "Processing", app_code: selectedApp.app_code }
          : {
              status: "Processing",
            },
      }
    );
    const processingData = processingRes.data;
    if (processingData.status) {
      setProcessingList(processingData.data.logs);
    }
    const issuedRes = await Axios.get(
      "https://comms.globalxchange.com/coin/vault/service/spending/balance/request/get",
      {
        params: selectedApp
          ? { status: "Approved", app_code: selectedApp.app_code }
          : {
              status: "Approved",
            },
      }
    );
    const issuedData = issuedRes.data;
    if (issuedData.status) {
      setIssuedList(issuedData.data.logs);
    }
    const cancelledRes = await Axios.get(
      "https://comms.globalxchange.com/coin/vault/service/spending/balance/request/get",
      {
        params: selectedApp
          ? { status: "Rejected", app_code: selectedApp.app_code }
          : {
              status: "Rejected",
            },
      }
    );
    const cancelledData = cancelledRes.data;
    if (cancelledData.status) {
      setCancelledList(cancelledData.data.logs);
    }
    setLoading(false);
  };
  useEffect(() => {
    getCardLists();
  }, [selectedApp]);
  const [emailSearch, setEmailSearch] = useState("");
  return (
    <div className="withdrawalViewContent">
      {filterOn && (
        <VendorsFilter
          setSearchStr={setEmailSearch}
          searchStr={emailSearch}
          placeholder={`Seach ${tabSelected} Issuance Requests From ${
            selectedApp.app_name || "All Apps"
          }`}
          filterOne={selectedApp.app_name || "All"}
          filterTwo={"All"}
          filterOneClick={() => typeOfPage === "banker" && setAppListOpen(true)}
          hideBtns={dropDownOpen}
        />
      )}
      <div className={`withdrawListNFilter`}>
        <Scrollbars
          autoHide
          className="withdrawListWrapper"
          renderView={(props) => <div {...props} className="withdrawList" />}
        >
          {(tabSelected === "Pending" || tabSelected === "All") && (
            <div className="typeColoumn">
              <div className="head">
                Requests
                <FontAwesomeIcon icon={faEllipsisH} />
              </div>
              <Scrollbars
                className="columScrlWrap"
                autoHide
                renderView={(props) => (
                  <div {...props} className="coloumnScroll" />
                )}
              >
                {loading
                  ? [1, 2, 3, 4, 5, 6, 7, 8].map((i) => (
                      <div className="transactionItm" key={i}>
                        <div className="txnHead">
                          <div className="banker"></div>
                          <Skeleton circle height={30} width={30} />
                        </div>
                        <div className="txnContent px-5">
                          <Skeleton height={40} count={4} />
                        </div>
                        <div className={`txnFooter processing`}></div>
                      </div>
                    ))
                  : processingList
                      .filter((card) =>
                        card.email
                          .toLowerCase()
                          .includes(emailSearch.toLowerCase())
                      )
                      .map((card) => (
                        <FundingCard
                          key={card._id}
                          card={card}
                          setToCopy={setToCopy}
                          status="processing"
                          selectedApp={selectedApp}
                          onIssue={() => {
                            setCardToIssue(card);
                          }}
                          onReject={(card) => {
                            setCardToReject(card);
                          }}
                          appListObject={appListObject}
                          setJsonShow={setJsonShow}
                        />
                      ))}
              </Scrollbars>
            </div>
          )}
          {(tabSelected === "Completed" || tabSelected === "All") && (
            <div className="typeColoumn">
              <div className="head">
                Credited
                <FontAwesomeIcon icon={faEllipsisH} />
              </div>
              <Scrollbars
                className="columScrlWrap"
                autoHide
                renderView={(props) => (
                  <div {...props} className="coloumnScroll" />
                )}
              >
                {loading
                  ? [1, 2, 3, 4, 5, 6, 7, 8].map((i) => (
                      <div className="transactionItm" key={i}>
                        <div className="txnHead">
                          <div className="banker"></div>
                          <Skeleton circle height={30} width={30} />
                        </div>
                        <div className="txnContent px-5">
                          <Skeleton height={40} count={4} />
                        </div>
                        <div className={`txnFooter issued`}></div>
                      </div>
                    ))
                  : issuedList
                      .filter((card) =>
                        card.email
                          .toLowerCase()
                          .includes(emailSearch.toLowerCase())
                      )
                      .map((card) => (
                        <FundingCard
                          key={card._id}
                          card={card}
                          setToCopy={setToCopy}
                          status="issued"
                          selectedApp={selectedApp}
                          onIssue={() => {
                            setCardToIssue(card);
                          }}
                          onReject={() => {
                            setCardToReject(card);
                          }}
                          appListObject={appListObject}
                          setJsonShow={setJsonShow}
                        />
                      ))}
              </Scrollbars>
            </div>
          )}
          {(tabSelected === "Rejected" || tabSelected === "All") && (
            <div className="typeColoumn">
              <div className="head">
                Declined
                <FontAwesomeIcon icon={faEllipsisH} />
              </div>
              <Scrollbars
                className="columScrlWrap"
                autoHide
                renderView={(props) => (
                  <div {...props} className="coloumnScroll" />
                )}
              >
                {loading
                  ? [1, 2, 3, 4, 5, 6, 7, 8].map((i) => (
                      <div className="transactionItm" key={i}>
                        <div className="txnHead">
                          <div className="banker"></div>
                          <Skeleton circle height={30} width={30} />
                        </div>
                        <div className="txnContent px-5">
                          <Skeleton height={40} count={4} />
                        </div>
                        <div className={`txnFooter issued`}></div>
                      </div>
                    ))
                  : cancelledList
                      .filter((card) =>
                        card.email
                          .toLowerCase()
                          .includes(emailSearch.toLowerCase())
                      )
                      .map((card) => (
                        <FundingCard
                          key={card._id}
                          card={card}
                          setToCopy={setToCopy}
                          status="cancelled"
                          selectedApp={selectedApp}
                          onIssue={() => {
                            setCardToIssue(card);
                          }}
                          onReject={() => {
                            setCardToReject(card);
                          }}
                          appListObject={appListObject}
                          setJsonShow={setJsonShow}
                        />
                      ))}
              </Scrollbars>
            </div>
          )}
        </Scrollbars>
      </div>

      {toCopy && (
        <CopyDetailModal
          publication={toCopy}
          onClose={() => setToCopy(false)}
        />
      )}
      {cardToIssue && (
        <CreditFundingModal
          cardToIssue={cardToIssue}
          onClose={() => setCardToIssue(false)}
          onSuccess={() => {
            setCardToIssue(false);
            getCardLists();
          }}
          setJsonShow={setJsonShow}
        />
      )}
      {cardToReject && (
        <RejectFundingModal
          cardToReject={cardToReject}
          onClose={() => setCardToReject(false)}
          onSuccess={() => {
            setCardToReject(false);
            getCardLists();
          }}
          setJsonShow={setJsonShow}
        />
      )}
      {jsonShow && (
        <CopyDetailModal
          publication={jsonShow}
          onClose={() => setJsonShow(false)}
          showJSON={true}
        />
      )}
    </div>
  );
}

export default IssuanceFunding;
