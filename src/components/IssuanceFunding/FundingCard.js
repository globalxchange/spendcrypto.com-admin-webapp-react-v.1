import Axios from "axios";
import React, { useEffect, useState } from "react";
import moment from "moment";
import Scrollbars from "react-custom-scrollbars";

import { FormatCurrency } from "../../utils/FunctionTools";

function FundingCard({
  card,
  setToCopy,
  status,
  onIssue,
  appListObject,
  onReject,
  setJsonShow,
}) {
  const [profile, setProfile] = useState({
    username: "",
    name: "",
    profile_img: "",
  });
  const [selectKeyOpen, setSelectKeyOpen] = useState(false);
  useEffect(() => {
    Axios.post("https://comms.globalxchange.com/get_affiliate_data_no_logs", {
      email: card.email,
    }).then((res) => {
      const data = res.data[0];
      if (data) {
        setProfile({
          username: data.username,
          name: data.name,
          profile_img: data.profile_img,
        });
      }
    });
  }, [card.email]);
  return (
    <>
      <div className="transactionItm" key={card._id}>
        <div className="cardHead">
          <div
            className="coinSel"
            onClick={() => setSelectKeyOpen(!selectKeyOpen)}
          >
            <img src={""} alt="" className="bankerLogo" />
            <div className="banker">{card.coin}</div>
            {/* {selectKeyOpen && (
              <div className="coinList">
                {balanceKeys.map((key) => (
                  <div className="coin" onClick={() => setSelectedKey(key)}>
                    <img src={""} alt="" className="bankerLogo" />
                    <div className="banker">
                      {key.replace("_balance", "").toUpperCase()}
                    </div>
                  </div>
                ))}
              </div>
            )} */}
          </div>
          <div className="">
            <div className="banker">
              {appListObject[card.app_code] &&
                appListObject[card.app_code].app_name}
            </div>
            <img
              src={
                appListObject[card.app_code] &&
                appListObject[card.app_code].app_icon
              }
              alt=""
              className="bankerLogo"
            />
          </div>
        </div>
        <div className="cardContent">
          <div className="nameNativeValue">
            <span>{profile.name || profile.username}</span>
            <span>
              {FormatCurrency(card.requestAmount, card.coin)} {card.coin}
            </span>
          </div>
          <div className="emailUsdValue">
            <span>{card.email}</span>
            <span>
              ${FormatCurrency(card.request_usd_value, card.coin)} USD
            </span>
          </div>
          <Scrollbars
            className="btnScrlWrap"
            renderView={(props) => <div {...props} className="btnScrlList" />}
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
            autoHide
          >
            <div
              className="btnAction"
              onClick={() => {
                try {
                  onIssue();
                } catch (e) {}
              }}
            >
              Credit
            </div>
            <div
              className="btnAction"
              onClick={() => {
                try {
                  onReject({
                    ...card,
                    username: profile.name || profile.username,
                  });
                } catch (e) {}
              }}
            >
              Decline
            </div>
            <div
              className="btnAction disable"
              onClick={() => {
                setJsonShow(card);
              }}
            >
              Expand
            </div>
            <div className="btnAction" onClick={() => setToCopy(card)}>
              Copy
            </div>
          </Scrollbars>
        </div>
        <div className={`cardFooter ${status}`}>
          <span>{moment(card.timestamp).format("MMM Do YYYY")}</span>
          <span>{moment(card.timestamp).format("h:mm:ss A z")}</span>
        </div>
      </div>
    </>
  );
}

export default FundingCard;
