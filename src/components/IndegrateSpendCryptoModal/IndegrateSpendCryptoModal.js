import React, { useState } from "react";
import IndegrateSpendCryptoStepFive from "./Steps/IndegrateSpendCryptoStepFive";
import IndegrateSpendCryptoStepFour from "./Steps/IndegrateSpendCryptoStepFour";
import IndegrateSpendCryptoStepOne from "./Steps/IndegrateSpendCryptoStepOne";
import IndegrateSpendCryptoStepThree from "./Steps/IndegrateSpendCryptoStepThree";
import IndegrateSpendCryptoStepTwo from "./Steps/IndegrateSpendCryptoStepTwo";

const steps = [
  <IndegrateSpendCryptoStepOne />,
  <IndegrateSpendCryptoStepTwo />,
  <IndegrateSpendCryptoStepThree />,
  <IndegrateSpendCryptoStepFour />,
  <IndegrateSpendCryptoStepFive />,
];
function IndegrateSpendCryptoModal({ onClose }) {
  const [step, setStep] = useState(0);
  return (
    <div className="inviteModal">
      <div
        className="overlay"
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      />
      <div className="modalInvite">
        <div className="headInvite">
          <img
            src="/static/media/spendCryptoLogo.cf52fb69.svg"
            alt=""
            className="headLogo"
          />
        </div>
        {steps[step]}
        <div
          className="footer"
          onClick={() => {
            if (step === 4) {
              try {
                onClose();
              } catch (error) {}
            } else setStep(step + 1);
          }}
        >
          {step === 4 ? "Close" : "Next"}
        </div>
      </div>
    </div>
  );
}

export default IndegrateSpendCryptoModal;
