import React, { useState } from "react";

function IndegrateSpendCryptoStepThree() {
  const [name, setName] = useState("");
  const [link, setLink] = useState("");
  return (
    <div className="contentIndegrate sb">
      <div className="grp">
        <label htmlFor>What Is The Name Of Your Company</label>
        <div className="inputBox">
          <input
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            className="number"
            placeholder="Enter Name Here"
          />
        </div>
      </div>
      <div className="grp">
        <label htmlFor>Provide A Web Or Social Link For Your Company</label>
        <div className="inputBox">
          <input
            type="text"
            value={link}
            onChange={(e) => setLink(e.target.value)}
            className="number"
            placeholder="Enter Link Here"
          />
        </div>
      </div>
    </div>
  );
}

export default IndegrateSpendCryptoStepThree;
