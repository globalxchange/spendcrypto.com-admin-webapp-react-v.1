import React, { useState } from "react";

function IndegrateSpendCryptoStepFour() {
  const [name, setName] = useState("");
  return (
    <div className="contentIndegrate">
      <label htmlFor>What Do You Want To Do With SpendCrypto?</label>
      <div className="areaBox">
        <textarea
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          className="number"
          placeholder="Ex. I Want To Integrate SpendCrypto Into My Exchange"
        />
      </div>
    </div>
  );
}

export default IndegrateSpendCryptoStepFour;
