import React from "react";
import Scrollbars from "react-custom-scrollbars";
import wallet from "../../../static/images/modalIcons/wallet.svg";
import broker from "../../../static/images/modalIcons/broker.svg";
import card from "../../../static/images/modalIcons/card.svg";
import pos from "../../../static/images/modalIcons/pos.svg";
import support from "../../../static/images/modalIcons/support.svg";
import { useState } from "react";

function IndegrateSpendCryptoStepOne() {
  const [option, setOption] = useState(0);
  return (
    <div className="contentIndegrate">
      <label htmlFor>How Would You Describe Your Situation?</label>
      <Scrollbars
        className="userTypeListScrl"
        renderThumbHorizontal={() => <div />}
        renderThumbVertical={() => <div />}
      >
        <div
          className={`listItem ${option === 0}`}
          onClick={() => setOption(0)}
        >
          <img src={wallet} alt="" />
          <span>I Am Running My Own Crypto Related Company</span>
        </div>
        <div
          className={`listItem ${option === 1}`}
          onClick={() => setOption(1)}
        >
          <img src={support} alt="" />
          <span>I Want To Create A Crypto Related Company</span>
        </div>
        <div
          className={`listItem ${option === 2}`}
          onClick={() => setOption(2)}
        >
          <img src={pos} alt="" />
          <span>
            I Work In Traditional Payments & Want To Expand My Offerings
          </span>
        </div>
        <div
          className={`listItem ${option === 3}`}
          onClick={() => setOption(3)}
        >
          <img src={card} alt="" />
          <span>
            I Work In Traditional Banking & Want To Issue Cards For Crypto
            Companies
          </span>
        </div>
        <div
          className={`listItem ${option === 4}`}
          onClick={() => setOption(4)}
        >
          <img src={broker} alt="" />
          <span>
            I Am A Broker And Want To Introduce SpendCrypto To My Network
          </span>
        </div>
      </Scrollbars>
    </div>
  );
}

export default IndegrateSpendCryptoStepOne;
