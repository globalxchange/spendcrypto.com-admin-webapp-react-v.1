import React, { useState } from "react";
import Scrollbars from "react-custom-scrollbars";

import telegram from "../../../static/images/clipIcons/telegram.svg";
import email from "../../../static/images/clipIcons/email.svg";
import whatsapp from "../../../static/images/clipIcons/whatsapp.svg";

function IndegrateSpendCryptoStepTwo() {
  const [option, setOption] = useState(0);
  return (
    <div className="contentIndegrate">
      <label htmlFor>How Do You Want To Be Reached?</label>
      <Scrollbars
        className="userTypeListScrl"
        renderThumbHorizontal={() => <div />}
        renderThumbVertical={() => <div />}
      >
        <div
          className={`listItem ${option === 0}`}
          onClick={() => setOption(0)}
        >
          <img src={email} alt="" />
          <span>Email</span>
        </div>
        <div
          className={`listItem ${option === 1}`}
          onClick={() => setOption(1)}
        >
          <img src={whatsapp} alt="" />
          <span>WhatsApp</span>
        </div>
        <div
          className={`listItem ${option === 2}`}
          onClick={() => setOption(2)}
        >
          <img src={telegram} alt="" />
          <span>Telegram</span>
        </div>
      </Scrollbars>
    </div>
  );
}

export default IndegrateSpendCryptoStepTwo;
