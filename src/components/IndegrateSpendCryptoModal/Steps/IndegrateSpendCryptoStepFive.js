import React from "react";
import Lottie from "react-lottie";
import * as animationData from "../../../static/animations/moneyCard.json";

function IndegrateSpendCryptoStepFive() {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData.default,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <div className="contentIndegrate">
      <div className="landingLottie">
        <div className="m-auto">
          <Lottie options={defaultOptions} width={300} height={300} />
        </div>
      </div>
    </div>
  );
}

export default IndegrateSpendCryptoStepFive;
