import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { MainContext } from "../../context/MainContext";

import spendCrypto from "../../static/images/logos/spendCryptoLogo.svg";

function RejectFundingModal({ onClose, cardToReject, onSuccess, setJsonShow }) {
  const { email, token, tostShowOn } = useContext(MainContext);
  const [dp, setDp] = useState("");
  useEffect(() => {
    cardToReject &&
      Axios.post("https://comms.globalxchange.com/get_affiliate_data_no_logs", {
        email: cardToReject.email,
      }).then((res) => {
        const data = res.data[0];
        if (data) {
          // username: data.username,
          // name: data.name,
          setDp(data.profile_img);
        }
      });
  }, [cardToReject]);
  const [loading, setLoading] = useState(false);
  const issueCard = () => {
    setLoading(true);
    Axios.post(
      "https://comms.globalxchange.com/coin/vault/service/spending/vault/card/admin/update",
      {
        email: email,
        token: token,
        request_identifier: cardToReject.identifier,
        fee_percentage: 50,
        status: "Rejected",
      }
    )
      .then(({ data }) => {
        if (data.status) {
          tostShowOn("Fund Rejected");
          try {
            onSuccess();
          } catch (error) {}
        } else {
          tostShowOn(data.message);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return (
    <div className="issueModal">
      <div
        className="overlay"
        onClick={() => {
          try {
            onClose();
          } catch (e) {}
        }}
      />
      <div className="modalIssue">
        <div className="head">
          <img src={spendCrypto} alt="" />
        </div>
        <div className="modalContent">
          <p className="rejectText">
            Are You Sure You Want To Reject {cardToReject.username}’s Funding
            Request?
          </p>
          <div className="seeDetail" onClick={()  =>  setJsonShow(cardToReject)}>
            
            See Funding Request Details
          
          </div>
          {loading ? (
            <div className="btnIssue">Rejecting Card...</div>
          ) : (
            <div className="btnIssue" onClick={issueCard}>
              Reject
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default RejectFundingModal;
