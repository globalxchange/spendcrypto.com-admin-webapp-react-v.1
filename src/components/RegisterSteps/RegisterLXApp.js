import React from "react";
import { ReactComponent as TypeLogo } from "../../static/images/loginBgs/typeLogo.svg";
import RegisterCreateLXProfile from "./RegisterCreateLXProfile";

function RegisterLXApp({ emailId, setLxData }) {
  return (
    <div className="registerAllUserTypes">
      <TypeLogo className="partnerLogo" />
      <RegisterCreateLXProfile
        finish={(fName, lName, dpLink) => {
          setLxData({ fName, lName, dpLink });
        }}
        email={emailId}
      />
    </div>
  );
}

export default RegisterLXApp;
