import React, { useContext, useState } from "react";
import JsonWebToken from "jsonwebtoken";
import Axios from "axios";

import { MainContext } from "../../context/MainContext";
import cloudUpload from "../../static/images/clipIcons/cloudCircle.svg";

function renameFile(originalFile, newName) {
  return new File([originalFile], newName, {
    type: originalFile.type,
    lastModified: originalFile.lastModified,
  });
}

const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; // secret not to be disclosed anywhere.
const emailDev = "rahulrajsb@outlook.com"; // email of the developer.

function RegisterCreateLXProfile({ finish, email }) {
  const [step, setStep] = useState(0);
  const [fName, setFName] = useState("");
  const [lName, setLName] = useState("");

  const { tostShowOn } = useContext(MainContext);
  const [thubnailLink, setThubnailLink] = useState("");
  const [thumbLoading, setThumbLoading] = useState(false);
  const uploadImage = async (e) => {
    setThumbLoading(true);
    const fileName = `${new Date().getTime()}${e.target.files[0].name.substr(
      e.target.files[0].name.lastIndexOf(".")
    )}`;
    const formData = new FormData();
    const file = renameFile(e.target.files[0], fileName);
    formData.append("files", file);
    const path_inside_brain = "root/";
    const token = JsonWebToken.sign(
      { name: fileName, email: emailDev },
      secret,
      {
        algorithm: "HS512",
        expiresIn: 240,
        issuer: "gxjwtenchs512",
      }
    );
    console.log("file,fileName", file, fileName);
    let { data } = await Axios.post(
      `https://drivetest.globalxchange.io/file/dev-upload-file?email=${emailDev}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
      formData,
      {
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
      }
    );
    setThubnailLink(data.payload.url);
    setThumbLoading(false);
  };
  const steps = [
    <>
      <div className="group">
        <input
          type="text"
          value={fName}
          onChange={(e) => setFName(e.target.value)}
          required="required"
        />
        <span className="highlight" />
        <span className="bar" />
        <label>First Name</label>
      </div>
      <div className="group">
        <input
          type="text"
          value={lName}
          onChange={(e) => setLName(e.target.value)}
          required="required"
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              setStep(1);
            }
          }}
        />
        <span className="highlight" />
        <span className="bar" />
        <label>Last Name</label>
      </div>
      <div className="btnNext" onClick={() => setStep(1)}>
        Next Step
      </div>
    </>,
    <>
      <div className="uploadRow">
        <label className="imgWrap">
          <input type="file" className="d-none" onChange={uploadImage} />
          <img
            src={thubnailLink || cloudUpload}
            className="cloudUpload"
            alt=""
          />
        </label>
        <div className="texts">
          <div className="name">{`${fName} ${lName}`}</div>
          <div className="email">{email}</div>
        </div>
      </div>
      <div
        className="btnNext"
        onClick={() => {
          if (!thumbLoading && thubnailLink)
            try {
              finish(fName, lName, thubnailLink);
            } catch (error) {}
        }}
      >
        {thumbLoading
          ? "Uploading Image..."
          : thubnailLink
          ? "Next"
          : "Upload Photo"}
      </div>
    </>,
  ];
  return <>{steps[step]}</>;
}

export default RegisterCreateLXProfile;
