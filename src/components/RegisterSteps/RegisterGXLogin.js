import {
  faArrowLeft,
  faEye,
  faEyeSlash,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { Link } from "react-router-dom";

function RegisterGXLogin({ loginValidate, app, onClose }) {
  const [emailid, setEmailId] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  return (
    <div className="loginWrapper registerStep">
      <FontAwesomeIcon
        icon={faArrowLeft}
        className="back"
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      />
      <div className="step">
        <div className="stepTitle">Login</div>
        <div className="stepDesc">With Your {app} Credentials</div>
      </div>
      <form
        className="login-form"
        onSubmit={(e) => {
          e.preventDefault();
          loginValidate(emailid, password, app);
        }}
      >
        <div className="group">
          <input
            type="text"
            name="email"
            value={emailid}
            onChange={(e) => setEmailId(e.target.value)}
            required="required"
          />
          <span className="highlight" />
          <span className="bar" />
          <label>Email</label>
        </div>
        <div className="group">
          <input
            type={showPassword ? "text" : "password"}
            name="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required="required"
          />
          <span className="highlight" />
          <span className="bar" />
          <FontAwesomeIcon
            className="eye"
            onClick={() => {
              setShowPassword(!showPassword);
            }}
            icon={showPassword ? faEyeSlash : faEye}
          />
          <label>Password</label>
        </div>
        <div className="group">
          <button type="submit">Login</button>
        </div>
      </form>
      <Link to="/" className="goHome">
        Go Home
      </Link>
    </div>
  );
}

export default RegisterGXLogin;
