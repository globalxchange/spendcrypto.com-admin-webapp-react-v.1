import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useContext } from "react";
import { MainContext } from "../../context/MainContext";
import copyIcon from "../../static/images/clipIcons/copyIcon.svg";
import linkIcon from "../../static/images/clipIcons/linkIcon.svg";

function RegisterSorryOperator({ onClose }) {
  const { tostShowOn } = useContext(MainContext);
  return (
    <div className="registerisGx registerStep">
      <FontAwesomeIcon
        icon={faArrowLeft}
        className="back"
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      />
      <div className="step">
        <div className="stepTitle">Sorry</div>
        <div className="stepDesc">
          Only Agency Operators Can Become SpendCrypto Vendores. Please Create
          Your Agency By Clicking The Link Below
        </div>
        <div className="btns">
          <div className="btnLink">
            <a
              href="https://www.mycryptobrand.com/"
              target="_blank"
              rel="noopener noreferrer"
            >
              www.mycryptobrand.com
            </a>
            <div
              className="icon"
              onClick={() => {
                navigator.clipboard
                  .writeText("https://www.mycryptobrand.com/")
                  .then(() => {
                    tostShowOn("Link Copied To Clipboard");
                  });
              }}
            >
              <img src={copyIcon} alt="" />
            </div>
            <a
              href="https://www.mycryptobrand.com/"
              target="_blank"
              rel="noopener noreferrer"
              className="icon"
            >
              <img src={linkIcon} alt="" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RegisterSorryOperator;
