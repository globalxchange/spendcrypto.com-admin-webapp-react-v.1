import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import RegisterGXLogin from "./RegisterGXLogin";

function RegisterGXUser({ loginValidate, app, step = "3", onClose }) {
  const [gx, setGx] = useState("");
  return (
    <>
      {gx ? (
        <RegisterGXLogin
          loginValidate={loginValidate}
          app={gx}
          onClose={onClose}
        />
      ) : (
        <div className="registerisGx registerStep">
          <FontAwesomeIcon
            icon={faArrowLeft}
            className="back"
            onClick={() => {
              try {
                onClose();
              } catch (error) {}
            }}
          />
          <div className="step">
            <div className="stepTitle">Step {step}</div>
            <div className="stepDesc">Are You Already A SpendCrypto User?</div>
            <div className="btns">
              <div className="btnYes" onClick={() => setGx(app)}>
                No
              </div>
              <div className="btnYes" onClick={() => setGx("SpendCrypto")}>
                Yes
              </div>
            </div>
          </div>
          <Link to="/" className="goHome">
            Go Home
          </Link>
        </div>
      )}
    </>
  );
}

export default RegisterGXUser;
