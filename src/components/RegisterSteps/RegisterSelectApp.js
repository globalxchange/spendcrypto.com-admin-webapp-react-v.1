import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Axios from "axios";
import React, { useEffect, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";
import RegisterGXUser from "./RegisterGXUser";

function RegisterSelectApp({ loginValidate, setAppCode, onClose }) {
  const [appList, setAppList] = useState([]);
  const [appSelected, setAppSelected] = useState({});
  const [appListObject, setAppListObject] = useState({});
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState("");

  useEffect(() => {
    setLoading(true);
    Axios.get("https://comms.globalxchange.com/gxb/apps/get")
      .then(({ data }) => {
        if (data.status) {
          setAppList(data.apps);
          let tempObj = {};
          data.apps.forEach((app) => {
            tempObj[app.app_code] = app;
          });
          setAppListObject(tempObj);
        }
      })
      .finally(() => setLoading(false));
  }, []);
  return (
    <>
      {appSelected.app_code ? (
        <RegisterGXUser
          loginValidate={loginValidate}
          app={appSelected?.app_name}
          step="4"
          onClose={() => {
            try {
              onClose();
            } catch (error) {}
          }}
        />
      ) : (
        <div className="registerSelectApp registerStep pt12">
          <FontAwesomeIcon
            icon={faArrowLeft}
            className="back"
            onClick={() => {
              try {
                onClose();
              } catch (error) {}
            }}
          />
          <div className="step">
            <div className="stepTitle">Step 3</div>
            <div className="stepDesc">Select App To Indegrate</div>
          </div>
          <div className="searchBar">
            <input
              type="text"
              className="searchInp"
              placeholder="Search Apps"
              value={search}
              onChange={(e) => setSearch(e.target.value)}
            />
          </div>
          <Scrollbars
            className="appListScrl"
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
          >
            {loading
              ? Array(6)
                  .fill("")
                  .map((a, i) => (
                    <div className="appItem">
                      <Skeleton className="img" />
                      <Skeleton width={300} />
                    </div>
                  ))
              : appList
                  ?.filter(
                    (app) =>
                      app?.app_name
                        ?.toLowerCase()
                        .includes(search?.toLowerCase()) ||
                      app.app_code
                        ?.toLowerCase()
                        .includes(search?.toLowerCase())
                  )
                  .map((app) => (
                    <div
                      className="appItem"
                      onClick={() => {
                        setAppSelected(
                          appListObject &&
                            app?.app_code &&
                            appListObject[app.app_code] &&
                            appListObject[app.app_code]
                        );
                        setAppCode(app.app_code);
                      }}
                    >
                      <img
                        src={
                          appListObject &&
                          app?.app_code &&
                          appListObject[app.app_code] &&
                          appListObject[app.app_code]?.app_icon
                        }
                        alt=""
                      />
                      <span>
                        {appListObject &&
                          app?.app_code &&
                          appListObject[app.app_code] &&
                          appListObject[app.app_code]?.app_name}
                      </span>
                    </div>
                  ))}
          </Scrollbars>
        </div>
      )}
    </>
  );
}

export default RegisterSelectApp;
