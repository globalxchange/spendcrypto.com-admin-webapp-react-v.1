import React from "react";
import { ReactComponent as TypeLogo } from "../../static/images/loginBgs/typeLogo.svg";

function RegisterSelectUserType({ setUserType }) {
  return (
    <div className="registerSelectType registerStep">
      <div className="step">
        <div className="stepTitle">Step 1</div>
        <div className="stepDesc">Choose Type Of Account</div>
      </div>
      <div
        className="spendPartner vendor"
        onClick={() => setUserType("partner")}
      >
        <TypeLogo className="typeLogo" />
        <p>
          The Initial Fund Offering (IFO) is a system which allows the user to
          invest into a hedge fund manager trustless while at the same time
          creating liquidity for the invested stake in the fund...{" "}
          <span>Read More</span>
        </p>
      </div>
      <div
        className="spendPartner partner"
        onClick={() => setUserType("vendor")}
      >
        <TypeLogo className="typeLogo" />
        <p>
          The Initial Fund Offering (IFO) is a system which allows the user to
          invest into a hedge fund manager trustless while at the same time
          creating liquidity for the invested stake in the fund...{" "}
          <span>Read More</span>
        </p>
      </div>
    </div>
  );
}

export default RegisterSelectUserType;
