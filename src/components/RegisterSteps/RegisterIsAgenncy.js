import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Link } from "react-router-dom";

function RegisterIsAgenncy({ setIsAgency, onClose }) {
  return (
    <div className="registerisGx registerStep">
      <FontAwesomeIcon
        icon={faArrowLeft}
        className="back"
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      />
      <div className="step">
        <div className="stepTitle">Step 2</div>
        <div className="stepDesc">Are You Already An Agency Operator?</div>
        <div className="btns">
          <div className="btnYes" onClick={() => setIsAgency("no")}>
            No
          </div>
          <div className="btnYes" onClick={() => setIsAgency("yes")}>
            Yes
          </div>
        </div>
      </div>
      <Link to="/" className="goHome">
        Go Home
      </Link>
    </div>
  );
}

export default RegisterIsAgenncy;
