import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import noneOfAboveIcon from "../../static/images/clipIcons/noneOfAbove.svg";
import bankerIcon from "../../static/images/logos/bankerIcon.svg";
import gxIcon from "../../static/images/logos/gxIcon.svg";

function RegisterSelectUserLevel({ setUserLevel, onClose }) {
  return (
    <div className="registerSelectLevel registerStep">
      <FontAwesomeIcon
        icon={faArrowLeft}
        className="back"
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      />
      <div className="step">
        <div className="stepTitle">Step 2</div>
        <div className="stepDesc">
          Which Of The Following Statements Describe You
        </div>
      </div>
      <div className="userLevelList">
        <div className="listItm" onClick={() => setUserLevel("banker")}>
          <img src={bankerIcon} alt="" />
          <span>I Am A Banker </span>
        </div>
        <div className="listItm" onClick={() => setUserLevel("gx")}>
          <img src={gxIcon} alt="" />
          <span>I Am A User Of A GX Application</span>
        </div>
        <div className="listItm" onClick={() => setUserLevel("nota")}>
          <img src={noneOfAboveIcon} alt="" />
          <span>I Am None Of The Above</span>
        </div>
      </div>
    </div>
  );
}

export default RegisterSelectUserLevel;
