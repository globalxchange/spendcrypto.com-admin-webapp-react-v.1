import React from "react";
import { Link } from "react-router-dom";

function RegisterSuccess({ userType, succesGoPage }) {
  return (
    <div className="registerSuccess registerStep">
      <div className="step">
        <div className="stepTitle">Success</div>
        <div className="stepDesc">
          Your {userType === "vendor" ? "SpendVendor" : "SpendPartner"} Account
          Has Been Created
        </div>
      </div>
      <Link to="/" className="btnCostumize">
        Go Home
      </Link>
      <Link to={succesGoPage} className="btnLogin">
        Login
      </Link>
    </div>
  );
}

export default RegisterSuccess;
