import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import CryptoJS from "crypto-js";
import OtpInput from "react-otp-input";

import { MainContext } from "../../context/MainContext";
import { ReactComponent as TypeLogo } from "../../static/images/loginBgs/typeLogo.svg";
import RegisterCreateLXProfile from "./RegisterCreateLXProfile";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

const emailRegex = new RegExp(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/);
const capRegex = new RegExp(/^.*[A-Z].*/);
const numRegex = new RegExp(/^.*[0-9].*/);
const speRegex = new RegExp(/^.*[!@#$%^&*()+=].*/);
const otpRegex = new RegExp(/^\d*$/);

function RegisterAllUserTypes({
  loginValidate,
  setLxData,
  config,
  setLoading,
  setLoadingMessage,
  onClose,
}) {
  const { tostShowOn } = useContext(MainContext);

  const [mailNUnames, setMailNUnames] = useState({
    emails: [],
    usernames: [],
  });
  useEffect(() => {
    Axios.get("https://comms.globalxchange.com/listUsernames").then((res) => {
      const { data } = res;
      if (data.status) {
        let bytes = CryptoJS.Rabbit.decrypt(data.payload, "gmBuuQ6Er8XqYBd");
        let jsonString = bytes.toString(CryptoJS.enc.Utf8);
        let result_obj = JSON.parse(jsonString);
        setMailNUnames(result_obj);
      }
    });
  }, []);

  const trueCircle = (
    <svg
      className="circle"
      viewBox="0 0 14 14"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="7" cy="7" r="6.5" fill="#002A51" stroke="#2F72AE" />
    </svg>
  );
  const falseCircle = (
    <svg
      className="circle"
      viewBox="0 0 14 14"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="7" cy="7" r="7" fill="#BE241A" />
    </svg>
  );

  const [isValid, setIsValid] = useState({});
  const [brokerSync, setBrokerSync] = useState("");
  const [emailId, setEmailId] = useState("");
  const [userId, setUserId] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [pin, setPin] = useState("");
  const [step, setStep] = useState(0);

  useEffect(() => {
    setIsValid({
      uname: !mailNUnames.usernames.includes(userId) && userId.length > 3,
      email: emailRegex.test(emailId) && !mailNUnames.emails.includes(emailId),
      password:
        capRegex.test(password) &&
        numRegex.test(password) &&
        speRegex.test(password) &&
        password.length >= 8,
      confirmPassword: confirmPassword === password,
      pin: String(pin).length === 6,
    });
  }, [
    userId,
    emailId,
    password,
    confirmPassword,
    pin,
    mailNUnames.usernames,
    mailNUnames.emails,
  ]);

  const uNameValidate = (e) => {
    const { value } = e.target;
    setUserId(value.trim().toLowerCase());
  };

  const emailValidate = (e) => {
    const { value } = e.target;
    setEmailId(value.trim().toLowerCase());
  };

  const passwordValidate = (e) => {
    const { value } = e.target;
    setPassword(value.trim());
  };

  const validateCnfrmPaswd = (e) => {
    const { value } = e.target;
    setConfirmPassword(value.trim());
  };
  const pinValidator = (pinStr) => {
    if (otpRegex.test(pinStr)) setPin(pinStr);
  };

  function signup() {
    setLoading(true);
    setLoadingMessage("Creating GX Profile");
    Axios.post(
      "https://gxauth.apimachine.com/gx/user/signup",
      {
        username: userId,
        email: emailId,
        password: password,
        ref_affiliate: brokerSync || "1", // reference/upline affiliate-id
        account_type: "Personal",
        signedup_app: "spendcrypto",
      },
      config
    )
      .then(({ data }) => {
        if (data.status) {
          setStep(4);
          tostShowOn(`Enter OTP from ${emailId}`);
        }
      })
      .finally(() => {
        setLoading(false);
        setLoadingMessage("");
      });
  }
  function cnrfmSignup() {
    setLoading(true);
    setLoadingMessage("Validating OTP");
    Axios.post("https://gxauth.apimachine.com/gx/user/confirm", {
      email: emailId,
      code: pin,
    })
      .then(({ data }) => {
        if (data.status) {
          loginValidate(emailId, password);
        }
      })
      .finally(() => {
        setLoading(false);
        setLoadingMessage("");
      });
  }
  const steps = [
    <>
      <div className="group">
        <input
          type="text"
          value={brokerSync}
          onChange={(e) => setBrokerSync(e.target.value)}
          required="required"
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              setStep(1);
            }
          }}
        />
        <span className="highlight" />
        <span className="bar" />
        <label>Enter A Broker Sync Code</label>
      </div>
      <div className="btnNext" onClick={() => setStep(1)}>
        Next Step
      </div>
    </>,
    <>
      <div className="group">
        <input
          type="text"
          value={userId}
          onChange={uNameValidate}
          required="required"
        />
        <span className="highlight" />
        <span className="bar" />
        <label>Create Username</label>
        {isValid.uname ? trueCircle : falseCircle}
      </div>
      <div className="group">
        <input
          type="text"
          value={emailId}
          onChange={emailValidate}
          required="required"
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              isValid.email && isValid.uname && setStep(2);
            }
          }}
        />
        <span className="highlight" />
        <span className="bar" />
        <label>Enter Email</label>
        {isValid.email ? trueCircle : falseCircle}
      </div>
      <div
        className="btnNext"
        onClick={() => {
          isValid.email && isValid.uname && setStep(2);
        }}
      >
        Next Step
      </div>
    </>,
    <>
      <div className="group">
        <input
          type="password"
          value={password}
          onChange={passwordValidate}
          required="required"
        />
        <span className="highlight" />
        <span className="bar" />
        <label>Create Password</label>
        {isValid.password ? trueCircle : falseCircle}
      </div>
      <div className="group">
        <input
          type="password"
          value={confirmPassword}
          onChange={validateCnfrmPaswd}
          required="required"
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              isValid.password && isValid.confirmPassword && setStep(3);
            }
          }}
        />
        <span className="highlight" />
        <span className="bar" />
        <label>Confirm Passwrod</label>
        {isValid.confirmPassword ? trueCircle : falseCircle}
      </div>
      <div
        className="btnNext"
        onClick={() => {
          isValid.password && isValid.confirmPassword && setStep(3);
        }}
      >
        Next Step
      </div>
    </>,
    <RegisterCreateLXProfile
      finish={(fName, lName, dpLink) => {
        signup();
        setLxData({ fName, lName, dpLink });
      }}
      email={emailId}
    />,
    <>
      <div className="otpLabel">Enter Otp From Email</div>
      <OtpInput
        containerStyle="otpWrapper"
        inputStyle="inputStyle"
        value={pin}
        onChange={pinValidator}
        numInputs={6}
      />
      <div
        className="btnNext"
        onClick={() => {
          isValid.pin && cnrfmSignup();
        }}
      >
        Confirm Signup
      </div>
    </>,
  ];
  return (
    <div className="registerAllUserTypes registerStep">
      <FontAwesomeIcon
        icon={faArrowLeft}
        className="back"
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      />
      <TypeLogo className="partnerLogo" />
      {steps[step]}
      <Link to="/" className="goHome">
        Go Home
      </Link>
    </div>
  );
}

export default RegisterAllUserTypes;
