import Axios from "axios";
import React, { useContext, useEffect, useRef, useState } from "react";

import { MainContext } from "../../context/MainContext";
import MasterCardSvg from "../MasterCardSvg/MasterCardSvg";

import spendCryptoLogo from "../../static/images/logos/spendCryptoWhite.svg";
import EditSidebar from "./EditSidebar";
import VendorsFilter from "../VendorsFilter/VendorsFilter";

function MyCards() {
  const { appSelected } = useContext(MainContext);
  const [index, setIndex] = useState(0);
  const [res, setRes] = useState({});
  console.log("appSelected", appSelected);
  const getData = () => {
    Axios.get(
      `https://comms.globalxchange.com/gxb/apps/get?app_code=${appSelected?.app_code}`
    ).then(({ data }) => {
      if (data.status) {
        setRes(data.apps[0]);
      }
    });
  };
  useEffect(() => {
    getData();
  }, [appSelected]);
  const isPrev = (i) => {
    if (index === 0) {
      if (i === 4 - 1) return true;
    } else {
      if (i === index - 1) return true;
    }
    return false;
  };
  const isNext = (i) => {
    if (index === 4 - 1) {
      if (i === 0) return true;
    } else {
      if (i === index + 1) return true;
    }
    return false;
  };

  const ref = useRef();
  useEffect(() => {
    ref?.current?.style &&
      ref.current.style.setProperty(
        "--card-color",
        res?.color_codes && res?.color_codes[0]
          ? `${res?.color_codes[0]}`
          : "#484848"
      );
    return () => {};
  }, [res]);

  const [pageStep, setPageStep] = useState("home");

  const getContet = () => {
    switch (pageStep) {
      case "home":
        return (
          <>
            <div className="headLogoArea">
              <div className="mainTitle">Welcome To Your Card Manager</div>
              <div className="subTitle">
                Here You Can Edit Your Existing Cards
              </div>
            </div>
            <div className="carousel-wrapper">
              <div className="carousel-horizontal">
                {Array(4)
                  .fill("")
                  .map((app, i) => (
                    <MasterCardSvg
                      logo={res?.spendcryptocardlogo1 || spendCryptoLogo}
                      color={
                        res?.[`scc${i + 1}backgroundcolor`]
                          ? `#${res?.[`scc${i + 1}backgroundcolor`]}`
                          : "#050505"
                      }
                      gradient={!res?.[`scc${i + 1}backgroundcolor`] && i}
                      onClick={() => setIndex(i)}
                      className={
                        index === i
                          ? "active"
                          : isPrev(i)
                          ? "prev"
                          : isNext(i)
                          ? "next"
                          : "inactive"
                      }
                    />
                  ))}
              </div>
            </div>
            <div className="btnsList">
              <div className="btnItem" onClick={() => setPageStep("edit")}>
                Edit
              </div>
              <div className="btnItem disable">Data</div>
            </div>
          </>
        );
      case "edit":
        return (
          <div className="editCards">
            <div className="cardContent">
              <div className="cardName">Name Of Card</div>
              <MasterCardSvg
                logo={res?.spendcryptocardlogo1 || spendCryptoLogo}
                color={
                  res?.[`scc${index + 1}backgroundcolor`]
                    ? `#${res?.[`scc${index + 1}backgroundcolor`]}`
                    : "#050505"
                }
                gradient={!res?.[`scc${index + 1}backgroundcolor`] && index}
                onClick={() => {}}
                className="active"
              />
            </div>
            <EditSidebar
              index={index}
              res={res}
              getData={getData}
              setPageStep={setPageStep}
            />
          </div>
        );
      default:
        break;
    }
  };

  return (
    <div div className="myCards" ref={ref}>
      {getContet()}
    </div>
  );
}

export default MyCards;
