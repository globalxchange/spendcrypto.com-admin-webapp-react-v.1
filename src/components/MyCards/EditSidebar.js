import React, { Fragment, useContext, useState } from "react";
import Axios from "axios";
import JsonWebToken from "jsonwebtoken";

import spendCryptoLogo from "../../static/images/logos/spendCryptoLogo.svg";
import cloudUpload from "../../static/images/postClipArt/cloudUpload.svg";
import { MainContext } from "../../context/MainContext";

function renameFile(originalFile, newName) {
  return new File([originalFile], newName, {
    type: originalFile.type,
    lastModified: originalFile.lastModified,
  });
}
const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; // secret not to be disclosed anywhere.
const emailDev = "rahulrajsb@outlook.com"; // email of the developer.

function EditSidebar({ res, getData, setPageStep, index }) {
  const { email, token, tostShowOn, appSelected } = useContext(MainContext);
  const [color, setColor] = useState("");
  const [thumbLink, setThumbLink] = useState("");
  const [thumbLoading, setThumbLoading] = useState(false);
  const uploadImage = async (e) => {
    setThumbLoading(true);
    const fileName = `${new Date().getTime()}${e.target.files[0].name.substr(
      e.target.files[0].name.lastIndexOf(".")
    )}`;
    const formData = new FormData();
    const file = renameFile(e.target.files[0], fileName);
    formData.append("files", file);
    const path_inside_brain = "root/";
    const token = JsonWebToken.sign(
      { name: fileName, email: emailDev },
      secret,
      {
        algorithm: "HS512",
        expiresIn: 240,
        issuer: "gxjwtenchs512",
      }
    );
    let { data } = await Axios.post(
      `https://drivetest.globalxchange.io/file/dev-upload-file?email=${emailDev}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
      formData,
      {
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
      }
    );
    setThumbLink(data.payload.url);
    setThumbLoading(false);
  };
  const [updatingLogo, setUpdatingLogo] = useState(false);
  function updateLogo() {
    setUpdatingLogo(true);
    Axios.post("https://comms.globalxchange.com/gxb/apps/profile/update", {
      token: token,
      email: email,
      app_code: appSelected?.app_code,
      update_data: {
        spendcryptocardlogo1: thumbLink,
      },
    })
      .then(({ data }) => {
        if (!data?.status) tostShowOn(data.message);
        else {
          tostShowOn("Logo Updated");
        }
        getData();
      })
      .finally(() => {
        setUpdatingLogo(false);
      });
  }
  function updateColor() {
    Axios.post("https://comms.globalxchange.com/gxb/apps/profile/update", {
      token: token,
      email: email,
      app_code: appSelected?.app_code,
      update_data: {
        [`scc${index + 1}backgroundcolor`]: color,
      },
    }).then(({ data }) => {
      if (!data?.status) tostShowOn(data.message);
      else {
        getData();
      }
    });
  }
  const [step, setStep] = useState("");
  function getStep() {
    switch (step) {
      case "logo":
        return (
          <div className="logo">
            <img className="logoIcn" src={spendCryptoLogo} alt="" />
            <div className="icLabel">Current Card Logo</div>
            <div className="boxLogo">
              <img
                src={res?.spendcryptocardlogo1 || spendCryptoLogo}
                alt=""
                className="curLogo"
              />
            </div>
            <div className="icLabel">New Card Logo</div>
            <label className="boxLogo upload">
              {thumbLink ? (
                <>
                  <img src={thumbLink} className="curLogo" alt="" />
                </>
              ) : (
                <>
                  <img src={cloudUpload} className="curLogo" alt="" />
                  <span>
                    {thumbLoading ? "Uploading...." : "Upload New Logo"}
                  </span>
                </>
              )}
              <input
                type="file"
                accept="image/*"
                className="d-none"
                onChange={uploadImage}
              />
            </label>
            <div className="btns">
              <div className="btnNP" onClick={() => setPageStep("home")}>
                Back
              </div>
              <div
                className="btnNP nxt"
                onClick={() => {
                  if (thumbLink && !thumbLoading) {
                    updateLogo();
                  }
                }}
              >
                {updatingLogo ? "Updating" : "Next"}
              </div>
            </div>
          </div>
        );
      case "color":
        return (
          <div className="logo">
            <img className="logoIcn" src={spendCryptoLogo} alt="" />
            <div className="icLabel">Current Card Color</div>
            <div
              className="boxLogo"
              style={{
                background: res?.[`scc${index + 1}backgroundcolor`]
                  ? `#${res?.[`scc${index + 1}backgroundcolor`]}`
                  : "#050505",
              }}
            ></div>
            <div className="icLabel">New Card Colour</div>
            <label className="inpBox">
              <input
                type="text"
                className="input"
                placeholder="Ex. 251672"
                value={color}
                onChange={(e) => setColor(e.target.value)}
              />
              <div className="colorBox" style={{ background: `#${color}` }} />
            </label>
            <div className="btns">
              <div className="btnNP" onClick={() => setPageStep("home")}>
                Back
              </div>
              <div
                className="btnNP nxt"
                onClick={() => {
                  if (RegExp(/^([0-9a-f]{3}|[0-9a-f]{6})$/i).test(color)) {
                    updateColor();
                  } else {
                    tostShowOn("Enter Valid Color Code");
                  }
                }}
              >
                {updatingLogo ? "Updating" : "Next"}
              </div>
            </div>
          </div>
        );
      default:
        return (
          <Fragment key="0">
            <div className="optionItm" onClick={() => setStep("logo")}>
              Card Logo
            </div>
            <div className="optionItm" onClick={() => setStep("color")}>
              Card Colour
            </div>
          </Fragment>
        );
    }
  }
  return <div className="editSidebar">{getStep()}</div>;
}

export default EditSidebar;
