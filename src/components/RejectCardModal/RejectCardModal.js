import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { MainContext } from "../../context/MainContext";

import spendCrypto from "../../static/images/logos/spendCryptoLogo.svg";

function RejectCardModal({ onClose, cardToReject, onSuccess }) {
  const { email, token, tostShowOn } = useContext(MainContext);
  const [dp, setDp] = useState("");
  useEffect(() => {
    cardToReject &&
      Axios.post("https://comms.globalxchange.com/get_affiliate_data_no_logs", {
        email: cardToReject.email,
      }).then((res) => {
        const data = res.data[0];
        if (data) {
          // username: data.username,
          // name: data.name,
          setDp(data.profile_img);
        }
      });
  }, [cardToReject]);
  const [loading, setLoading] = useState(false);
  const issueCard = () => {
    setLoading(true);
    Axios.post(
      "https://comms.globalxchange.com/coin/vault/service/spending/vault/card/admin/update",
      {
        email: email,
        token: token,
        vault_id: cardToReject.vault_id,
        status: "Cancelled",
        card_data: {
          card_number: "",
          card_name: "",
          expiry: "",
          ccv: "",
        },
        card_fee: 0,
      }
    )
      .then(({ data }) => {
        if (data.status) {
          tostShowOn("Card Rejected");
          try {
            onSuccess();
          } catch (error) {}
        } else {
          tostShowOn(data.message);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return (
    <div className="issueModal">
      <div
        className="overlay"
        onClick={() => {
          try {
            onClose();
          } catch (e) {}
        }}
      />
      <div className="modalIssue">
        <div className="head">
          <img src={spendCrypto} alt="" />
        </div>
        <div className="modalContent">
          <div className="profile">
            <img src={dp} alt="" />
            <div className="textDetail">
              <div className="email">{cardToReject.email}</div>
              <div className="vid">
                <span>VauldID:</span> {cardToReject.vault_id}
              </div>
            </div>
          </div>
          <p className="rejectText">
            Are You Sure You Want To Reject This Request?
          </p>
          {loading ? (
            <div className="btnIssue">Rejecting Card...</div>
          ) : (
            <div className="btnIssue" onClick={issueCard}>
              Sumbit
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default RejectCardModal;
