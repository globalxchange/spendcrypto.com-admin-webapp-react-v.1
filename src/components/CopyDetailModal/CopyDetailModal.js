import React, { useContext, useEffect, useState } from "react";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { faCopy } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import spendCryptoLogo from "../../static/images/logos/spendCryptoLogo.svg";
import ReactJson from "react-json-view";
import { MainContext } from "../../context/MainContext";

function CopyDetailModal({ publication, onClose, showJSON = false }) {
  const { tostShowOn } = useContext(MainContext);
  const [dropOpen, setDropOpen] = useState(false);
  const [selectedKey, setSelectedKey] = useState("name");
  useEffect(() => {
    if (publication.name) {
      setSelectedKey("name");
    } else if (publication.bankerTag) {
      setSelectedKey("bankerTag");
    } else {
      setSelectedKey(Object.keys(publication)[0]);
    }
  }, []);
  const [showDetail, setShowDetail] = useState(showJSON);
  return (
    <div className="copyPubModal">
      <div
        className="overlay"
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      />
      <div className="modalCopy">
        <div className="head">
          <img src={spendCryptoLogo} alt="" />
        </div>
        <div className="copyContent">
          {showDetail ? (
            <ReactJson src={publication} />
          ) : (
            <>
              <div className="cpLabel">Select The Field You Want To See</div>
              <div className="btnDrop">
                <div
                  className="btnDropdown"
                  onClick={() => setDropOpen(!dropOpen)}
                >
                  {selectedKey.slice().replace(/_/g, " ")}
                  <FontAwesomeIcon icon={dropOpen ? faCaretUp : faCaretDown} />
                  {dropOpen && (
                    <div className="dropList">
                      {Object.keys(publication).map((key) => (
                        <div
                          className="dropItem"
                          onClick={() => setSelectedKey(key)}
                        >
                          {key.slice().replace(/_/g, " ")}
                        </div>
                      ))}
                    </div>
                  )}
                </div>
                <div
                  className="btnShow"
                  onClick={() => {
                    navigator.clipboard
                      .writeText(JSON.stringify(publication))
                      .then(() => {
                        tostShowOn(`Values copied to clipboard`);
                      });
                    setShowDetail(true);
                  }}
                >
                  Show All
                </div>
              </div>
              <div className="res">
                <span className="resVal">
                  {JSON.stringify(publication && publication[selectedKey])}
                </span>
                <FontAwesomeIcon
                  className="icon"
                  icon={faCopy}
                  onClick={() => {
                    navigator.clipboard
                      .writeText(
                        JSON.stringify(publication && publication[selectedKey])
                      )
                      .then(() => {
                        tostShowOn(
                          `Value of ${selectedKey} copied to clipboard`
                        );
                      });
                  }}
                />
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default CopyDetailModal;
