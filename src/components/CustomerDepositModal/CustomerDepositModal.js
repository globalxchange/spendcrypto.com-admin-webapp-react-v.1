import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment";
import JsonWebToken from "jsonwebtoken";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import Scrollbars from "react-custom-scrollbars";
import { MainContext } from "../../context/MainContext";

import spendCrypto from "../../static/images/logos/spendCryptoLogo.svg";
import cloudUpload from "../../static/images/postClipArt/cloudUpload.svg";
import LoadingAnim from "../LoadingAnim/LoadingAnim";

function renameFile(originalFile, newName) {
  return new File([originalFile], newName, {
    type: originalFile.type,
    lastModified: originalFile.lastModified,
  });
}

const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; // secret not to be disclosed anywhere.
const emailDev = "rahulrajsb@outlook.com"; // email of the developer.

function CustomerDepositModal({ onClose, data, onSuccess }) {
  const { email, token, tostShowOn } = useContext(MainContext);
  const [dp, setDp] = useState("");
  const [name, setName] = useState("");
  useEffect(() => {
    data &&
      Axios.post("https://comms.globalxchange.com/get_affiliate_data_no_logs", {
        email: data._id,
      }).then((res) => {
        const data = res.data[0];
        if (data) {
          setName(data.name || data.username);
          setDp(data.profile_img);
        }
      });
  }, [data]);
  const spendingVaults = data?.appVaults[0]?.spendingVaults;
  const appCode = data?.appVaults[0]?.app_code;
  const [selectedCard, setSelectedCard] = useState(false);
  const [loading, setLoading] = useState(false);

  const [setValues, setSetValues] = useState(false);

  const [thubnailLink, setThubnailLink] = useState("");
  const [thumbLoading, setThumbLoading] = useState(false);
  const uploadImage = async (e) => {
    setThumbLoading(true);
    const fileName = `${new Date().getTime()}${e.target.files[0].name.substr(
      e.target.files[0].name.lastIndexOf(".")
    )}`;
    const formData = new FormData();
    const file = renameFile(e.target.files[0], fileName);
    formData.append("files", file);
    const path_inside_brain = "root/";
    const token = JsonWebToken.sign(
      { name: fileName, email: emailDev },
      secret,
      {
        algorithm: "HS512",
        expiresIn: 240,
        issuer: "gxjwtenchs512",
      }
    );
    let { data } = await Axios.post(
      `https://drivetest.globalxchange.io/file/dev-upload-file?email=${emailDev}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
      formData,
      {
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
      }
    );
    setThubnailLink(data.payload.url);
    setThumbLoading(false);
  };
  console.log(data);

  const [cardAmount, setCardAmount] = useState("");
  const [merchantName, setMerchantName] = useState("");
  const [notes, setNotes] = useState("");
  const issueDebit = () => {
    setLoading(true);
    Axios.post(
      "https://comms.globalxchange.com/coin/vault/service/spending/balance/debit",
      {
        token: token,
        email: email,
        vault_id: selectedCard.vault_id, // USER VAULT ID
        userEmail: data._id,
        coin: "USD",
        merchant_name: merchantName,
        txn_date: moment.format("MM/DD/YYYY, hh:mm:ss A"),
        amount: parseFloat(cardAmount),
        icon: thubnailLink,
        notes: notes,
      }
    )
      .then(({ data }) => {
        if (data.status) {
          tostShowOn("Fund Debited");
          try {
            onSuccess();
          } catch (error) {}
        } else {
          tostShowOn(data.message);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <div className="issueModal">
      <div
        className="overlay"
        onClick={() => {
          try {
            onClose();
          } catch (e) {}
        }}
      />
      <div className="modalIssue">
        <div className="head">
          <img src={spendCrypto} alt="" />
        </div>
        <div className="modalContent">
          <div className="profile">
            <img src={dp} alt="" />
            <div className="textDetail">
              <div className="email">{data._id}</div>
              <div className="vid">
                {selectedCard ? (
                  <span>VauldID: {selectedCard.vault_id}</span>
                ) : (
                  appCode
                )}
              </div>
            </div>
          </div>
          {setValues ? (
            <>
              <div className="cFee">
                <input
                  type="text"
                  placeholder="Amount To Credit"
                  value={cardAmount}
                  onChange={(e) => setCardAmount(e.target.value)}
                />
                <div className="coinFee">USD</div>
              </div>
              <div className="cFee d-none">
                <input type="text" placeholder="Amount To Credit" />
                <div className="coinFee">USD</div>
                value={cardAmount}
                onChange={(e) => setCardAmount(e.target.value)}
              </div>
              <div className="cFee">
                <input
                  type="text"
                  placeholder="Merchant Name "
                  value={merchantName}
                  onChange={(e) => setMerchantName(e.target.value)}
                />
                <label className="settingsBox" onClick={() => {}}>
                  {thumbLoading ? (
                    <FontAwesomeIcon icon={faSpinner} spin />
                  ) : (
                    <img src={cloudUpload} alt="" />
                  )}
                  <input
                    type="file"
                    accept="image/*"
                    className="d-none"
                    onChange={uploadImage}
                  />
                </label>
              </div>
              <div className="cFee free">
                <textarea
                  className="textArea"
                  placeholder="Enter Details About This Transactions"
                  value={notes}
                  onChange={(e) => setNotes(e.target.value)}
                />
              </div>
              <div
                className="btnIssue"
                onClick={() => {
                  issueDebit();
                }}
              >
                Issue
              </div>
            </>
          ) : (
            <>
              <div className="headTitle">Select One Of {name}’s Cards</div>
              <Scrollbars
                className="cardListWrap"
                autoHide
                renderView={(props) => <div {...props} className="cardList" />}
              >
                {spendingVaults.map((vault) => (
                  <div
                    className={`vaultCard ${vault === selectedCard}`}
                    onClick={() => setSelectedCard(vault)}
                  >
                    {vault.vault_name}
                  </div>
                ))}
              </Scrollbars>
              <div
                className="btnIssue"
                onClick={() => {
                  if (selectedCard) setSetValues(true);
                }}
              >
                Issue
              </div>
            </>
          )}
          {loading && <LoadingAnim />}
        </div>
      </div>
    </div>
  );
}

export default CustomerDepositModal;
