import React, { useContext, useState } from "react";
import Axios from "axios";
import JsonWebToken from "jsonwebtoken";

import virtualCard from "../../static/images/cardIcons/virtual.svg";
import physicalCard from "../../static/images/cardIcons/physical.svg";
import masterCard from "../../static/images/cardIcons/masterCard.svg";
import visa from "../../static/images/cardIcons/visa.svg";
import spendCrypto from "../../static/images/logos/spendIconLogo.svg";
import cloudIcon from "../../static/images/postClipArt/cloudUpload.svg";
import { MainContext } from "../../context/MainContext";
import CardSvg from "./CardSvg";
import { Fragment } from "react";

function renameFile(originalFile, newName) {
  return new File([originalFile], newName, {
    type: originalFile.type,
    lastModified: originalFile.lastModified,
  });
}
const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; // secret not to be disclosed anywhere.
const emailDev = "rahulrajsb@outlook.com"; // email of the developer.

function NewCard() {
  const { email, token, tostShowOn, appSelected } = useContext(MainContext);

  const [step, setStep] = useState(0);
  const [color, setColor] = useState("");
  const [thumbLink, setThumbLink] = useState("");
  const [thumbLoading, setThumbLoading] = useState(false);
  const [typeOfCard, setTypeOfCard] = useState("");
  const [cardBrand, setCardBrand] = useState("");
  const [reseller, setReseller] = useState("");
  const [cardName, setCardName] = useState("");
  const [isUserNameReqired, setIsUserNameReqired] = useState(true);
  const [isUserAddressRequired, setIsUserAddressRequired] = useState(true);
  const [isRewardProgramme, setIsRewardProgramme] = useState(true);
  const [physicalCardData, setPhysicalCardData] = useState({});
  const [cardIssuanceData, setCardIssuanceData] = useState({});
  const [spendableBalanceData, setSpendableBalanceData] = useState({});

  const uploadImage = async (e) => {
    setThumbLoading(true);
    const fileName = `${new Date().getTime()}${e.target.files[0].name.substr(
      e.target.files[0].name.lastIndexOf(".")
    )}`;
    const formData = new FormData();
    const file = renameFile(e.target.files[0], fileName);
    formData.append("files", file);
    const path_inside_brain = "root/";
    const token = JsonWebToken.sign(
      { name: fileName, email: emailDev },
      secret,
      {
        algorithm: "HS512",
        expiresIn: 240,
        issuer: "gxjwtenchs512",
      }
    );
    let { data } = await Axios.post(
      `https://drivetest.globalxchange.io/file/dev-upload-file?email=${emailDev}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
      formData,
      {
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
      }
    );
    setThumbLink(data.payload.url);
    setThumbLoading(false);
  };

  // function getData() {
  //   Axios.get(
  //     `https://comms.globalxchange.com/gxb/apps/get?app_code=${appSelected.app_code}`
  //   ).then(({ data }) => {
  //     if (data.status) {
  //       setThumbLink(data.apps[0]?.spendcryptocardlogo1);
  //       setColor(data.apps[0]?.scc1backgroundcolor);
  //     }
  //   });
  // }

  function addCard() {
    Axios.post("https://comms.globalxchange.com/coin/sc/vendor/card/add", {
      email: email,
      token: token,
      card_type: cardBrand,
      card_name: cardName,
      card_background: `#${color}`,
      card_logo: thumbLink,
      physical_card_availability: typeOfCard === "Physical",
      name_of_user_required: isUserNameReqired,
      address_of_user_required: isUserAddressRequired,
      rewards_program: isRewardProgramme,
      physical_card_data: physicalCardData,
      card_issuance_data: cardIssuanceData,
      spendable_balance_data: spendableBalanceData,

      base_currency: "USD",
      additional_user_info: {},
      bank_data: {},
      transaction_fees: {},
      kickback_on_txn_fees: {},
      branding_logo: true,
      branding_background: true,
      adding_card_cost_id: "",
    });
  }

  function getStep(step) {
    switch (step) {
      case 0:
        return (
          <Fragment key="0">
            <div className="title">Select Type Of Card</div>
            <div className="options">
              <div
                className="option"
                onClick={() => {
                  setStep(1);
                  setTypeOfCard("Virtual");
                }}
              >
                <img className="icon" src={virtualCard} alt="" />
                <div className="label">Virtual</div>
              </div>
              <div
                className="option"
                onClick={() => {
                  setStep(1);
                  setTypeOfCard("Physical");
                }}
              >
                <img className="icon" src={physicalCard} alt="" />
                <div className="label">Physical</div>
              </div>
            </div>
          </Fragment>
        );
      case 1:
        return (
          <Fragment key="1">
            <div className="title">Select Card Brand</div>
            <div className="options">
              <div
                className="option"
                onClick={() => {
                  setStep(2);
                  setCardBrand("Master");
                }}
              >
                <img className="icon" src={masterCard} alt="" />
                <div className="label">MasterCard</div>
              </div>
              <div
                className="option"
                onClick={() => {
                  setStep(2);
                  setCardBrand("VISA");
                }}
              >
                <img className="icon" src={visa} alt="" />
                <div className="label">VISA</div>
              </div>
            </div>
          </Fragment>
        );
      case 2:
        return (
          <Fragment key="2">
            <div className="title">Select MasterCard Authorized Reseller</div>
            <div className="options">
              <div
                className="option"
                onClick={() => {
                  setReseller("SpendCrypto");
                  setStep(3);
                }}
              >
                <img className="icon" src={spendCrypto} alt="" />
                <div className="label">SpendCrypto</div>
              </div>
            </div>
          </Fragment>
        );
      case 3:
        return (
          <Fragment key="3">
            <div className="title">What Is The Name This Card?</div>
            <label className="inpBox">
              <input
                type="text"
                className="input"
                value={cardName}
                onChange={(e) => setCardName(e.target.value)}
                placeholder="Ex. Crypto Loyalty Card"
              />
            </label>
            <div
              className="btnNext"
              onClick={() => {
                cardName && setStep(4);
              }}
            >
              Next
            </div>
          </Fragment>
        );
      case 4:
        return (
          <Fragment key="4">
            <div className="title">What Is The Color Of The Card</div>
            <label className="inpBox">
              <input
                type="text"
                className="input"
                placeholder="Ex. 251672"
                value={color}
                onChange={(e) => setColor(e.target.value)}
              />
              <div className="colorBox" style={{ background: `#${color}` }} />
            </label>
            <div
              className="btnNext"
              onClick={() => {
                if (RegExp(/^([0-9a-f]{3}|[0-9a-f]{6})$/i).test(color)) {
                  setStep(5);
                } else {
                  tostShowOn("Enter Valid Color Code");
                }
              }}
            >
              Next
            </div>
          </Fragment>
        );
      case 5:
        return (
          <Fragment key="5">
            <div className="title">What Is The Logo On The Card?</div>
            <div className="options">
              <label className="option">
                <input
                  type="file"
                  accept="image/*"
                  className="d-none"
                  onChange={uploadImage}
                />
                <img src={cloudIcon} alt="" className="icon" />
                <div className="label">Upload</div>
              </label>
            </div>
            <div
              className="btnNext"
              onClick={() => {
                if (thumbLink && !thumbLoading) {
                  setStep(6);
                }
              }}
            >
              Next
            </div>
          </Fragment>
        );
      case 6:
        return (
          <Fragment key="5">
            <div className="title">
              Do You Need The Apps To Collect User Data
            </div>
            <div className="d-flex">
              <div
                className="btnNext"
                onClick={() => {
                  setStep(7);
                }}
              >
                Yes
              </div>
              <div
                className="btnNext"
                onClick={() => {
                  setStep(7);
                }}
              >
                No
              </div>
            </div>
          </Fragment>
        );
      case 7:
        return (
          <Fragment key="5">
            <div className="title">
              Do You Need The Apps To Collect User Living Data
            </div>
            <div className="d-flex">
              <div
                className="btnNext"
                onClick={() => {
                  setStep(8);
                }}
              >
                Yes
              </div>
              <div
                className="btnNext"
                onClick={() => {
                  setStep(8);
                }}
              >
                No
              </div>
            </div>
          </Fragment>
        );
      case 8:
        return (
          <Fragment key="5">
            <div className="title">Do You Want To Enable Commissions?</div>
            <div className="d-flex">
              <div
                className="btnNext"
                onClick={() => {
                  setStep(9);
                }}
              >
                Yes
              </div>
              <div
                className="btnNext"
                onClick={() => {
                  setStep(9);
                }}
              >
                No
              </div>
            </div>
          </Fragment>
        );
      case 9:
        return (
          <Fragment key="5">
            <div className="title">
              Is This Vehicle Look For An Immediate Transfer
            </div>
            <div className="d-flex">
              <div
                className="btnNext"
                onClick={() => {
                  setStep(10);
                }}
              >
                Yes
              </div>
              <div
                className="btnNext"
                onClick={() => {
                  setStep(10);
                }}
              >
                No
              </div>
            </div>
          </Fragment>
        );
      case 10:
        return (
          <Fragment key="5">
            <div className="title">Is Name On Card Reqired?</div>
            <div className="d-flex">
              <div
                className="btnNext"
                onClick={() => {
                  setStep(11);
                  setIsUserNameReqired(true);
                }}
              >
                Yes
              </div>
              <div
                className="btnNext"
                onClick={() => {
                  setStep(11);
                  setIsUserNameReqired(false);
                }}
              >
                No
              </div>
            </div>
          </Fragment>
        );
      case 11:
        return (
          <Fragment key="5">
            <div className="title">Is User Address Required?</div>
            <div className="d-flex">
              <div
                className="btnNext"
                onClick={() => {
                  setStep(12);
                  setIsUserAddressRequired(true);
                }}
              >
                Yes
              </div>
              <div
                className="btnNext"
                onClick={() => {
                  setStep(12);
                  setIsUserAddressRequired(false);
                }}
              >
                No
              </div>
            </div>
          </Fragment>
        );
      case 12:
        return (
          <Fragment key="5">
            <div className="title">Is Part Of Rewards Program</div>
            <div className="d-flex">
              <div
                className="btnNext"
                onClick={() => {
                  setStep(13);
                  setIsRewardProgramme(true);
                }}
              >
                Yes
              </div>
              <div
                className="btnNext"
                onClick={() => {
                  setStep(13);
                  setIsRewardProgramme(false);
                }}
              >
                No
              </div>
            </div>
          </Fragment>
        );
      case 13:
        return (
          <Fragment key="13">
            <div className="title">Card Issuance Fees</div>
            <div className="d-flex">
              <input
                className="btnNext"
                type="text"
                value={cardIssuanceData.amount}
                placeholder="Amount"
                onChange={(e) => {
                  e.persist();
                  setCardIssuanceData({
                    ...cardIssuanceData,
                    amount: e.target.value,
                  });
                }}
              />
              <input
                className="btnNext"
                type="text"
                value={cardIssuanceData.percentage}
                placeholder="Percentage"
                onChange={(e) => {
                  e.persist();
                  setCardIssuanceData({
                    ...cardIssuanceData,
                    percentage: e.target.value,
                  });
                }}
              />
              <input
                className="btnNext"
                type="text"
                value={cardIssuanceData.fixed_value}
                placeholder="Fixed Value"
                onChange={(e) =>
                  setCardIssuanceData({
                    ...cardIssuanceData,
                    fixed_value: e.target.value,
                  })
                }
              />
            </div>
            <div
              className="btnNext"
              onClick={() => {
                setStep(14);
              }}
            >
              Next
            </div>
          </Fragment>
        );
      case 14:
        return (
          <Fragment key="14">
            <div className="title">Physical Card Issuance Fees</div>
            <div className="d-flex">
              <input
                className="btnNext"
                type="text"
                value={physicalCardData.amount}
                placeholder="Amount"
                onChange={(e) =>
                  setPhysicalCardData({
                    ...physicalCardData,
                    amount: e.target.value,
                  })
                }
              />
              <input
                className="btnNext"
                type="text"
                value={physicalCardData.percentage}
                placeholder="Percentage"
                onChange={(e) =>
                  setPhysicalCardData({
                    ...physicalCardData,
                    percentage: e.target.value,
                  })
                }
              />
              <input
                className="btnNext"
                type="text"
                value={physicalCardData.fixed_value}
                placeholder="Fixed Value"
                onChange={(e) =>
                  setPhysicalCardData({
                    ...physicalCardData,
                    fixed_value: e.target.value,
                  })
                }
              />
            </div>
            <div
              className="btnNext"
              onClick={() => {
                setStep(15);
              }}
            >
              Next
            </div>
          </Fragment>
        );
      case 15:
        return (
          <Fragment key="15">
            <div className="title">Card Issuance Fees</div>
            <div className="d-flex">
              <input
                className="btnNext"
                type="text"
                value={spendableBalanceData.amount}
                placeholder="Amount"
                onChange={(e) =>
                  setSpendableBalanceData({
                    ...spendableBalanceData,
                    amount: e.target.value,
                  })
                }
              />
              <input
                className="btnNext"
                type="text"
                value={spendableBalanceData.percentage}
                placeholder="Percentage"
                onChange={(e) =>
                  setSpendableBalanceData({
                    ...spendableBalanceData,
                    percentage: e.target.value,
                  })
                }
              />
              <input
                className="btnNext"
                type="text"
                value={spendableBalanceData.fixed_value}
                placeholder="Fixed Value"
                onChange={(e) =>
                  setSpendableBalanceData({
                    ...spendableBalanceData,
                    fixed_value: e.target.value,
                  })
                }
              />
            </div>
            <div
              className="btnNext"
              onClick={() => {
                setStep(16);
              }}
            >
              Next
            </div>
          </Fragment>
        );

      default:
        break;
    }
  }
  return (
    <div className="newCard">
      <div className="newCardContent">{getStep(step)}</div>
      <div className="cardFormSidebar">
        {typeOfCard && (
          <>
            {cardName && <div className="headCardName">{cardName}</div>}
            <div className="cardContent">
              <div className="name">
                {typeOfCard} {cardBrand} Card
              </div>
              {reseller && <div className="by">By {reseller}</div>}

              <CardSvg
                cardTypeLogo={
                  cardBrand && (cardBrand === "Master" ? masterCard : visa)
                }
                logo={thumbLink}
                color={color}
              />
              {step === 16 && (
                <div onClick={addCard} className="issueCard">
                  Issue Card
                </div>
              )}
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default NewCard;
