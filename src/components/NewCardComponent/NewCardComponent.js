import React from "react";

import NewCard from "../../components/NewCard/NewCard";

function NewCardComponent() {
  return (
    <>
      <NewCard />
    </>
  );
}

export default NewCardComponent;
