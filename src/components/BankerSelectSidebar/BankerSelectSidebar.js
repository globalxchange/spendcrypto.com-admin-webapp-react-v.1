import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import Skeleton from "react-loading-skeleton";
import OtpInput from "react-otp-input";
import { useHistory } from "react-router-dom";
import { MainContext } from "../../context/MainContext";

function BankerSelectSidebar({ search }) {
  const history = useHistory();

  const [loading, setLoading] = useState(true);
  const [pin, setPin] = useState("");
  const [emailInp, setEmailInp] = useState("");
  const [bankerListObject, setBankerListObject] = useState({});
  const [bankerList, setBankerList] = useState([]);

  const {
    bankerSelected,
    setBankerSelected,
    setVendorEmail,
    setVendorPin,
  } = useContext(MainContext);

  useEffect(() => {
    setLoading(true);
    Axios.get("https://teller2.apimachine.com/admin/allBankers").then(
      ({ data }) => {
        if (data.status) {
          let tempObj = {};
          data.data.forEach((banker) => {
            tempObj[banker.BankerGXProfile] = banker;
          });
          setBankerListObject(tempObj);
          Axios.get("https://comms.globalxchange.com/coin/sc/vendor/get")
            .then(({ data }) => {
              if (data.status) {
                setBankerList(data.vendors);
              }
            })
            .finally(() => {
              setLoading(false);
            });
        }
      }
    );
  }, []);
  const [step, setStep] = useState("");

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      if (emailInp === bankerSelected.email) {
        setBankerSelected({
          sc_vendor_id: bankerSelected.sc_vendor_id,
          ...bankerListObject[bankerSelected.banker_profile_id],
        });
        history.push("/banker/login");
        setVendorEmail(emailInp);
      } else {
        setStep("denied");
      }
    }
  };

  function getStep() {
    switch (step) {
      case "pin":
        return (
          <div className="loginView">
            <div className="title">Enter Pin</div>
            <OtpInput
              containerStyle="otpWrapper"
              inputStyle="inputStyle"
              value={pin}
              onChange={setPin}
              shouldAutoFocus
            />
            <div
              className="btnNext"
              onClick={() => {
                if (pin === "4141") {
                  setVendorPin(true);
                  setBankerSelected({
                    sc_vendor_id: bankerSelected.sc_vendor_id,
                    ...bankerListObject[bankerSelected.banker_profile_id],
                  });
                  history.push("/banker/login");
                }
              }}
            >
              Login
            </div>
            <div className="goBack" onClick={() => setStep("")}>
              Go Back
            </div>
          </div>
        );
      case "denied":
        return (
          <div className="loginView">
            <div className="title">Access Denied</div>
            <p>
              The Details You Entered Don’t Match With The Permissioned Access
              Points For That App.
            </p>
            <div className="goBack" onClick={() => setStep("")}>
              Go Back
            </div>
          </div>
        );
      default:
        return (
          <div className="loginView">
            <div className="title">Enter Email</div>
            <input
              type="email"
              value={emailInp}
              onChange={(e) => setEmailInp(e.target.value)}
              className="inpBox"
              placeholder="What Is Your Login Email"
              onKeyPress={handleKeyPress}
            />
            <div
              className="btnNext"
              onClick={() => {
                if (emailInp === bankerSelected.email) {
                  setBankerSelected({
                    sc_vendor_id: bankerSelected.sc_vendor_id,
                    ...bankerListObject[bankerSelected.banker_profile_id],
                  });
                  history.push("/banker/login");
                  setVendorEmail(emailInp);
                } else {
                  setStep("denied");
                }
              }}
            >
              Next
            </div>
            <div className="goBack" onClick={() => setStep("pin")}>
              Use Access Pin
            </div>
          </div>
        );
    }
  }
  return (
    <>
      {bankerSelected?.banker_profile_id
        ? getStep()
        : loading
        ? Array(6)
            .fill("")
            .map((a, i) => (
              <div className="appItem">
                <Skeleton className="smIcon" circle />
                <span>
                  <Skeleton width={180} />
                </span>
              </div>
            ))
        : bankerList
            .filter(
              (banker) =>
                bankerListObject[banker.banker_profile_id]?._displayName
                  ?.toLowerCase()
                  .includes(search?.toLowerCase()) ||
                bankerListObject[banker.banker_profile_id]?.bankerTag
                  ?.toLowerCase()
                  .includes(search?.toLowerCase())
            )
            .map((banker) => (
              <div
                onClick={() => setBankerSelected(banker)}
                className="appItem"
              >
                <img
                  className="smIcon"
                  src={
                    bankerListObject[banker.banker_profile_id]?.profilePicURL
                  }
                  alt=""
                />
                <span>
                  {bankerListObject[banker.banker_profile_id]?.displayName ||
                    bankerListObject[banker.banker_profile_id]?.bankerTag}
                </span>
              </div>
            ))}
    </>
  );
}

export default BankerSelectSidebar;
