import React, { useState, useEffect, useContext } from "react";
import Scrollbars from "react-custom-scrollbars";
import { useParams } from "react-router-dom";
import Axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisH } from "@fortawesome/free-solid-svg-icons";
import Skeleton from "react-loading-skeleton";

import CopyDetailModal from "../CopyDetailModal/CopyDetailModal";
import IssuanceCard from "./IssuanceCard";
import IssueCardModal from "../IssueCardModal/IssueCardModal";
import RejectCardModal from "../RejectCardModal/RejectCardModal";
import VendorsFilter from "../VendorsFilter/VendorsFilter";
import { MainContext } from "../../context/MainContext";

function IssuanceFeed({
  selectedApp,
  isList,
  appListObject,
  tabSelected,
  setAppListOpen,
  dropDownOpen,
  filterOn,
}) {
  const { typeOfPage } = useParams();
  const { bankerSelected } = useContext(MainContext);
  const [loading, setLoading] = useState(false);
  const [processingList, setProcessingList] = useState([]);
  const [issuedList, setIssuedList] = useState([]);
  const [cancelledList, setCancelledList] = useState([]);
  const [toCopy, setToCopy] = useState(false);
  const [cardToIssue, setCardToIssue] = useState(false);
  const [cardToReject, setCardToReject] = useState(false);
  const getCardLists = async () => {
    setLoading(true);
    const processingRes = await Axios.post(
      "https://comms.globalxchange.com/coin/vault/service/spending/vault/cards/get",
      selectedApp
        ? {
            status: "Processing",
            app_code: selectedApp.app_code,
            sc_vendor_id: bankerSelected.sc_vendor_id,
          }
        : {
            status: "Processing",
            sc_vendor_id: bankerSelected.sc_vendor_id,
          }
    );
    const processingData = processingRes.data;
    if (processingData.status) {
      setProcessingList(processingData.spendingCards);
    }
    const issuedRes = await Axios.post(
      "https://comms.globalxchange.com/coin/vault/service/spending/vault/cards/get",
      selectedApp
        ? {
            status: "Issued",
            app_code: selectedApp.app_code,
            sc_vendor_id: bankerSelected.sc_vendor_id,
          }
        : {
            status: "Issued",
            sc_vendor_id: bankerSelected.sc_vendor_id,
          }
    );
    const issuedData = issuedRes.data;
    if (issuedData.status) {
      setIssuedList(issuedData.spendingCards);
    }
    const cancelledRes = await Axios.post(
      "https://comms.globalxchange.com/coin/vault/service/spending/vault/cards/get",
      selectedApp
        ? {
            status: "Cancelled",
            app_code: selectedApp.app_code,
            sc_vendor_id: bankerSelected.sc_vendor_id,
          }
        : {
            status: "Cancelled",
            sc_vendor_id: bankerSelected.sc_vendor_id,
          }
    );
    const cancelledData = cancelledRes.data;
    if (cancelledData.status) {
      setCancelledList(cancelledData.spendingCards);
    }
    setLoading(false);
  };
  useEffect(() => {
    getCardLists();
  }, [selectedApp]);
  const [filterType, setFilterType] = useState("Email");
  const [emailSearch, setEmailSearch] = useState("");
  return (
    <div className="withdrawalViewContent">
      {filterOn && (
        <VendorsFilter
          setSearchStr={setEmailSearch}
          searchStr={emailSearch}
          placeholder={`Seach ${tabSelected} Issuance Requests From ${
            selectedApp.app_name || "All Apps"
          }`}
          filterOne={selectedApp.app_name || "All"}
          filterTwo={"All"}
          filterOneClick={() => typeOfPage === "banker" && setAppListOpen(true)}
          hideBtns={dropDownOpen}
        />
      )}
      <div className={`withdrawListNFilter`}>
        <Scrollbars
          autoHide
          className="withdrawListWrapper"
          renderView={(props) => <div {...props} className="withdrawList" />}
        >
          {(tabSelected === "Pending" || tabSelected === "All") && (
            <div className="typeColoumn">
              <div className="head">
                Processing
                <FontAwesomeIcon icon={faEllipsisH} />
              </div>
              <Scrollbars
                className="columScrlWrap"
                autoHide
                renderView={(props) => (
                  <div {...props} className="coloumnScroll" />
                )}
              >
                {loading
                  ? [1, 2, 3, 4, 5, 6, 7, 8].map((i) => (
                      <div className="transactionItm" key={i}>
                        <div className="txnHead">
                          <div className="banker"></div>
                          <Skeleton circle height={30} width={30} />
                        </div>
                        <div className="txnContent px-5">
                          <Skeleton height={40} count={4} />
                        </div>
                        <div className={`txnFooter processing`}></div>
                      </div>
                    ))
                  : processingList
                      .filter((card) =>
                        card.email
                          .toLowerCase()
                          .includes(emailSearch.toLowerCase())
                      )
                      .map((card) => (
                        <IssuanceCard
                          key={card._id}
                          card={card}
                          setToCopy={setToCopy}
                          status="processing"
                          selectedApp={selectedApp}
                          onIssue={() => {
                            setCardToIssue(card);
                          }}
                          onReject={() => {
                            setCardToReject(card);
                          }}
                          appListObject={appListObject}
                        />
                      ))}
              </Scrollbars>
            </div>
          )}
          {(tabSelected === "Completed" || tabSelected === "All") && (
            <div className="typeColoumn">
              <div className="head">
                Issued
                <FontAwesomeIcon icon={faEllipsisH} />
              </div>
              <Scrollbars
                className="columScrlWrap"
                autoHide
                renderView={(props) => (
                  <div {...props} className="coloumnScroll" />
                )}
              >
                {loading
                  ? [1, 2, 3, 4, 5, 6, 7, 8].map((i) => (
                      <div className="transactionItm" key={i}>
                        <div className="txnHead">
                          <div className="banker"></div>
                          <Skeleton circle height={30} width={30} />
                        </div>
                        <div className="txnContent px-5">
                          <Skeleton height={40} count={4} />
                        </div>
                        <div className={`txnFooter issued`}></div>
                      </div>
                    ))
                  : issuedList
                      .filter((card) =>
                        card.email
                          .toLowerCase()
                          .includes(emailSearch.toLowerCase())
                      )
                      .map((card) => (
                        <IssuanceCard
                          key={card._id}
                          card={card}
                          setToCopy={setToCopy}
                          status="issued"
                          selectedApp={selectedApp}
                          onIssue={() => {
                            setCardToIssue(card);
                          }}
                          onReject={() => {
                            setCardToReject(card);
                          }}
                          appListObject={appListObject}
                        />
                      ))}
              </Scrollbars>
            </div>
          )}
          {(tabSelected === "Rejected" || tabSelected === "All") && (
            <div className="typeColoumn">
              <div className="head">
                Cancelled
                <FontAwesomeIcon icon={faEllipsisH} />
              </div>
              <Scrollbars
                className="columScrlWrap"
                autoHide
                renderView={(props) => (
                  <div {...props} className="coloumnScroll" />
                )}
              >
                {loading
                  ? [1, 2, 3, 4, 5, 6, 7, 8].map((i) => (
                      <div className="transactionItm" key={i}>
                        <div className="txnHead">
                          <div className="banker"></div>
                          <Skeleton circle height={30} width={30} />
                        </div>
                        <div className="txnContent px-5">
                          <Skeleton height={40} count={4} />
                        </div>
                        <div className={`txnFooter issued`}></div>
                      </div>
                    ))
                  : cancelledList
                      .filter((card) =>
                        card.email
                          .toLowerCase()
                          .includes(emailSearch.toLowerCase())
                      )
                      .map((card) => (
                        <IssuanceCard
                          key={card._id}
                          card={card}
                          setToCopy={setToCopy}
                          status="cancelled"
                          selectedApp={selectedApp}
                          onIssue={() => {
                            setCardToIssue(card);
                          }}
                          onReject={() => {
                            setCardToReject(card);
                          }}
                          appListObject={appListObject}
                        />
                      ))}
              </Scrollbars>
            </div>
          )}
        </Scrollbars>
      </div>

      {toCopy && (
        <CopyDetailModal
          publication={toCopy}
          onClose={() => setToCopy(false)}
        />
      )}
      {cardToIssue && (
        <IssueCardModal
          cardToIssue={cardToIssue}
          onClose={() => setCardToIssue(false)}
          onSuccess={() => {
            setCardToIssue(false);
            getCardLists();
          }}
        />
      )}
      {cardToReject && (
        <RejectCardModal
          cardToReject={cardToReject}
          onClose={() => setCardToReject(false)}
          onSuccess={() => {
            setCardToReject(false);
            getCardLists();
          }}
        />
      )}
    </div>
  );
}

export default IssuanceFeed;
