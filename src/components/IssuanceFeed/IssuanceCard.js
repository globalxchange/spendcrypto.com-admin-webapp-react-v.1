import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import moment from "moment";
import Scrollbars from "react-custom-scrollbars";

import { MainContext } from "../../context/MainContext";
import IsuanceSpendCard from "./IsuanceSpendCard";

function IssuanceCard({
  card,
  setToCopy,
  status,
  selectedApp,
  onIssue,
  appListObject,
  onReject,
}) {
  const { rates } = useContext(MainContext);
  const [profile, setProfile] = useState({
    username: "",
    name: "",
    profile_img: "",
  });
  const [vault, setVault] = useState({});
  const [selectedKey, setSelectedKey] = useState("usd_balance");
  const [selectKeyOpen, setSelectKeyOpen] = useState(false);
  const [balanceKeys, setBalanceKeys] = useState(["usd_balance"]);
  useEffect(() => {
    Axios.post("https://comms.globalxchange.com/get_affiliate_data_no_logs", {
      email: card.email,
    }).then((res) => {
      const data = res.data[0];
      if (data) {
        setProfile({
          username: data.username,
          name: data.name,
          profile_img: data.profile_img,
        });
      }
    });
    Axios.get(
      `https://comms.globalxchange.com/coin/vault/service/spending/vault/get?email=${card.email}`
    ).then(({ data }) => {
      let spendingVaults = data?.users[0]?.appVaults[0]?.spendingVaults;
      let tempVault = spendingVaults.filter(
        (vault) => vault.vault_id === card.vault_id
      )[0];
      if (tempVault) {
        setVault(tempVault);
        let balanceKeys = Object.keys(tempVault).filter((key) =>
          new RegExp(/_balance/).test(key)
        );
        if (balanceKeys.length) {
          setBalanceKeys(balanceKeys);
        }
      }
    });
  }, [card.email, card.vault_id]);
  return (
    <>
      <div className="transactionItm" key={card._id}>
        <div className="cardHead">
          <div className="">
            <img
              src={
                appListObject[card.app_code] &&
                appListObject[card.app_code].app_icon
              }
              alt=""
              className="bankerLogo"
            />
            <div className="banker">
              {appListObject[card.app_code] &&
                appListObject[card.app_code].app_name}
            </div>
          </div>
          <div
            className="coinSel"
            onClick={() => setSelectKeyOpen(!selectKeyOpen)}
          >
            <img src={""} alt="" className="bankerLogo" />
            <div className="banker">
              {selectedKey.replace("_balance", "").toUpperCase()}
            </div>
            {selectKeyOpen && (
              <div className="coinList">
                {balanceKeys.map((key) => (
                  <div className="coin" onClick={() => setSelectedKey(key)}>
                    <img src={""} alt="" className="bankerLogo" />
                    <div className="banker">
                      {key.replace("_balance", "").toUpperCase()}
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
        <div className="cardContent">
          <div className="d-flex">
            <IsuanceSpendCard cardId={card.card_id} appCode={card.app_code} />
            <div className="">
              <div className="nameNativeValue">
                <span>{profile.name || profile.username}</span>
                {/* <span>
              {FormatCurrency(
                vault[selectedKey],
                selectedKey.replace("_balance", "").toUpperCase()
              )}{" "}
              {selectedKey.replace("_balance", "").toUpperCase()}
            </span> */}
              </div>
              <div className="emailUsdValue">
                <span>{card.email}</span>
                {rates &&
                  rates[selectedKey.replace("_balance", "").toUpperCase()] &&
                  // <span>
                  //   $
                  //   {FormatCurrency(
                  //     vault[selectedKey] *
                  //       rates[selectedKey.replace("_balance", "").toUpperCase()]
                  //   )}{" "}
                  //   USD
                  // </span>
                  ""}
              </div>
            </div>
          </div>
          <Scrollbars
            className="btnScrlWrap"
            renderView={(props) => <div {...props} className="btnScrlList" />}
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
            autoHide
          >
            <div
              className="btnAction"
              onClick={() => {
                try {
                  onIssue();
                } catch (e) {}
              }}
            >
              Issue
            </div>
            <div
              className="btnAction"
              onClick={() => {
                try {
                  onReject();
                } catch (e) {}
              }}
            >
              Reject
            </div>
            <div
              className="btnAction disable"
              onClick={() => {
                // history.push(`/withdrawals/${card._id}`);
              }}
            >
              Expand
            </div>
            <div className="btnAction" onClick={() => setToCopy(card)}>
              Copy
            </div>
          </Scrollbars>
        </div>
        <div className={`cardFooter ${status}`}>
          <span>{moment(card.timestamp).format("MMM Do YYYY")}</span>
          <span>{moment(card.timestamp).format("h:mm:ss A z")}</span>
        </div>
      </div>
    </>
  );
}

export default IssuanceCard;
