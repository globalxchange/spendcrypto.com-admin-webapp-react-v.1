import Axios from "axios";
import React, { useEffect, useState } from "react";
import MasterCardSvg from "../MasterCardSvg/MasterCardSvg";

import masterCard from "../../static/images/cardIcons/masterCard.svg";
import visa from "../../static/images/cardIcons/visa.svg";

function IsuanceSpendCard({ cardId, appCode }) {
  const [cardDetail, setCardDetail] = useState();
  useEffect(() => {
    Axios.get(
      `https://comms.globalxchange.com/coin/sc/partner/cards/get?card_id=${cardId}&app_code=${appCode}`
    ).then(({ data }) => {
      if (data.status) {
        setCardDetail(data.partnerCards[0]);
      }
    });
  }, [cardId, appCode]);
  return (
    <MasterCardSvg
      logo={cardDetail?.branding_logo || cardDetail?.cardData?.card_logo}
      cardName={cardDetail?.branding_name || cardDetail?.cardData?.card_name}
      color="#050505"
      className="myCard"
      onClick={() => {}}
      cardTypeLogo={
        cardDetail?.cardData?.card_type === "Master" ? masterCard : visa
      }
    />
  );
}

export default IsuanceSpendCard;
