import React from "react";

function CustomerFilter({
  emailSearch,
  setEmailSearch,
  filterType,
  setFilterType,
}) {
  return (
    <div className="viewingAs">
      <input
        type="text"
        value={emailSearch}
        onChange={(e) => setEmailSearch(e.target.value)}
        className="searchInp"
        placeholder={`Type In Any Users ${filterType}.....`}
      />
      <div
        className={`btnType ${filterType === "Username"}`}
        onClick={() => setFilterType("Username")}
      >
        Username
      </div>
      <div
        className={`btnType ${filterType === "Email"}`}
        onClick={() => setFilterType("Email")}
      >
        Email
      </div>
    </div>
  );
}

export default CustomerFilter;
