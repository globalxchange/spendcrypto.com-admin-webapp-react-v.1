import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { MainContext } from "../../context/MainContext";

import spendCrypto from "../../static/images/logos/spendCryptoLogo.svg";
import settings from "../../static/images/clipIcons/settings.svg";

function IssueCardModal({ onClose, cardToIssue, onSuccess }) {
  const { email, token, tostShowOn } = useContext(MainContext);
  const [cardNumber, setCardNumber] = useState("");
  const [cardName, setCardName] = useState(cardToIssue.vault_name);
  const [mm, setMm] = useState("");
  const [yy, setYy] = useState("");
  const [cvv, setCvv] = useState("");
  const [fee, setFee] = useState(26);
  const [dp, setDp] = useState("");
  useEffect(() => {
    cardToIssue &&
      Axios.post("https://comms.globalxchange.com/get_affiliate_data_no_logs", {
        email: cardToIssue.email,
      }).then((res) => {
        const data = res.data[0];
        if (data) {
          // username: data.username,
          // name: data.name,
          setDp(data.profile_img);
        }
      });
  }, [cardToIssue]);
  const [loading, setLoading] = useState(false);
  const issueCard = () => {
    setLoading(true);
    Axios.post(
      "https://comms.globalxchange.com/coin/vault/service/spending/vault/card/admin/update",
      {
        email: email,
        token: token,
        vault_id: cardToIssue.vault_id,
        status: "Issued",
        card_data: {
          card_number: cardNumber,
          card_name: cardName,
          expiry: `${mm}/${yy}`,
          ccv: cvv,
        },
        card_fee: parseFloat(fee),
      }
    )
      .then(({ data }) => {
        tostShowOn("Card Issued");
        try {
          onSuccess();
        } catch (error) {}
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const [nameReadOnly, setNameReadOnly] = useState(true);
  const [feeReadOnly, setFeeReadOnly] = useState(true);
  return (
    <div className="issueModal">
      <div
        className="overlay"
        onClick={() => {
          try {
            onClose();
          } catch (e) {}
        }}
      />
      <div className="modalIssue">
        <div className="head">
          <img src={spendCrypto} alt="" />
        </div>
        <div className="modalContent">
          <div className="profile">
            <img src={dp} alt="" />
            <div className="textDetail">
              <div className="email">{cardToIssue.email}</div>
              <div className="vid">
                <span>VauldID:</span> {cardToIssue.vault_id}
              </div>
            </div>
          </div>
          <div className="cName">
            <input
              type="text"
              placeholder="Name On Card"
              value={cardName}
              onChange={(e) => setCardName(e.target.value)}
              readOnly={nameReadOnly}
            />
            <div
              className="settingsBox"
              onClick={() => setNameReadOnly(!nameReadOnly)}
            >
              <img src={settings} alt="" />
            </div>
          </div>
          <input
            type="text"
            placeholder="Card Number"
            className="cNum"
            value={cardNumber}
            onChange={(e) => setCardNumber(e.target.value)}
          />
          <div className="cred">
            <input
              type="text"
              placeholder="00"
              className="mm"
              value={mm}
              onChange={(e) => setMm(e.target.value)}
            />
            <input
              type="text"
              placeholder="00"
              className="yy"
              value={yy}
              onChange={(e) => setYy(e.target.value)}
            />
            <input
              type="text"
              placeholder="CCV"
              className="cvv"
              value={cvv}
              onChange={(e) => setCvv(e.target.value)}
            />
          </div>
          <div className="cFee">
            <input
              type="text"
              placeholder="Issuance Fee"
              value={fee}
              onChange={(e) => setFee(e.target.value)}
              readOnly={feeReadOnly}
            />
            <div className="coinFee">USD</div>
            <div
              className="settingsBox"
              onClick={() => setFeeReadOnly(!feeReadOnly)}
            >
              <img src={settings} alt="" />
            </div>
          </div>
          {loading ? (
            <div className="btnIssue">Issuing Card...</div>
          ) : (
            <div className="btnIssue" onClick={issueCard}>
              Issue
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default IssueCardModal;
