import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { MainContext } from "../../context/MainContext";

import spendCrypto from "../../static/images/logos/spendCryptoLogo.svg";
import settings from "../../static/images/clipIcons/settings.svg";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { FormatCurrency } from "../../utils/FunctionTools";

const balanceTypes = ["Processing Balance", "Vault Balance", "Card Balance"];
function CreditFundingModal({ onClose, cardToIssue, onSuccess, setJsonShow }) {
  const { email, token, tostShowOn } = useContext(MainContext);
  const [dp, setDp] = useState("");
  useEffect(() => {
    cardToIssue &&
      Axios.post("https://comms.globalxchange.com/get_affiliate_data_no_logs", {
        email: cardToIssue.email,
      }).then((res) => {
        const data = res.data[0];
        if (data) {
          // username: data.username,
          // name: data.name,
          setDp(data.profile_img);
        }
      });
  }, [cardToIssue]);
  const [loading, setLoading] = useState(false);
  const [feePercent, setFeePercent] = useState(3.5);
  const [feePercentReadOnly, setFeePercentReadOnly] = useState(true);
  const credit = cardToIssue.requestAmount;
  const afterFee = cardToIssue.requestAmount * (1 - feePercent / 100);
  const issueCard = () => {
    setLoading(true);
    Axios.post(
      "https://comms.globalxchange.com/coin/vault/service/spending/balance/request/admin",
      {
        email: email,
        token: token,
        request_identifier: cardToIssue.identifier,
        fee_percentage: feePercent,
        status: "Approved",
      }
    )
      .then(({ data }) => {
        if (data.status) {
          tostShowOn("Fund Issued");
          try {
            onSuccess();
          } catch (error) {}
        } else {
          tostShowOn(data.message);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };
  const [vautlBalances, setVautlBalances] = useState();
  const [spendableBalances, setSpendableBalances] = useState();
  const [processingBalance, setProcessingBalance] = useState();
  useEffect(() => {
    Axios.get(
      "https://comms.globalxchange.com/coin/vault/service/spending/vault/get",
      {
        params: { vault_id: cardToIssue.vault_id, email: cardToIssue.email },
      }
    )
      .then(({ data }) => {
        if (data.status) {
          let obj = data?.users[0]?.appVaults[0]?.spendingVaults?.filter(
            (vault) => vault.vault_id === cardToIssue.vault_id
          )[0];
          let balanceKeys = Object.keys(obj).filter((key) =>
            new RegExp(/_balance/).test(key)
          );
          let balances = {};
          balanceKeys.forEach((key) => {
            balances[key.replace(/_balance/, "").toUpperCase()] = obj[key];
          });
          delete balances.SPENDABLE;
          setVautlBalances(balances);
          setSpendableBalances(obj.spendable_balance);
        }
      })
      .finally(() => {});
    Axios.get(
      "https://comms.globalxchange.com/coin/vault/service/spending/processing/balance/get",
      {
        params: { vault_id: cardToIssue.vault_id, email: cardToIssue.email },
      }
    ).then(({ data }) => {
      if (data.status) {
        setProcessingBalance(data.balances[0]?.amount);
      }
    });
  }, [cardToIssue]);

  const [showSummary, setShowSummary] = useState(false);
  const [dropOpen, setDropOpen] = useState(false);
  const [selectedType, setSelectedType] = useState("Processing Balance");

  return (
    <div className="issueModal">
      <div
        className="overlay"
        onClick={() => {
          try {
            onClose();
          } catch (e) {}
        }}
      />
      <div className="modalIssue">
        <div className="head">
          <img src={spendCrypto} alt="" />
        </div>
        <div className="modalContent">
          <div className="profile">
            <img src={dp} alt="" />
            <div className="textDetail">
              <div className="email">{cardToIssue.email}</div>
              <div className="vid">
                <span>VauldID:</span> {cardToIssue.vault_id}
              </div>
            </div>
          </div>
          {showSummary ? (
            <>
              <div className="headTitle">Funding Summary</div>
              <div className="valItm">
                <span className="valName">Identifier</span>
                <span className="value">{cardToIssue.identifier}</span>
              </div>
              <div className="valItm">
                <span className="valName">Card Details</span>
                <span
                  className="value tdu"
                  onClick={() => setJsonShow(cardToIssue.card_data)}
                >
                  Show
                </span>
              </div>
              <div className="valItm">
                <div
                  className="btnDropdown"
                  onClick={() => setDropOpen(!dropOpen)}
                >
                  {selectedType}
                  <FontAwesomeIcon icon={dropOpen ? faCaretUp : faCaretDown} />
                  {dropOpen && (
                    <div className="dropList">
                      {balanceTypes.map(
                        (type) =>
                          type !== selectedType && (
                            <div
                              className="dropItem"
                              onClick={() => setSelectedType(type)}
                            >
                              {type}
                            </div>
                          )
                      )}
                    </div>
                  )}
                </div>
                <span className="value tdu">
                  {selectedType === "Vault Balance" ? (
                    <div className="list">
                      {Object.keys(vautlBalances).map((key) => (
                        <span>
                          {FormatCurrency(vautlBalances[key], key)} {key}
                        </span>
                      ))}
                    </div>
                  ) : selectedType === "Card Balance" ? (
                    <div className="list">
                      {Object.keys(spendableBalances).map((key) => (
                        <span>
                          {FormatCurrency(spendableBalances[key], key)} {key}
                        </span>
                      ))}
                    </div>
                  ) : (
                    <span>
                      {FormatCurrency(processingBalance, cardToIssue.coin)}{" "}
                      {cardToIssue.coin}
                    </span>
                  )}
                </span>
              </div>
              <hr className="hr" />
              <div className="valItm">
                <span className="valName">Requested Amount</span>
                <span className="value">
                  {cardToIssue.requestAmount} {cardToIssue.coin}
                </span>
              </div>
              <div className="valItm">
                <span className="valName">Crediting Amount</span>
                <span className="value">
                  {FormatCurrency(credit, cardToIssue.coin)} {cardToIssue.coin}
                </span>
              </div>
              <div className="valItm">
                <span className="valName">Funding Fee ($)</span>
                <span className="value">
                  {FormatCurrency(
                    (feePercent / 100) * cardToIssue.request_usd_value
                  )}{" "}
                  USD
                </span>
              </div>
              <div className="valItm">
                <span className="valName">After Fees Amount</span>
                <span className="value">
                  {FormatCurrency(afterFee, cardToIssue.coin)}{" "}
                  {cardToIssue.coin}
                </span>
              </div>
              <div className="spacer"></div>
              <div className="btnIssue" onClick={issueCard}>
                Confirm
              </div>
            </>
          ) : (
            <>
              <div className="cFee">
                <input
                  type="text"
                  placeholder="Amount To Credit"
                  value={credit}
                  readOnly
                />
                <div className="coinFee">USD</div>
              </div>
              <div className="cFee">
                <input
                  type="text"
                  placeholder="Issuance Fee"
                  value={feePercent}
                  onChange={(e) => setFeePercent(e.target.value)}
                  readOnly={feePercentReadOnly}
                />
                <div className="coinFee">%</div>
                <div
                  className="settingsBox"
                  onClick={() => setFeePercentReadOnly(!feePercentReadOnly)}
                >
                  <img src={settings} alt="" />
                </div>
              </div>
              <div className="cFee">
                <input
                  type="text"
                  placeholder="After Fees"
                  value={afterFee}
                  readOnly
                />
                <div className="coinFee">USD</div>
              </div>
              <div className="btnIssue" onClick={() => setShowSummary(true)}>
                Issue
              </div>
            </>
          )}
          {loading ? <div className="btnIssue">Issuing Fund...</div> : ""}
        </div>
      </div>
    </div>
  );
}

export default CreditFundingModal;
