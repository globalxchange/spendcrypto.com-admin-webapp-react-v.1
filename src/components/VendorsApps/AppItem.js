import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import guest from "../../static/images/coinIcon/crownCoin.svg";

function AppItem({ appData, onUsers }) {
  const [moreDropOpen, setMoreDropOpen] = useState(false);
  return (
    <div className="userItem">
      <img src={appData?.app_icon || guest} alt="" className="profileImg" />
      <div className="nameEmail">
        <span className="name">{appData?.app_name}</span>
        <span className="email">{appData?.app_code}</span>
      </div>
      <div className="time"></div>
      <div className="balance"></div>
      <div className="btnActions">
        <div
          className="btnAction"
          onClick={() => {
            try {
              onUsers();
            } catch (error) {}
          }}
        >
          Users
        </div>
        <div className="btnAction" onClick={() => {}}>
          Profile
        </div>
        <div
          className="btnAction more"
          onMouseEnter={() => setMoreDropOpen(true)}
          onMouseLeave={() => {
            setTimeout(() => {
              setMoreDropOpen(false);
            }, 100);
          }}
          onClick={() => setMoreDropOpen(!moreDropOpen)}
        >
          More
          {moreDropOpen && false && (
            <div className="dropList">
              {/* <div className="dropItm">Lineage</div>
              <div className="dropItm">Transactions</div>
              <div className="dropItm">Balances</div> */}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default AppItem;
