import React from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";

import AppItem from "./AppItem";

function VendorsApps({
  searchStr,
  appList,
  loading,
  setAppSelected,
  setTabSelected,
}) {
  return (
    <div className="userListDetailWrap">
      <Scrollbars
        className={`userListWrap`} //true
        autoHide
        renderView={(props) => <div {...props} className="userList" />}
      >
        {loading
          ? Array.from(Array(10).keys()).map((key) => (
              <div className="userItem" key={key}>
                <Skeleton circle width={50} height={50} />
                <div className="nameEmail">
                  <span className="name">
                    <Skeleton />
                  </span>
                  <span className="email">
                    <Skeleton />
                  </span>
                </div>
                <div className="time">
                  <Skeleton />
                </div>
                <div className="balance">
                  <Skeleton />
                </div>
                <div className="btnActions">
                  <Skeleton className="btnAction" />
                  <Skeleton className="btnAction" />
                  <Skeleton className="btnAction" />
                </div>
              </div>
            ))
          : appList
              .filter(
                (app) =>
                  app.app_name
                    .toLowerCase()
                    .includes(searchStr.toLowerCase()) ||
                  app.app_code.toLowerCase().includes(searchStr.toLowerCase())
              )
              .map((app) => {
                return (
                  <AppItem
                    key={app._id}
                    appData={app}
                    onUsers={() => {
                      setAppSelected(app);
                      setTabSelected("Users");
                    }}
                  />
                );
              })}
      </Scrollbars>
    </div>
  );
}

export default VendorsApps;
