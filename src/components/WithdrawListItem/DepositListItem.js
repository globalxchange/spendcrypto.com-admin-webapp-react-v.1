import Axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import moment from "moment";
import Scrollbars from "react-custom-scrollbars";

import { FormatCurrency } from "../../utils/FunctionTools";

function DepositListItem({ txn, selectedBanker, onAction, setToCopy }) {
  const history = useHistory();
  const { typeOfPage } = useParams();

  const [profile, setProfile] = useState({
    username: "",
    name: "",
    profile_img: "",
  });
  useEffect(() => {
    Axios.post("https://comms.globalxchange.com/get_affiliate_data_no_logs", {
      email: txn.email,
    }).then((res) => {
      const data = res.data[0];
      if (data) {
        setProfile({
          username: data.username,
          name: data.name,
          profile_img: data.profile_img,
        });
      }
    });
  }, [txn]);

  return (
    <>
      <div className="transactionItm" key={txn._id}>
        <div className="txnHead">
          <div className="banker">{selectedBanker.bankerTag}</div>
          <img
            src={selectedBanker.profilePicURL}
            alt=""
            className="bankerLogo"
          />
        </div>
        <div className="txnContent">
          <div className="nameNativeValue">
            <span>{profile.name || profile.username}</span>
            <span>
              {FormatCurrency(txn.buy_amount, txn.buy_coin)} {txn.buy_coin}
            </span>
          </div>
          <div className="emailUsdValue">
            <span>{txn.email}</span>
            <span>
              ${FormatCurrency(txn.bankerCredit && txn.bankerCredit.usd_value)}{" "}
              USD
            </span>
          </div>
          <Scrollbars
            className="btnScrlWrap"
            renderView={(props) => <div {...props} className="btnScrlList" />}
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
            autoHide
          >
            <div className="btnAction">Source Of Funds</div>
            <div className="btnAction">Uplines</div>
            <div className="btnAction">Why Meter</div>
            <div
              className="btnAction"
              onClick={() => {
                try {
                  onAction(txn._id, "credit");
                } catch (e) {}
              }}
            >
              Credit
            </div>
            <div
              className="btnAction"
              onClick={() => {
                try {
                  onAction(txn._id, "update");
                } catch (e) {}
              }}
            >
              Update
            </div>
            <div
              className="btnAction"
              onClick={() => {
                history.push(`/${typeOfPage}/withdrawals/${txn._id}`);
              }}
            >
              Expand
            </div>
            <div className="btnAction" onClick={() => setToCopy(txn)}>
              Copy
            </div>
          </Scrollbars>
        </div>
        <div
          className={`txnFooter ${
            txn.current_step_data && txn.current_step_data.status
          }`}
        >
          <span>{moment(txn.timestamp).format("MMM Do YYYY")}</span>
          <span>{moment(txn.timestamp).format("h:mm:ss A z")}</span>
        </div>
      </div>
    </>
  );
}

export default DepositListItem;
