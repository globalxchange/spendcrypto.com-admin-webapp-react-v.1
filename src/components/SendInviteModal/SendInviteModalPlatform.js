import React, { useState } from "react";

import spendCrypto from "../../static/images/logos/spendCryptoLogo.svg";
import android from "../../static/images/platforms/androidColor.svg";
import ios from "../../static/images/platforms/iosColor.svg";

function SendInviteModalPlatform({ onClose, setPlatform, appName }) {
  const [download, setDownload] = useState(false);
  return (
    <div className="inviteModal">
      <div
        className="overlay"
        onClick={() => {
          try {
            onClose();
          } catch (e) {}
        }}
      />
      <div className="modalInvite">
        {download ? (
          <>
            <div className="headInvite">
              <img src={spendCrypto} alt="" className="headLogo" />
            </div>
            <div className="contentInvite platform">
              <label htmlFor="">Which App Do You Want? </label>
              <div className="inputBoxIn">
                <div className="boxApp" onClick={() => setPlatform("android")}>
                  <img src={android} alt="" />
                  Android
                </div>
                <div className="boxApp" onClick={() => setPlatform("ios")}>
                  <img src={ios} alt="" />
                  Iphone
                </div>
              </div>
            </div>
          </>
        ) : (
          <>
            <div className="headInviteMain">
              <img src={spendCrypto} alt="" className="headLogo" />
            </div>
            <div className="contentInvite main">
              <p className="downloadDesc">
                To Spend Crypto From Your {appName} Wallet You Need To Login To
                The SpendCrypto App With Your {appName} Credentials.{" "}
              </p>
            </div>
            <div className="footer" onClick={() => setDownload(true)}>
              Download The App
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default SendInviteModalPlatform;
