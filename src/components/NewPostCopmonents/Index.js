import React, { useState } from "react";
import NewPostLander from "./NewPostLander";
import NewVideoPost from "./NewVideoPost";

function NewPost({ publication }) {
  const [postType, setPostType] = useState("");
  const getContent = () => {
    switch (postType) {
      case "video":
        return <NewVideoPost publication={publication} />;

      default:
        return <NewPostLander setPostType={setPostType} />;
    }
  };
  return <div className="newPost">{getContent()}</div>;
}

export default NewPost;
