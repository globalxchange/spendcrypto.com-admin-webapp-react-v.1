import React from "react";

function VendorsFilter({
  searchStr,
  setSearchStr,
  placeholder,
  filterOne,
  filterTwo,
  filterOneClick,
  filterTwoClick,
  hideBtns,
}) {
  return (
    <div className="viewingAs">
      <input
        type="text"
        value={searchStr}
        onChange={(e) => setSearchStr(e.target.value)}
        className="searchInp"
        placeholder={placeholder}
      />
      {!hideBtns && (
        <>
          <div
            className="btnType"
            onClick={() => {
              try {
                filterOneClick();
              } catch (error) {}
            }}
          >
            {filterOne}
          </div>
          <div
            className="btnType"
            onClick={() => {
              try {
                filterTwoClick();
              } catch (error) {}
            }}
          >
            {filterTwo}
          </div>
        </>
      )}
    </div>
  );
}

export default VendorsFilter;
