import Axios from "axios";
import React, { useState, useContext, useEffect } from "react";
import Skeleton from "react-loading-skeleton";
import OtpInput from "react-otp-input";
import { useHistory } from "react-router-dom";
import { MainContext } from "../../context/MainContext";

function AppSelectSidebar({ search }) {
  const history = useHistory();
  // App List Data
  const {
    appSelected,
    setAppSelected,
    setVendorEmail,
    setVendorPin,
  } = useContext(MainContext);
  const [appListObject, setAppListObject] = useState({});
  const [appList, setAppList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [pin, setPin] = useState("");
  const [emailInp, setEmailInp] = useState("");

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      if (emailInp === appSelected.email) {
        history.push("/partner/login");
        setVendorEmail(emailInp);
        setAppSelected(appListObject[appSelected.app_code]);
      } else {
        setStep("denied");
      }
    }
  };

  useEffect(() => {
    setLoading(true);
    Axios.get("https://comms.globalxchange.com/gxb/apps/get").then(
      ({ data }) => {
        if (data.status) {
          let tempObj = {};
          data.apps.forEach((app) => {
            tempObj[app.app_code] = app;
          });
          setAppListObject(tempObj);
          Axios.get("https://comms.globalxchange.com/coin/sc/partner/get")
            .then(({ data }) => {
              if (data.status) {
                setAppList(data.partners);
              }
            })
            .finally(() => {
              setLoading(false);
            });
        }
      }
    );
  }, [appSelected, setAppSelected]);
  const [step, setStep] = useState("");
  function getStep() {
    switch (step) {
      case "pin":
        return (
          <div className="loginView">
            <div className="title">Enter Pin</div>
            <OtpInput
              containerStyle="otpWrapper"
              inputStyle="inputStyle"
              value={pin}
              onChange={setPin}
              shouldAutoFocus
            />
            <div
              className="btnNext"
              onClick={() => {
                if (pin === "4141") {
                  setVendorPin(true);
                  setAppSelected(appListObject[appSelected.app_code]);
                  history.push("/partner/login");
                }
              }}
            >
              Login
            </div>
            <div className="goBack" onClick={() => setStep("")}>
              Go Back
            </div>
          </div>
        );
      case "denied":
        return (
          <div className="loginView">
            <div className="title">Access Denied</div>
            <p>
              The Details You Entered Don’t Match With The Permissioned Access
              Points For That App.
            </p>
            <div className="goBack" onClick={() => setStep("")}>
              Go Back
            </div>
          </div>
        );
      default:
        return (
          <div className="loginView">
            <div className="title">Enter Email</div>
            <input
              type="email"
              value={emailInp}
              onChange={(e) => setEmailInp(e.target.value)}
              className="inpBox"
              placeholder="What Is Your Login Email"
              onKeyPress={handleKeyPress}
            />
            <div
              className="btnNext"
              onClick={() => {
                if (emailInp === appSelected.email) {
                  setAppSelected(appListObject[appSelected.app_code]);
                  history.push("/partner/login");
                  setVendorEmail(emailInp);
                } else {
                  setStep("denied");
                }
              }}
            >
              Next
            </div>
            <div className="goBack" onClick={() => setStep("pin")}>
              Use Access Pin
            </div>
          </div>
        );
    }
  }
  return (
    <>
      {appSelected?.app_code
        ? getStep()
        : loading
        ? Array(6)
            .fill("")
            .map((a, i) => (
              <div className="appItem">
                <Skeleton className="smIcon" circle />
                <span>
                  <Skeleton width={180} />
                </span>
              </div>
            ))
        : appList
            .filter(
              (app) =>
                appListObject[app.app_code]?.app_name
                  ?.toLowerCase()
                  .includes(search?.toLowerCase()) ||
                appListObject[app.app_code]?.app_code
                  ?.toLowerCase()
                  .includes(search?.toLowerCase())
            )
            .map((app) => (
              <div onClick={() => setAppSelected(app)} className="appItem">
                <img
                  className="smIcon"
                  src={appListObject[app.app_code].app_icon}
                  alt=""
                />
                <span>{appListObject[app.app_code].app_name}</span>
              </div>
            ))}
    </>
  );
}

export default AppSelectSidebar;
