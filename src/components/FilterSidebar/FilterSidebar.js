import React, { useState, useEffect } from "react";
import Scrollbars from "react-custom-scrollbars";

import crown from "../../static/images/coinIcon/crownCoin.svg";

function FilterSidebar({
  setAppSelected,
  setDropDownOpen,
  appList,
  search,
  tab,
  setFilter,
}) {
  const [tabSelected, setTabSelected] = useState(tab);
  useEffect(() => {
    setTabSelected(tab);
  }, [tab]);
  const getContent = () => {
    switch (tabSelected) {
      case "App":
        return (
          <Scrollbars
            autoHide
            className="sideBarAppSelect"
            renderView={(props) => <div {...props} className="view" />}
          >
            <div
              className="appPublication"
              onClick={() => {
                setAppSelected("");
                setDropDownOpen(false);
              }}
            >
              <img src={crown} alt="" className="pubLogo" />
              <div className="nameNbtns">
                <div className="name">All Apps</div>
                <div className="appCode">all</div>
              </div>
            </div>
            {appList
              .filter((app) =>
                app.app_name.toLowerCase().includes(search.toLowerCase())
              )
              .map((app) => (
                <div
                  className="appPublication"
                  key={app._id}
                  onClick={() => {
                    setAppSelected(app);
                    setDropDownOpen(false);
                  }}
                >
                  <img src={app.app_icon} alt="" className="pubLogo" />
                  <div className="nameNbtns">
                    <div className="name">{app.app_name}</div>
                    <div className="appCode">{app.app_code}</div>
                  </div>
                </div>
              ))}
          </Scrollbars>
        );

      case "Filter 1":
        return (
          <div className="filterConfig">
            <div className="inpBox">
              <div className="labelBox">Field</div>
              <input
                type="text"
                className="inputBox"
                placeholder="Field"
                value="Spending Vaults"
              />
            </div>
            <div className="inpBox">
              <div className="labelBox">Parameters</div>
              <input
                type="text"
                className="inputBox"
                placeholder="Parameters"
                value="Minimum"
              />
            </div>
            <div className="inpBox">
              <div className="labelBox">Value</div>
              <input
                type="text"
                className="inputBox"
                placeholder="Value"
                value="1"
              />
            </div>
          </div>
        );

      default:
        return (
          <div className="filterConfig" key={tabSelected}>
            <div className="inpBox">
              <div className="labelBox">Field</div>
              <input type="text" className="inputBox" placeholder="Field" />
            </div>
            <div className="inpBox">
              <div className="labelBox">Parameters</div>
              <input
                type="text"
                className="inputBox"
                placeholder="Parameters"
              />
            </div>
            <div className="inpBox">
              <div className="labelBox">Value</div>
              <input type="text" className="inputBox" placeholder="Value" />
            </div>
          </div>
        );
    }
  };
  return (
    <div className="filterSidebar">
      <Scrollbars
        className="scrollTabHead"
        renderThumbHorizontal={() => <div />}
        renderThumbVertical={() => <div />}
      >
        <div
          className={`tab ${tabSelected === "App"}`}
          onClick={() => setTabSelected("App")}
        >
          App
        </div>
        <div
          className={`tab ${tabSelected === "Filter 1"}`}
          onClick={() => setTabSelected("Filter 1")}
        >
          Filter 1
        </div>
        <div
          className={`tab ${tabSelected === "Filter 2"}`}
          onClick={() => setTabSelected("Filter 2")}
        >
          Filter 2
        </div>
        <div
          className={`tab ${tabSelected === "Filter 3"}`}
          onClick={() => setTabSelected("Filter 3")}
        >
          Filter 3
        </div>
        <div
          className={`tab ${tabSelected === "Filter 4"}`}
          onClick={() => setTabSelected("Filter 4")}
        >
          Filter 4
        </div>
      </Scrollbars>
      {getContent()}
      <div className="btnsResetFilter">
        <div className="btns" onClick={() => setFilter("All")}>
          Reset
        </div>
        <div
          className="btns inv"
          onClick={() => {
            if (tabSelected === "Filter 1") {
              setFilter("1SPV");
            }
          }}
        >
          Filter
        </div>
      </div>
    </div>
  );
}

export default FilterSidebar;
